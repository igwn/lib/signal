# ROOT C++ Library Template

This is a ready-to-use template for library development purpose! It is meant to normalize the development of C++ libraries, offering a structured setup making use of cmake, testing features, documentation generation using Doxygen, a Docker integration, and GitLab CI/CD workflows for continuous integration and deployment. The framework now includes the CERN ROOT C++ framework, empowering scientific development capabilities within the project.

## Features

- **Library**: Compile the library using `./bootstrap.sh` or running `cmake && make -j && make install`.
- **Tests**: Run tests with `make tests`.
- **Examples**: Executable examples (accessible in the build directory) not meant to be installed, but to illustrate how to use the main library features.
- **Scripts**: Executable tools designed for library usage. These tools are practical use meant to take advantage of the library features and installed into the final directory.
- **Doxygen Documentation**: Generates a responsive webpage (using Doxygen Awesome) including versioning and complementary documentation information based on markdown files located in `./docs`.
- **Docker Integration**: Includes a Dockerfile for custom image creation, aiding CI/CD workflows.
- **GitLab CI/CD**: Structured as a GitLab template for easy adoption, preparing the environment, compiling, and deploying the library documentation a dedicated GitLab pipeline.

## CERN ROOT Integration

The integration of the CERN ROOT C++ framework enhances the scientific capabilities of this library template. ROOT offers a comprehensive set of tools for data analysis, visualization, and statistical computations. By incorporating ROOT into this development environment, the compiled programs and libraries ship with enhanced scientific functionalities.

ROOT provides features like a histogramming system, graphical representation tools, and data storage and analysis libraries, making it particularly beneficial for scientific computing tasks. Integrating ROOT into this library template extends the capabilities for scientific research and analysis within C++ development projects.

## Source Code and Directory Structure

The `./src` directory contains the core source code of the library, while the `./include` directory stores essential header files required for the library.

A `CMakeLists.txt` file facilitates library compilation and manages dependencies. Subdirectories include:
- `./tests`: Holds test suites for ensuring library functionality.
- `./examples`: Contains executable examples illustrating various usage scenarios of the library.
- `./scripts`: Stores executable tools designed to enhance and complement the library's functionalities.

## Documentation

The documentation can be generated using Doxygen `make docs` after the preparation of the `build` directory. The `docs` directory stores all Markdown files used for on-the-fly documentation generation. The comprehensive Doxygen documentation will then be located in `./public` after compilation. This directory can be moved and served using an HTTP server (e.g., NGINX or Apache2).

## GitLab CI/CD and Pages Integration

This framework seamlessly integrates with GitLab CI/CD for automated testing, building, and deployment. GitLab Pages are automatically generated upon successful pipeline completion and tag publication. Configuration of these features can be done through the GitLab repository settings post the initial successful pipeline run.

The CI/CD workflow will automatically run on a GitLab instance when pushing a commit or releasing a new version. It ensures the compilation of the library within a Docker container, comprehensive testing, and initiates documentation deployment upon creating a new tag.

## Docker Container

Utilize the Dockerfile provided for generating Docker containers. The Docker image gets updated as needed during CI/CD. For a new repository, execute commands like `make image` or `make image-publish` for initial image preparation. The default DockerHub registry can be customized using `-DDOCKER_REGISTRY=https://<your url>` within the cmake command.

## Usage

1. **Clone the Repository**:

    ```bash
    git clone https://gitlab.com/your-namespace/your-cpp-library.git
    cd your-cpp-library
    ```

2. **Bootstrap the Project**:

    Run `./bootstrap.sh` to prepare, compile, and install the project:

    ```bash
    ./bootstrap.sh
    ```

    This script prepares the project, compiles the code, and installs necessary components. You can also call `./bootstrap.sh 1` if you need to reconfigure your project by rerunning the cmake command.

3. **Building and Testing**:

    Execute tests to ensure library functionality:
    ```bash
    make tests
    ```

4. **Generating Documentation**:

    Generate comprehensive documentation, including versioning and additional Markdown information:

    ```bash
    make docs
    ```

    Access the generated documentation by opening `./public/index.html` in a web browser.

5. **Exploring Examples**:

    Explore the executable examples located in the `examples/` directory to understand and visualize various library usage scenarios.

6. **Docker Integration**:

    Utilize the provided Dockerfile to streamline CI/CD workflows.

## Contributions and Support

Contributions, bug reports, and feature requests are welcome! Please refer to CONTRIBUTING.md for detailed guidelines.

## License

Licensed under the [MIT License](LICENSE.md).
