# Version used during development
cmake_minimum_required(VERSION 3.5)
project(RSignal/FFTW)

# Include custom CMake modules and utilities
list(PREPEND CMAKE_MODULE_PATH 
    ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/Inc/
    ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/Req/
    ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/Modules/
)

#
# Prepare library including ROOT    
set(LIBRARY "${LIBRARY}_FFTW")
FILE_SOURCES(SOURCES "src")
add_library(${LIBRARY} SHARED ${SOURCES})
target_include_directories(${LIBRARY} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

target_link_libraries(${LIBRARY} ${ROOT_LIBRARIES})
target_link_package(${LIBRARY} FFTW REQUIRED)

#
# Prepare ROOT Dictionary
set(DICTIONARY ${LIBRARY}.Dict)
set(LINKDEF LinkDef.h)
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include/${LINKDEF})
    message(FATAL_ERROR "ROOT LinkDef is missing `${LINKDEF}`.")
endif()

FILE_HEADERS_RECURSE(HEADERS "include")
list(REMOVE_ITEM HEADERS ${LINKDEF})
ROOT_GENERATE_DICTIONARY(${DICTIONARY} ${HEADERS} LINKDEF ${CMAKE_CURRENT_SOURCE_DIR}/include/${LINKDEF} MODULE ${LIBRARY})

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include DESTINATION . PATTERN "*.in" EXCLUDE)
install(TARGETS ${LIBRARY} LIBRARY DESTINATION lib ARCHIVE DESTINATION lib RUNTIME DESTINATION bin)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}.rootmap DESTINATION lib)   # Install the rootmap file
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}_rdict.pcm DESTINATION lib) # Install the rdict.pcm file
