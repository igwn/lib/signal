# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
  NAMES fftw3 fftw3f
  HEADERS "fftw3.h" "fftw3f.h"
  PATHS $ENV{FFTW} $ENV{FFTW3}
)