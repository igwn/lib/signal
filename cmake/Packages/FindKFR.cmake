# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
set(_KFR_CAPI kfr_capi)

if(KFR_VERSION STREQUAL 5)

	find_package_standard(
	        NAMES kfr_io kfr_dft
	        OPTIONALS kfr_capi
	        HEADERS "kfr/kfr.h"
	        PATHS ${KFR} $ENV{KFR}
	)

else()

	find_package_standard(
		NAMES kfr_io
		OPTIONALS ${_KFR_CAPI} 
		          kfr_dft_neon64 libkfr_dft_sse2 libkfr_dft_avx512 libkfr_dft_avx2 libkfr_dft_avx libkfr_dft_sse41 libkfr_dft_sse2
		          kfr_dsp_neon64 libkfr_dsp_sse2 libkfr_dsp_avx512 libkfr_dsp_avx2 libkfr_dsp_avx libkfr_dsp_sse41 libkfr_dsp_sse2
		HEADERS "kfr/kfr.h"
		PATHS ${KFR} $ENV{KFR}
	)

endif()
