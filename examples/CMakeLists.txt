# Version used during development
cmake_minimum_required(VERSION 3.5)
project(RSignal/examples)

# Include custom CMake modules and utilities
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

include(MessageColor)        # Include utility for colored message output
include(FindPackageStandard) # Include utility for finding packages
include(ProjectArchitecture) # Include utility for project architecture
include(ParentCMakeOnly)     # Include utility for parent CMake only actions

# Output section header for Examples
message_title("Examples")

# Collect example sources and display project architecture
FILE_SOURCES(EXAMPLES "${CMAKE_CURRENT_SOURCE_DIR}")
DUMP_ARCHITECTURE(EXAMPLES)
message("")

# Get common path for examples
GET_COMMON_PATH(EXAMPLEPATH_COMMON EXAMPLES)

# Add a custom target 'examples' for running examples
add_custom_target(examples COMMENT "Running examples")

# Find example files, prepare example targets, link required libraries, and install examples
foreach(SRC ${EXAMPLES})

    get_filename_component(EXAMPLE ${SRC} NAME_WE)
    if(EXAMPLE STREQUAL "main")
        get_filename_component(EXAMPLE ${SRC} DIRECTORY)
        get_filename_component(EXAMPLE ${EXAMPLE} NAME_WE)
    endif()

    get_filename_component(EXAMPLEPATH ${SRC} PATH)
    file(RELATIVE_PATH EXAMPLEPATH_REL ${EXAMPLEPATH_COMMON} ${EXAMPLEPATH})
    if(NOT "${EXAMPLEPATH_REL}" STREQUAL "")
        list(APPEND EXAMPLEPATH_REL "/")
    endif()

    message_color("-- Preparing target example `./${EXAMPLEPATH_REL}${EXAMPLE}`" COLOR GREEN)

    # Prepare example program
    add_executable(${EXAMPLE} ${SRC})
    target_link_package(${EXAMPLE} ${LIBRARY} REQUIRED)

    # Custom `make examples`
    add_dependencies(examples ${EXAMPLE})

    # Install example files to specified location with permissions
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${EXAMPLE}
            DESTINATION share/examples
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
            OPTIONAL
    )
endforeach()