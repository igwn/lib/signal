/**
 * *********************************************
 *
 * \file TComplexPlan.h
 * \brief Header of the TComplexPlan class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TComplexPlan
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date May 19th, 2022
 *
 * *********************************************
 */

#pragma once

#include <Riostream.h>
#include <TF2.h>
#include <TAxis.h>
#include <TPad.h>
#include <TComplex.h>
#include <TGraph.h>

#include <string>
#include <regex>
#include <iterator>

namespace ROOT { namespace Signal {

        class TComplexPlan: public TF2
        {
                protected:
                        int sectors;
                        double unit;

                public:
                        using TF2::TF2;
                        TComplexPlan() {};
                        ~TComplexPlan() { };
                        
                        TComplexPlan(const char *name, const char *formula, Double_t xmin=0, Double_t xmax=1, Double_t ymin=0, Double_t ymax=1, Option_t * opt = nullptr);
                        void Draw(Option_t *option = "", int level = 0);
                        void DrawContour(Option_t *option = "");
                        void DrawUnitCircle(int level = 0);
                        void DrawSectors(int level = 0);
                        
                        void SetUnit(double unit = 1.0);
                        void SetNsectors(int sectors = 4);

                        static TString Regex(const char* formula)
                        {
                                TString regex = TString(std::regex_replace(
                                        std::regex_replace(
                                                std::string("TComplex::Abs("+std::string(formula)+")"), 
                                                std::regex("\\bz\\b"), 
                                                std::string("TComplex(x,y)")
                                        ),
                                        std::regex("\\b(i|j)\\b"), 
                                        std::string("TComplex(0,1)")).c_str());

                                return regex;
                        }

                ClassDef(ROOT::Signal::TComplexPlan, 1);
        };
}}
