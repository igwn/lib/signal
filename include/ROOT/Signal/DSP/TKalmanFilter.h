/**
 * *********************************************
 *
 * \file TKalmanFilter.h
 * \brief Header of the TKalmanFilter class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TKalmanFilter
 * \brief Base class for Kalman filter implementation..
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 * \date July 7th, 2023
 *
 * *********************************************
 */

#pragma once

#include <Rioplus.h>
#include "../TKFRLegacy.h"

//
// Adapted from https://github.com/hmartiro/kalman-cpp/tree/master
// Source: http://www.cs.unc.edu/~welch/media/pdf/kalman_intro.pdf

namespace ROOT { namespace Signal {

    class TKalmanFilter
    {
        public:

            struct Measurement
            {
                Measurement(int m = 0) : _self(m), _time(NAN) {};
                Measurement(int m, double *z, double _time = NAN) : _self(m, z), _time(_time) {};
                Measurement(const TVectorD &z, double _time = NAN) : _self(z.GetNrows()), _time(_time) {};
                Measurement(const std::vector<double> &z, double _time = NAN) : _self(z.size(), &z[0]), _time(_time) {};
                Measurement(const Measurement &z) : _self(z._self), _time(z._time) { };

                const TVectorD &operator()() const { return _self; }
                const Measurement &operator=(const Measurement &x) 
                {
                    this->_self.ResizeTo(x._self); 
                    this->_self = x._self;
                    
                    this->_time  = x._time;
                    return *this;
                }
                
                TVectorD _self; // Observation state
                double   _time; // Time

                void Print() const
                {
                    std::cout << "Observation state";
                    std::cout << "(t = " << _time << ")";
                    _self.Print();
                }
            };

            struct State
            {
                State(int n = 0) : _self(n), _time(NAN), _cov(n,n) {};
                State(int m, double *x, double _time = NAN) : _self(m, x), _time(_time) {};
                State(const TVectorD &x, double _time = NAN) : _self(x.GetNrows()), _time(_time), _cov(x.GetNrows(), x.GetNrows()) {};
                State(const std::vector<double> &x, double _time = NAN) : _self(x.size(), &x[0]), _time(_time), _cov(x.size(), x.size()) {};
                State(const State &x) : _self(x._self), _time(x._time), _cov(x._cov) { };
                
                const TVectorD &operator()() const { return _self; }
                const State &operator=(const State &x) 
                {
                    this->_self.ResizeTo(x._self);
                    this->_self = x._self;

                    this->_cov.ResizeTo(x._cov);
                    this->_cov = x._cov;

                    this->_time = x._time;
                    return *this;
                }

                TVectorD _self; // State
                TMatrixD _cov;  // Covariance

                double   _time; // Time


                void Print() const
                {
                    std::cout << "Measurement state";
                    std::cout << "(t = " << _time << ")";
                    _self.Print();

                    std::cout << "Covariant matrix";
                    std::cout << "(t = " << _time << ")";
                    _cov.Print();
                }

            };

        protected:

            // Time step (if applied)
            double timeStep = NAN;

            // Matrices declaration for computation
            TMatrixD A;  // State-transition model - System dynamics matrix(n,n) relates the state at the previous _time step (k-1)
            TMatrixD At; 
            TMatrixD B;  // Control-input model (l,n) - Applied to the control vector uk; (often null)
            TMatrixD Bt; 
            TMatrixD H;  // Observation noise model (m,n) - Maps the true state space into the observed space
            TMatrixD Ht;

            // Control-input vector (l size)
            TVectorD u; 

            // Kalman coefficient matrix
            TMatrixD K;  
            // Identity matrix
            TMatrixD I;  

            // Last estimate
            State xk; /* handling P(n,n), i.e Estimate error covariance*/
            State wk; /* handling Q(n,n), i.e Process noise covariance matrix */
            State vk; /* handling R(m,m), i.e Measurement/observed noise covariance matrix */

            // Last measurement
            Measurement zk;

        protected:
            inline void Initialize(const TMatrixD& A, const TMatrixD& H) { return Initialize(A, H, TMatrixD(A.GetNrows(), 1), TVectorD(1)); };
                   void Initialize(const TMatrixD& A, const TMatrixD& H, const TMatrixD& B, const TVectorD& u);

        public:

            TKalmanFilter() {};
            TKalmanFilter(const TMatrixD& A, const TMatrixD& H): TKalmanFilter(A, H, TMatrixD(A.GetNrows(), 1), TVectorD(1)) { }
            TKalmanFilter(const TMatrixD& A, const TMatrixD& H, const TMatrixD& B, const TVectorD& u) { this->Initialize(A, H, B, u); }

            virtual ~TKalmanFilter();

            void First(double t0, const TVectorD& x0, const TMatrix &Q, const TMatrix &R, double timeStep = NAN);
            void First(double t0, const TVectorD& x0, const State &wk0, const State &vk0, double timeStep = NAN);
            void First(const State &xk0,              const TMatrix &Q, const TMatrix &R, double timeStep = NAN);
            void First(const State &xk0,              const State &wk0, const State &vk0, double timeStep = NAN);
            
            inline void Reset()
            {
                this->u = 0;

                this->xk._self = 0;
                this->vk._self = 0;
                this->wk._self = 0;
                
                this->zk._self = 0;
            }

            inline State Update(const TVectorD &z, double _time, const TMatrixD &A, const TMatrixD &B) { this->A = A; this->B = B; return this->Update(z, _time); }
            inline State Update(const TVectorD &z, double _time = NAN             ) { return this->Update(Measurement(z, _time)); }
            inline State Update(const Measurement &zk,           const TMatrixD &A, const TMatrixD &B) { this->A = A; this->B = B; return this->Update(zk); }
                   State Update(const Measurement &zk);

            void Print(Option_t *opt = "");
            int GetN() { return H.GetNcols(); }
            int GetM() { return H.GetNrows(); }

            TMatrixD Gain();

            ClassDef(ROOT::Signal::TKalmanFilter, 1);
    };
}}
