#pragma once

#include <TString.h>
#include "ROOT/IOPlus/Complex.h"

// Generic enum types used by Signal library
namespace ROOT {
    namespace Signal {

//        #using namespace ::ROOT::IOPlus;

        static inline void str2enum() { std::invalid_argument("Invalid argument converting string to enum"); }
        static inline void enum2str() { std::invalid_argument("Invalid argument converting enum to string"); }

        enum class Domain {Time, Frequency, ComplexFrequency};
        TString enum2str(Domain domain);

        enum class ComplexPart   {Real, Imag, Magnitude, Phase, Decibel, Argument};
        TString enum2str(ComplexPart part);

        enum class Filter {Analog = false, Digital = true};
        TString enum2str(Filter type);

        enum class FilterType {None, LowShelf, HighShelf, Notch, Peak, AllPass, LowPass, HighPass, BandPass, BandStop};
        TString enum2str(FilterType filter);

        enum class FilterTransform {
            GeneralizedBilinear, GBT,
            Bilinear, Tustin,
            ForwardEuler, Euler, 
            BackwardEuler, Backward, 
            ZeroOrderHold, ZOH, 
            FirstOrderHold, FOH,
            ImpulseResponse, Impulse
        };

        enum class FilterDesign {None, Butterworth, Elliptic, ChebyshevI, ChebyshevII, Bessel};
        TString enum2str(FilterDesign design);

        static FilterDesign str2filter(TString str);

        enum class Method { Direct, DFT, Polyphase };
        TString enum2str(Method m);

        static Method str2method(TString str);

        enum class Trend{None, Constant, Linear, Quadratic, QuadraticConcave, QuadraticConvex, Logarithmic, Hyperbolic};
        TString enum2str(Trend detrend);
        
        enum class Average{Mean, Median};
        TString enum2str(Average average);

        enum class Normalizer: char {None, Unitary = 'u', Scalar = 's', Orthonormal = 'o', Backward = 'b', Forward = 'f'};
        TString enum2str(Normalizer op);

        enum class Direction {Forward, ForwardBackward, Backward, BackwardForward, Both, Orthonormal};
        TString enum2str(Direction direction);

        enum class Estimator {Correlation, Periodogram, ModifiedPeriodogram, Welch, Bartlett, BlackmanTukey, LombScargle};
        TString enum2str(Estimator estimator);

        enum class SpectrumProperty{OneSided, TwoSided};
        TString enum2str(SpectrumProperty spectrum);

        enum class Transform {Standard, Fourier, Laplace, Z, Hilbert, HilbertHuang};
        TString enum2str(Transform transform);

        enum class WindowType {None, Zeros, ZeroPad, Unit, Bartlett, BartlettHann, Blackman, BlackmanHarris, PlanckTaper, PlanckTaperLeft, PlanckTaperRight,
                    Bohman, Cosine, Sine, FlatTop, Tukey, Gaussian, Hamming, Hann, Kaiser, Sawtooth, Square,
                    Lanczos, Rectangular, Rectangle, Triangular, Triangle};

        static WindowType str2window(TString str);
        TString enum2str(WindowType window);

        enum class WindowSymmetry { symmetric, periodic };
        static WindowSymmetry str2window_symmetry(TString str);
        TString enum2str(WindowSymmetry symmetry);

        std::ostream& operator<<(std::ostream& os, const Domain& x);
        std::ostream& operator<<(std::ostream& os, const ComplexPart& x);
        std::ostream& operator<<(std::ostream& os, const Filter& x);
        std::ostream& operator<<(std::ostream& os, const FilterType& x);
        std::ostream& operator<<(std::ostream& os, const FilterDesign& x);
        std::ostream& operator<<(std::ostream& os, const Method& x);
        std::ostream& operator<<(std::ostream& os, const Trend& x);
        std::ostream& operator<<(std::ostream& os, const Average& x);
        std::ostream& operator<<(std::ostream& os, const Direction& x);
        std::ostream& operator<<(std::ostream& os, const Estimator& x);
        std::ostream& operator<<(std::ostream& os, const SpectrumProperty& x);
        std::ostream& operator<<(std::ostream& os, const Transform& x);
        std::ostream& operator<<(std::ostream& os, const WindowType& x);
        std::ostream& operator<<(std::ostream& os, const WindowSymmetry& x);
    }
}
