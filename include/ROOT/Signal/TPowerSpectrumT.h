#pragma once

#include <Riostream.h>
#include "TFrequencySeriesT.h"
#include "TH2.h"

namespace ROOT {
    namespace Signal {

        template <typename Element, typename = 
                  typename std::enable_if<std::is_same<Element, float>::value || std::is_same<Element, double>::value || std::is_same<Element, int>::value>::type>
        class TPowerSpectrumT: public TFrequencySeriesT<Element> {

            public:
                TPowerSpectrumT();
                ~TPowerSpectrumT();

            ClassDef(TPowerSpectrumT, 1);
        };
    }
}
