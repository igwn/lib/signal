#pragma once

#include <Riostream.h>
#include <vector>
#include <complex>
#include "TH1D.h"

namespace ROOT {
    namespace Signal {

        template <typename Element = double>
        class TTimeFrequencyMap: public TTensorT<Element> {

            public:
                TTimeFrequencyMap();
                TTimeSeriesT(Double_t deltaTime, Double_t epoch, const std::vector<Element>& values, const char* unit = "", const char* label = "");

                virtual ~TTimeFrequencyMap();

                void Resize(UInt_t newSize);
                
            ClassDef(TTimeFrequencyMap, 1);
        };
    }
}
