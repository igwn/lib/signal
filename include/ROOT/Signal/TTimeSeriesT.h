#pragma once

#include <Riostream.h>
#include "EnumTypes.h"
#include <vector>
#include <complex>
#include "TH1D.h"

#include "TKFRLegacy.h"

namespace ROOT {
    namespace Signal {

        template <typename Element>
        class TTimeSeriesT: public TTensorT<Element> {

            public:

                TTimeSeriesT();
                TTimeSeriesT(Double_t deltaTime, Double_t epoch, const std::vector<Element>& values, const char* unit = "", const char* label = "");
                virtual ~TTimeSeriesT();

                Double_t GetDeltaTime() const;
                Double_t GetEpoch() const;
                Double_t GetHeterodyneFrequency() const;
                UInt_t GetN() const;
                Double_t GetLength() const;
                const char* GetUnitY() const;
                const char* GetLabelY() const;

                TH1* GetHistogram(ComplexPart part, const char* name, const char* title) const;

                void Resize(UInt_t newSize);
                void Clear();

                void Print(Option_t* option = "") const;
¥
            protected:
                Double_t fDeltaTime;
                Double_t tEpoch;
 
                Double_t f0;
                const char* unitY;
                const char* labelY;

            ClassDef(TTimeSeriesT, 1);
        };
    }
}