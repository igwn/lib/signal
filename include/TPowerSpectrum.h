#pragma once

#include "ROOT/Signal/TSpectrogramT.h"

class TPowerSpectrum : public ROOT::Signal::TPowerSpectrumT<double> {

    using TPowerSpectrumT<double>::TPowerSpectrumT;

    ClassDef(TPowerSpectrum, 1);  // ROOT Class Definition
};