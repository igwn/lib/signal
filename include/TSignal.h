#pragma once

#include <Riostream.h>
#include <TString.h>
#include <TObject.h>
#include <TMatrixT.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFormula.h>
#include <Math/Interpolator.h>
#include <Math/Polynomial.h>

#include <Math/Polynomial.h>
#include <TComplex.h>
#include <TMatrixDEigen.h>
#include <TDecompLU.h>

#include <thread>
#include <complex>
#include <vector>
#include <map>

#include "ROOT/Signal/EnumTypes.h"
#include "ROOT/Signal/TransferTypes.h"

template <typename T = double>
class TSignal : public ROOT::Signal::TKFR<T> {

public:

        static bool IsAlreadyRunningMT;
        static std::atomic<int> finishedWorkers;

        TSignal();
        ~TSignal();

        const char *Version();

        ClassDef(TSignal, 1);
};

R__EXTERN TSignal *gSignal;