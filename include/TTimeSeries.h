#pragma once

#include "ROOT/Signal/TTimeSeriesT.h"

namespace ROOT {
    namespace Signal {

        class TTimeSeries : public TTimeSeriesT<double> {

            public:
                using TTimeSeriesT<double>::TTimeSeriesT;

            ClassDef(TTimeSeries, 1);  // ROOT Class Definition
        };
    }
}
