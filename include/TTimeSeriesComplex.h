#pragma once

#include "ROOT/Signal/TTimeSeriesT.h"

namespace ROOT {
    namespace Signal {

        class TTimeSeriesComplex : public TTimeSeriesT<std::complex<double>> {

            public:
                using TTimeSeriesT<std::complex<double>>::TTimeSeriesT;

            ClassDef(TTimeSeriesComplex, 1);  // ROOT Class Definition
        };
    }
}
