/**
 **********************************************
 *
 * \file libRSignal.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__
#include "libRSignal.Config.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ namespace std+;

#pragma link C++ namespace ROOT::Signal+;
#pragma link C++ enum class ROOT::Signal::ComplexProperty+;
#pragma link C++ enum class ROOT::Signal::Transform+;
#pragma link C++ enum class ROOT::Signal::Domain+;
#pragma link C++ enum class ROOT::Signal::Filter+;
#pragma link C++ enum class ROOT::Signal::Direction+;
#pragma link C++ enum class ROOT::Signal::Output+;
#pragma link C++ enum class ROOT::Signal::Scale+;
#pragma link C++ enum class ROOT::Signal::Detrend+;
#pragma link C++ enum class ROOT::Signal::Window+;
#pragma link C++ enum class ROOT::Signal::WindowSymmetry+;

#pragma link C++ class ROOT::Signal::TKFRLegacy<float>+;
#pragma link C++ class ROOT::Signal::TKFRLegacy<double>+;
#pragma link C++ global gKFRLegacy;

#pragma link C++ class ROOT::Signal::TKFR<float>+;
#pragma link C++ class ROOT::Signal::TKFR<double>+;
#pragma link C++ global gKFR;

#pragma link C++ class ROOT::Signal::TTimeSeriesT<float>+;
#pragma link C++ class ROOT::Signal::TTimeSeriesT<double>+;
#pragma link C++ class ROOT::Signal::TTimeSeries+;

#pragma link C++ class ROOT::Signal::TFrequencySeriesT<float>+;
#pragma link C++ class ROOT::Signal::TFrequencySeriesT<double>+;
#pragma link C++ class ROOT::Signal::TFrequencySeries+;

#pragma link C++ class ROOT::Signal::TFrequencySeriesT<float>+;
#pragma link C++ class ROOT::Signal::TFrequencySeriesT<double>+;
#pragma link C++ class ROOT::Signal::TFrequencySeries+;

#pragma link C++ class ROOT::Signal::TSpectrogramT<float>+;
#pragma link C++ class ROOT::Signal::TSpectrogramT<double>+;
#pragma link C++ class ROOT::Signal::TSpectrogram+;

#pragma link C++ class      ROOT::Signal::TComplexFilter+;
#pragma link C++ enum class ROOT::Signal::TComplexFilter::FilterDomain+;
#pragma link C++ enum class ROOT::Signal::TComplexFilter::FilterTransform+;

#pragma link C++ class ROOT::Signal::TComplexPlan+;
#pragma link C++ class ROOT::Signal::TPoleZeroMap+;
#pragma link C++ class ROOT::Signal::TBiquadFilter+;
#pragma link C++ class ROOT::Signal::TKalmanFilter+;

#endif

