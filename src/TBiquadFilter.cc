/**
 **********************************************
 *
 * \file TBiquadFilter.cc
 * \brief Source code of the TBiquadFilter class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "ROOT/Signal/DSP/TBiquadFilter.h"
ClassImp(ROOT::Signal::TBiquadFilter)

namespace ROOT { namespace Signal {

    TBiquadFilter::TBiquadFilter(Filter filter, double k, TQuad B, TQuad A)
    : TComplexFilter(filter, k,
        std::vector<double>({B.x0,B.x1,B.x2}), 
        std::vector<double>({A.x0,A.x1,A.x2})) { }
}}