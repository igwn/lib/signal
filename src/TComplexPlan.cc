/**
 **********************************************
 *
 * \file TComplexPlan.cc
 * \brief Source code of the TComplexPlan class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <ROOT/Signal/DSP/TComplexPlan.h>
ClassImp(ROOT::Signal::TComplexPlan)

namespace ROOT { namespace Signal {

        TComplexPlan::TComplexPlan(const char *name, const char *formula, Double_t xmin, Double_t xmax, Double_t ymin, Double_t ymax, Option_t * opt)
        : TF2(name, TComplexPlan::Regex(formula).Data(), xmin, xmax, ymin, ymax, opt), unit(1), sectors(4)
        {
                this->SetTitle(std::string("|"+std::string(formula)+"|").c_str());
                this->GetXaxis()->SetTitle(Form("Re(z)"));
                this->GetYaxis()->SetTitle(Form("Im(z)"));
        }

        void TComplexPlan::SetUnit(double unit)
        {
                this->unit = unit;
        }

        void TComplexPlan::SetNsectors(int sectors)
        {
                this->sectors = TMath::Max(4, sectors);
        }

        void TComplexPlan::DrawContour(Option_t *option)
        {
                this->SetLineColor(kBlack);
                this->SetLineStyle(2);
                this->SetLineWidth(1);

                TF2::Draw(option);
        }

        void TComplexPlan::DrawUnitCircle(int level)
        {
                double x0 = this->GetXaxis()->GetXmax() - this->GetXaxis()->GetXmin();
                double y0 = this->GetYaxis()->GetXmax() - this->GetYaxis()->GetXmin();

                double Nunits = (level > 1 ? TMath::Max(x0,y0)/2 : 1) /this->unit;
                for(int i = 0; i < Nunits; i++) {

                        const Int_t n = 100;
                        Double_t x[n], y[n];

                        for (Int_t j=0;j<n;j++) {

                                x[j] = this->unit*(i+1) * TMath::Cos(2*TMath::Pi()*j/n);
                                y[j] = this->unit*(i+1) * TMath::Sin(2*TMath::Pi()*j/n);
                        }

                        x[n-1] = this->unit*(i+1);
                        y[n-1] = 0;

                        TGraph *gr = new TGraph(n,x,y);
                                gr->Draw("SAME");
                                gr->SetLineStyle(2);
                                gr->SetMarkerStyle(0);
                }
        }

        void TComplexPlan::DrawSectors(int level)
        {
                double x = this->GetXaxis()->GetXmax()-this->GetXaxis()->GetXmin();
                double x2 = x*x;

                double y = this->GetYaxis()->GetXmax()-this->GetYaxis()->GetXmin();
                double y2 = y*y;

                double sectors = (level > 2 ? this->sectors : 4);
                for(int i = 0; i < sectors; i++) {

                        double r = TMath::Sqrt(x2 + y2);
                        double cos = TMath::Cos(2*TMath::Pi()*i/sectors);
                        double sin = TMath::Sin(2*TMath::Pi()*i/sectors);
                        
                        TGraph *gr = new TGraph(2, &std::vector<double>({0,r*cos})[0], &std::vector<double>({0,r*sin})[0]);
                                gr->Draw("SAME");
                                gr->SetLineStyle(2);
                                gr->SetMarkerStyle(0);
                }
        }

        void TComplexPlan::Draw(Option_t *option, int level)
        {
                TString opt = option;
                if(!opt.EqualTo("") && !opt.EqualTo("SAME")) TF2::Draw(option); // needed to draw axis
                else DrawContour(option);

                if(level > 0) {
                        this->DrawUnitCircle(level);
                        this->DrawSectors(level);
                }
        }

}}