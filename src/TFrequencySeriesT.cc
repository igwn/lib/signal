#include "ROOT/Signal/TFrequencySeriesT.h"

ClassImp(ROOT::Signal::TFrequencySeriesT<double>)
ClassImp(ROOT::Signal::TFrequencySeriesT<float>)

namespace ROOT {
    namespace Signal {
                
        template <typename T>
        TFrequencySeriesT<T>::TFrequencySeriesT() 
        {
            // Default constructor initializes delta time to 1 second, epoch to 0, f0 to 0, and unitY and labelY to an empty string
        }

        template <typename T>
        TFrequencySeriesT<T>::~TFrequencySeriesT() {
            // Destructor
        }
    }
}

template class ROOT::Signal::TFrequencySeriesT<float>;
template class ROOT::Signal::TFrequencySeriesT<double>;