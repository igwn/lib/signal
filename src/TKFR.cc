#include "ROOT/Signal/Impl/TKFRImpl.h"

TKFR *gKFR = new TKFR();

ClassImp(ROOT::Signal::TKFR<double>)
ClassImp(ROOT::Signal::TKFR<float>)
    
#if KFR_VERSION_MAJOR < 5
    #define KFR__EXPRESSION_HANDLE kfr::expression_pointer
    #define KFR__TO_HANDLE kfr::to_pointer
#else
    #define KFR__EXPRESSION_HANDLE kfr::expression_handle
    #define KFR__TO_HANDLE kfr::to_handle
#endif

template<typename T>
bool ROOT::Signal::TKFR<T>::IsAlreadyRunningMT = false;
template<typename T>
std::atomic<int> ROOT::Signal::TKFR<T>::finishedWorkers = 0;
template<typename T>
const std::complex<T> ROOT::Signal::TKFR<T>::i = std::complex<T>(0.0,1.0);

namespace ROOT {
        namespace Signal {

                template <typename T>
                TKFR<T>::TKFR() {

                        this->pImpl = new TKFR<T>::Impl();
                        if constexpr (std::is_same<T, double>::value) {
                                if(gKFR == nullptr) gKFR = this;
                        }
                }

                template <typename T>
                TKFR<T>::~TKFR() {

                        if(this->pImpl) {
                        
                                delete this->pImpl;
                                this->pImpl = NULL;
                        }

                        if constexpr (std::is_same<T, double>::value) {
                                if(gKFR == this) gKFR = nullptr;
                        }
                }

                template <typename T>
                const char *TKFR<T>::Version()
                {
                        return this->pImpl->Version();
                }
        }
}

template class ROOT::Signal::TKFR<float>;
template class ROOT::Signal::TKFR<double>;
