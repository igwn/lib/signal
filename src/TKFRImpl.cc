#include "ROOT/Signal/Impl/TKFRImpl.h"

namespace ROOT { namespace Signal {

        template <typename T>
        TKFR<T>::Impl::~Impl()
        {
                kfr::dft_cache::instance().clear();
        }

        template <typename T>
        const char *TKFR<T>::Impl::Version()
        {
                return kfr::library_version_dft();
        }

        template <typename T>
        typename TKFR<T>::TBiquad TKFR<T>::Impl::stdbiquad(const kfr::biquad_params<T> &params)
        { 
                return TBiquad(std::make_tuple(params.b0, params.b1, params.b2), std::make_tuple(params.a0, params.a1, params.a2));
        }

        template <typename T>
        typename kfr::biquad_params<T> TKFR<T>::Impl::kfrbiquad(const TKFR<T>::TBiquad &params)
        { 
                kfr::biquad_params<T> _params;
                _params.b0 = params.B.x0;
                _params.b1 = params.B.x1;
                _params.b2 = params.B.x2;

                _params.a0 = params.A.x0;
                _params.a1 = params.A.x1;
                _params.a2 = params.A.x2;

                return _params;
        }

        template<typename T>
        kfr::zpk<T> TKFR<T>::Impl::kfrzpk(const ZPK &zpk)
        {
                kfr::zpk<T> _zpk;
                                _zpk.z = kfrvector(zpk.zeros);
                                _zpk.p = kfrvector(zpk.poles);
                                _zpk.k = zpk.gain;

                return _zpk;
        }

        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::stdzpk(const kfr::zpk<T> &zpk)
        {
                ZPK _zpk;
                        _zpk.zeros = stdvector(zpk.z);
                        _zpk.poles = stdvector(zpk.p);
                        _zpk.gain = zpk.k;

                return _zpk;
        }

        template<typename T>
        std::vector<kfr::biquad_params<T>> TKFR<T>::Impl::kfrsos(const SOS &sos)
        {
                std::vector<kfr::biquad_params<T>> _sos(sos.size(), kfr::biquad_params<T>());
                for(int i = 0, N = sos.size(); i < N; i++) {

                        _sos[i].b0 = sos[i].B.x0;
                        _sos[i].b1 = sos[i].B.x1;
                        _sos[i].b2 = sos[i].B.x2;
                        
                        _sos[i].a0 = sos[i].A.x0;
                        _sos[i].a1 = sos[i].A.x1;
                        _sos[i].a2 = sos[i].A.x2;
                }

                return _sos;
        }

        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::stdsos(const std::vector<kfr::biquad_params<T>> &sos)
        {
                SOS _sos(sos.size(), TBiquad());
                for(int i = 0, N = sos.size(); i < N; i++)
                        _sos[i] = stdbiquad(sos[i]);

                return _sos;
        }

        template <typename T>
        kfr::univector<T> TKFR<T>::Impl::kfrvector(const Vector1N &u)
        {
                kfr::univector<T> v(u.size());
                for(int i = 0, N = u.size(); i < N; i++)
                        v[i] = u[i];

                return v;
        }

        template <typename T>
        kfr::univector<std::complex<T>> TKFR<T>::Impl::kfrvector(const Vector1C &u)
        {
                kfr::univector<std::complex<T>> v(u.size());
                for(int i = 0, N = u.size(); i < N; i++)
                        v[i] = u[i];

                return v;
        }

        template <typename T>
        kfr::window_symmetry TKFR<T>::Impl::kfrsymmetry(const WindowSymmetry &symmetry)
        {
                switch(symmetry) {

                        case WindowSymmetry::symmetric  : return kfr::window_symmetry::symmetric;
                        case WindowSymmetry::periodic   : return kfr::window_symmetry::periodic;

                        default: enum2str();
                }      
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::stdvector(const kfr::univector<NumericT> &u)
        {
                Vector1N v;
                for(int i = 0, N = u.size(); i < N; i++)
                        v.push_back(u[i]);

                return v;
        }


        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::stdvector(const kfr::univector<ComplexT> &u)
        {
                Vector1C v;
                for(int i = 0, N = u.size(); i < N; i++)
                        v.push_back(u[i]);

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::stdvector(const TVectorT<NumericT> &v)
        {
                return Vector1N(v.GetMatrixArray(), v.GetMatrixArray() + v.GetNrows());
        }

        template <typename T>
        TVectorT<typename TKFR<T>::NumericT> TKFR<T>::Impl::rootvector(const Vector1N &v)
        {
                TVectorT<NumericT> rootv(v.size());
                for(int i = 0, N = v.size(); i < N; i++)
                        rootv(i) = v[i];

                return rootv;
        }

        template <typename T>
        TH1* TKFR<T>::Impl::ScaleX(TH1 *h, double scaleX, Option_t *option)
        {
                if(h == NULL) return NULL;

                TString opt = option;
                        opt.ToLower();

                TAxis *axis = NULL;
                if(opt.Contains("x")) axis = h->GetXaxis();
                else if(opt.Contains("y")) axis = h->GetYaxis();
                else if(opt.Contains("z")) axis = h->GetZaxis();
                if(axis == NULL) {

                        TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                        return NULL;
                }

                axis->Set(axis->GetNbins(), axis->GetXmin()*scaleX, axis->GetXmax()*scaleX);
                // if(opt.Contains("x")) {

                //         h->SetBins(axis->Get, axis->GetXmin(), axis->GetXmax()*scaleX);

                // } else if(opt.Contains("y")) {

                //         h->SetBins(h->GetXaxis()->GetNbins(), h->GetXaxis()->GetXmin(), h->GetXaxis()->GetXmax(), 
                //                 axis->Get, axis->GetXmin(), axis->GetXmax()*scaleX
                //         );

                // } else if(opt.Contains("z")) {

                //         h->SetBins(h->GetXaxis()->GetNbins(), h->GetXaxis()->GetXmin(), h->GetXaxis()->GetXmax(), 
                //                 h->GetYaxis()->GetNbins(), h->GetYaxis()->GetXmin(), h->GetYaxis()->GetXmax(), 
                //                 axis->Get, axis->GetXmin(), axis->GetXmax()*scaleX
                //         );

                // } else {

                //         TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //         return NULL;
                // }

                return h;
        }

        template <typename T>
        TH1* TKFR<T>::Impl::ScaleY(TH1 *h, double scaleY)
        {
                if(h == NULL) return NULL;
                
                h->Scale(scaleY);
                return h;
        }
        
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::SymmetricResize(Vector1N v, int n)
        {
                int n0 = v.size();
                if (n > n0) {

                        v.resize(n, 0);
                        std::rotate(v.begin(), v.begin() + n0 + (n-n0)/2, v.end());

                } else {

                        std::rotate(v.begin(), v.end() - (n-n0)/2, v.end());
                        v.resize(n, 0);
                }

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::SymmetricResize(Vector1C v, int n)
        {
                if (n > v.size()) {

                        v.resize(n);
                        std::rotate(v.begin(), v.end() + n/2, v.end());

                } else {

                        std::rotate(v.begin(), v.end() - n/2, v.end());
                        v.resize(n);
                }

                return v;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Real(const ComplexT &z) { return z.real(); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Real(const Vector1C &z)
        {
                Vector1N v;
                for(int i = 0, N = z.size(); i < N; i++)
                        v.push_back(z[i].real());

                return v;
        }
        template <typename T>
        typename TKFR<T>::Vector2N TKFR<T>::Impl::Real(const Vector2C &z)
        {
                Vector2N v;
                for(int i = 0, N = z.size(); i < N; i++)
                        v.push_back(Real(z[i]));

                return v;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Imag(const ComplexT &z) { return z.imag(); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Imag(const Vector1C &z)
        {
                Vector1N v;
                for(int i = 0, N = z.size(); i < N; i++)
                        v.push_back(z[i].imag());

                return v;
        }
        template <typename T>
        typename TKFR<T>::Vector2N TKFR<T>::Impl::Imag(const Vector2C &z)
        {
                Vector2N v;
                for(int i = 0, N = z.size(); i < N; i++)
                        v.push_back(Imag(z[i]));

                return v;
        }

        template <typename T>
        TH1* TKFR<T>::Impl::Empty(TH1 *h0)
        {
                for(int i = 0, N = h0->GetNcells(); i < N; i++) {

                        h0->SetBinContent(i,0);
                        h0->SetBinError(i,0);
                }

                h0->SetEntries(0);
                return h0;
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Complex(const NumericT &a, const NumericT &b) { return ComplexT(a, b); }
        template <typename T>
        typename TKFR<T>::Vector2C TKFR<T>::Impl::Complex(const Vector2N &a, const Vector2N &b)
        {
                Vector2C v;
                Vector1N e = {};
                for(unsigned int i = 0, N = TMath::Max(a.size(), b.size()); i < N; i++) {
                        v.push_back(Complex(i < a.size() ? a[i] : e, i < b.size() ? b[i] : e));
                }

                return v;
        }
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Complex(const Vector1N &_a, const Vector1N &_b)
        {
                int N = TMath::Max(_a.size(), _b.size());

                Vector1N a = _a, b = _b;
                if(a.size() != N) a.resize(N);
                if(b.size() != N) b.resize(N);

                Vector1C v(N);
                std::transform(std::execution::par_unseq, a.begin(), a.end(), b.begin(), v.begin(), [](const NumericT& real, const NumericT &imag) {
                        return ComplexT(real, imag);
                });

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Minus(const Vector1C &a) { 
                Vector1C output(a.size());
                std::transform(std::execution::par_unseq, a.begin(), a.end(), output.begin(), [](const ComplexT &x) { return -x; });
                return output;
        }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Minus(const Vector1N &a) {
                Vector1N output(a.size());
                std::transform(std::execution::par_unseq, a.begin(), a.end(), output.begin(), [](const NumericT &x) { return -x; });
                return output;
        }
        
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Negate(const Vector1C &x) { return Minus(x); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Negate(const Vector1N &v) { return Minus(v); }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Power(const Vector1C &X, const NumericT &y, bool handmade) {
                Vector1C output(X.size());
                std::transform(std::execution::seq, X.begin(), X.end(), output.begin(), [this, &y, &handmade](const ComplexT &x) { return Power(x, y, handmade); });
                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Power(const Vector1N &X, const NumericT &y, bool handmade) {
                Vector1N output(X.size());
                std::transform(std::execution::par_unseq, X.begin(), X.end(), output.begin(), [this, &y, &handmade](const NumericT &x) { return Power(x, y, handmade); });
                return output;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Power(const NumericT &x, const NumericT &y, bool handmade) {
                
                if(!handmade) return std::pow(x, y); 

                if (ROOT::IOPlus::Helpers::EpsilonEqualTo(y, (T) 0.0)) return (T) 1.0;
                else if (y < 0) return (T) 1.0 / Power(x, -y, handmade);
                
                T pow = 1.0;
                for(int i = 0; i < (int) y; i++)
                        pow *= x;

                return pow;

        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Power(const ComplexT &x, const NumericT &y, bool handmade) { 
                
                if(!handmade) return std::pow(x, y); 

                if (ROOT::IOPlus::Helpers::EpsilonEqualTo(y, (T) 0.0)) return (T) 1.0;
                else if (y < 0) return (T) 1.0 / Power(x, -y, handmade);
                
                ComplexT pow = std::complex<T>(1.0, 0.0);
                for(int i = 0; i < (int) y; i++)
                        pow *= x;

                return pow;
        }

        template <typename T>
        TH1* TKFR<T>::Impl::PowerX(TH1 *_h0, double X)
        {
                if(_h0 == NULL) return NULL;
                
                TH1 *h0 = (TH1*) _h0->Clone((TString) + _h0->GetName()+":x^("+TString::Itoa(X,10)+")");
                for(int i = 0, N = h0->GetNcells(); i < N; i++) {

                        if(h0->IsBinOverflow(i)) continue;
                        if(h0->IsBinUnderflow(i)) continue;

                        double content = TMath::Power(h0->GetBinContent(i), X);
                        double  sigma  = X * TMath::Power(h0->GetBinContent(i), X-1) * h0->GetBinError(i);

                        h0->SetBinContent(i, ROOT::IOPlus::Helpers::IsInf(content) || ROOT::IOPlus::Helpers::IsNaN(content) || ROOT::IOPlus::Helpers::IsInf(sigma) || ROOT::IOPlus::Helpers::IsNaN(sigma) ? 0 : content);
                        h0->SetBinError  (i, ROOT::IOPlus::Helpers::IsInf(content) || ROOT::IOPlus::Helpers::IsNaN(content) || ROOT::IOPlus::Helpers::IsInf(sigma) || ROOT::IOPlus::Helpers::IsNaN(sigma) ? 0 : sigma);
                }

                return h0;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Sqrt(const Vector1C &X) { return Power(X, 1/2.); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Sqrt(const Vector1N &X) { return Power(X, 1/2.); }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Sqrt(const NumericT &x) { return Power(x, 1/2.); }
        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Sqrt(const ComplexT &X) { return Power(X, 1/2.); }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Conj(const ComplexT &z) { return std::conj(z); }
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Conj(const Vector1C &z) { 
        
                Vector1C c(z.size());
                std::transform(std::execution::par_unseq, z.begin(), z.end(), c.begin(), [](const ComplexT &z) { return std::conj(z); });
                return c;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Mag2(const Vector1C &z) { 
        
                Vector1N c(z.size());
                std::transform(std::execution::par_unseq, z.begin(), z.end(), c.begin(), [](const ComplexT &z) { return (z.real()*z.real() + z.imag()*z.imag()); });
                return c;
        }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Mag (const Vector1C &z) { 
        
                Vector1N c(z.size());
                std::transform(std::execution::par_unseq, z.begin(), z.end(), c.begin(), [](const ComplexT &z) { return sqrt(z.real()*z.real() + z.imag()*z.imag()); });
                return c;
        }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Abs (const Vector1C &z) { return Mag(z); }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Mag2(const ComplexT &z) { return z.real()*z.real() + z.imag()*z.imag(); }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Mag(const ComplexT &z) { return Sqrt(Mag2(z)); }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Abs(const ComplexT &z) { return Mag(z); }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Mag2(const Vector1N &x) { 
        
                Vector1N c(x.size());
                std::transform(std::execution::par_unseq, x.begin(), x.end(), c.begin(), [](const NumericT &x) { return x*x; });
                return c;
        }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Mag (const Vector1N &x) { 
        
                Vector1N c(x.size());
                std::transform(std::execution::par_unseq, x.begin(), x.end(), c.begin(), [](const NumericT &x) { return sqrt(x*x); });
                return c;
        }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Abs (const Vector1N &x) { return Mag(x); }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Mag2(const NumericT &z) { return z*z; }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Mag(const NumericT &z) { return Sqrt(Mag2(z)); }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Abs(const NumericT &z) { return Mag(z); }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Mag2dB(const Vector1N &mag, const NumericT &mag0, bool powerQuantity)
        {
                NumericT powerFactor = 10 * (powerQuantity ? 1 : 2);

                Vector1N c(mag.size());
                std::transform(std::execution::par_unseq, mag.begin(), mag.end(), c.begin(), [&powerFactor, &mag0](const NumericT &mag) { return powerFactor * TMath::Log10(mag / mag0); });
                return c;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::dB(const Vector1N &z, const NumericT &mag0, bool powerQuantity) { return Mag2dB(Mag(z), mag0, powerQuantity); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::dB(const Vector1C &z, const NumericT &mag0, bool powerQuantity) { return Mag2dB(Mag(z), mag0, powerQuantity); }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::dB(const NumericT &z, const NumericT &mag0, bool powerQuantity) { return Mag2dB(Mag(z), mag0, powerQuantity); }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::dB(const ComplexT &z, const NumericT &mag0, bool powerQuantity) { return Mag2dB(Mag(z), mag0, powerQuantity); }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::dB2Mag(const Vector1N &dB, const NumericT &mag0, bool powerQuantity)
        {
                NumericT powerFactor = 10 * (powerQuantity ? 1 : 2);
                
                Vector1N v;
                for(int i = 0, N = dB.size(); i < N; i++)
                        v.push_back(mag0 * std::pow(10, dB[i] / powerFactor));

                return v;
        }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::dB2Mag(const NumericT &dB, const NumericT &mag0, bool powerQuantity)
        {
                NumericT powerFactor = 10 * (powerQuantity ? 1 : 2);
                return mag0 * std::pow(10, dB / powerFactor);
        }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Mag2dB(const NumericT &mag, const NumericT &mag0, bool powerQuantity)
        {
                NumericT powerFactor = 10 * (powerQuantity ? 1 : 2);
                return powerFactor * TMath::Log10(mag / mag0);
        }

        template<typename T>
        TH1* TKFR<T>::Impl::SetVectorError(TH1* h, Vector1N v, int first)
        {
                for(int i = first, j = 0, N = h->GetNcells(); i < N; i++) {

                        if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                        if(j > (int) v.size()-1) break;

                        h->SetBinError(i, v[j++]);
                }

                return h;
        }

        template<typename T>
        TH1* TKFR<T>::Impl::SetVectorContent(TH1* h, Vector1N v, int first)
        {
                int nEntries = h->GetEntries();
                for(int i = first, j = 0, N = h->GetNcells(); i < N; i++) {

                        if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                        if(j > (int) v.size()-1) break;

                        h->SetBinContent(i, v[j++]);
                }

                h->SetEntries(nEntries);
                return h;
        }

        template<typename T>
        TH1* TKFR<T>::Impl::AddVectorContent(TH1* h, Vector1N v, int c1)
        {        
                if(h == NULL) return NULL;
                
                int nEntries = h->GetEntries();
                for(int i = 0, I = v.size(); i < I; i++) {

                        int bin = h->GetBin(i+1);
                        h->SetBinContent(bin, h->GetBinContent(bin) + c1*v[i]);
                }

                h->SetEntries(nEntries);
                return h;
        }

        template<typename T>
        TH1* TKFR<T>::Impl::AddVectorContent(TH1* h, Vector2N v, int c1)
        {
                if(h == NULL) return NULL;
                
                if(h->InheritsFrom("TH3")) {
                        
                        TPrint::Error(__METHOD_NAME__, "Narrowing of 2D histogram not implemented");
                        return NULL;

                } else if(h->InheritsFrom("TH2")) {

                        for(int i = 0, I = v.size(); i < I; i++) {

                                for(int j = 0, J = v[i].size(); j < J; j++) {
                                
                                        int bin = h->GetBin(i+1, j+1);
                                        h->SetBinContent(bin, h->GetBinContent(bin) + c1*v[i][j]);
                                }
                        }

                } else {
                
                        for(int i = 0, I = v.size(); i < I; i++)
                                AddVectorContent(h, v[i], c1);
                }

                return h;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Argument(const ComplexT &z, const NumericT &offset, bool extend) { return (extend ? TMath::ATan2(z.imag(), z.real()) : TMath::ATan(z.imag()/z.real())) + ROOT::IOPlus::Helpers::MathMod(offset, 2*TMath::Pi()); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Argument(const Vector1C &z, const NumericT &offset, bool extend)
        {
                Vector1N v;
                for(int i = 0, N = z.size(); i < N; i++) {
                        v.push_back(Argument(z[i], offset, extend));
                }

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Unwrap(Vector1N v, NumericT period) 
        { 
                if(ROOT::IOPlus::Helpers::IsNaN(period)) period = 2*TMath::Pi();

                for(int i = 1, N = v.size(); i < N; i++) {
                        
                        NumericT p0 = v[i-1];
                        NumericT p  = v[i];

                        NumericT dd = fmod((p - p0) + period/2., period);
                        
                        if (dd < 0) dd += period;
                        v[i] = p0 + (dd - period/2.);
                }

                return v;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Phase(const ComplexT &z, const NumericT &offset, bool extend) { return Argument(z, offset, extend); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Phase(const Vector1C &z, const NumericT &offset, bool extend, bool unwrap) { return unwrap ? Unwrap(Argument(z, offset, extend)) : Argument(z, offset, extend); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Phase(const Vector1N &v, const NumericT &offset, bool unwrap) { return Add((unwrap ? Unwrap(v) : v), ROOT::IOPlus::Helpers::MathMod(offset, (T) 2*TMath::Pi())); }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Add(const Vector1N &a, const Vector1N &b )
        {
                if(a.size() != b.size())
                        throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

                Vector1N c(a.size());
                std::transform(std::execution::par_unseq, a.begin(), a.end(), b.begin(), c.begin(), std::plus<>());

                return c;
        }
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Add(const Vector1C &z1, const Vector1C &z2 )
        {
                if(z1.size() != z2.size())
                        throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

                Vector1C z(z1.size());
                std::transform(std::execution::par_unseq, z1.begin(), z1.end(), z2.begin(), z.begin(), std::plus<>());

                return z;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Divide(const Vector1N &x, const Vector1N &y, bool discardNaN) 
        { 
                Vector1N c(x.size());
                std::transform(std::execution::par_unseq, x.begin(), x.end(), y.begin(), c.begin(), 
                [&discardNaN](const NumericT& a, const NumericT& b) {

                        NumericT c = a/b;
                        return NumericT(discardNaN && ROOT::IOPlus::Helpers::IsNaN(c) ? 0 : c);
                });

                return c;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Divide(const Vector1C &z1, const Vector1C &z2, bool discardNaN)
        {
                if(z1.size() != z2.size())
                        throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

                Vector1C z(z1.size());
                std::transform(std::execution::par_unseq, z1.begin(), z1.end(), z2.begin(), z.begin(), 
                [&discardNaN](const ComplexT& z1, const ComplexT& z2) {

                        ComplexT z = z1/z2;
                        return ComplexT(
                                discardNaN && ROOT::IOPlus::Helpers::IsNaN(z.real()) ? 0 : z.real(),
                                discardNaN && ROOT::IOPlus::Helpers::IsNaN(z.imag()) ? 0 : z.imag()
                        );
                });

                return z;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Multiply(const Vector1N &a, const Vector1N &b)
        {
                if(a.size() != b.size())
                        throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

                Vector1N c(a.size());
                std::transform(std::execution::par_unseq, a.begin(), a.end(), b.begin(), c.begin(), std::multiplies<>());

                return c;
        }
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Multiply(const Vector1C &z1, const Vector1C &z2)
        {
                if(z1.size() != z2.size())
                        throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

                Vector1C z(z1.size());
                std::transform(std::execution::par_unseq, z1.begin(), z1.end(), z2.begin(), z.begin(), std::multiplies<>());

                return z;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Sum(const Vector1N &v) { 
                
                return std::reduce(std::execution::par_unseq, v.begin(), v.end());
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Sum(const Vector1C &v) { 
                
                return std::reduce(std::execution::par_unseq, v.begin(), v.end());
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::SumXY(const Vector1N &X, const Vector1N &Y) {

                return std::transform_reduce(std::execution::par_unseq, X.begin(), X.end(), Y.begin(), (NumericT) 0.0);
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::SumXY(const Vector1C &X, const Vector1C &Y) {

                return std::transform_reduce(std::execution::par_unseq, X.begin(), X.end(), Y.begin(), (ComplexT) 0.0);
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Subtract(const Vector1N &a, const Vector1N &b )
        {
                if(a.size() != b.size())
                        throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

                Vector1N c(a.size());
                std::transform(std::execution::par_unseq, a.begin(), a.end(), b.begin(), c.begin(), std::minus<>());

                return c;
        }
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Subtract(const Vector1C &z1, const Vector1C &z2 )
        {
                if(z1.size() != z2.size())
                        throw std::invalid_argument("Vectors are not same size: z1("+TString::Itoa(z1.size(),10)+") != z2("+TString::Itoa(z2.size(),10)+")");

                Vector1C z(z1.size());
                std::transform(std::execution::par_unseq, z1.begin(), z1.end(), z2.begin(), z.begin(), std::minus<>());

                return z;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Mean(const Vector1N &V )
        {
                if(V.size() == 0) return 0;
                return std::reduce(std::execution::par_unseq, V.begin(), V.end()) / V.size();
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Mean(const Vector1C &V )
        {
                if(V.size() == 0) return 0;
                return std::reduce(std::execution::par_unseq, V.begin(), V.end()) / ComplexT(V.size());
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Mean(const Vector2N &V)
        {
                Vector1N mean(V.size(), 0);
                for(int i = 0, N = V.size(); i < N; i++) {
                        mean[i] = Mean(V[i]);
                };

                return mean;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Mean(const Vector2C &V )
        {
                Vector1C mean(V.size(), 0);
                for(int i = 0, N = V.size(); i < N; i++) {
                        mean[i] = Mean(V[i]);
                };

                return mean;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::StdDev(const Vector1N &V)
        {
                NumericT stdDev = 0;
                if(V.size() == 0) return stdDev;

                NumericT mean = this->Mean(V);
                for(int i = 0, N = V.size(); i < N; i++)
                        stdDev += std::pow(V[i]-mean, 2);

                return this->Sqrt(mean/((T) V.size()));
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::StdDev(const Vector1C &V )
        {
                ComplexT stdDev = 0;

                if(V.size() == 0) return stdDev;

                ComplexT mean = this->Mean(V);
                for(int i = 0, N = V.size(); i < N; i++)
                        stdDev = stdDev + this->Power(V[i]-mean, 2);

                return this->Sqrt(stdDev/((T) V.size()));
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::StdDev(const Vector2C &V )
        {
                if(V.size() == 0) 
                        return Vector1C({});

                Vector1C mean = this->Mean(V);
                Vector1C Vn(V[0].size(), 0);
                for(int i = 0, N = V.size(); i < N; i++) {

                        if(Vn.size() != V[i].size())
                                throw std::invalid_argument("Vectors are not same size: Vn("+TString::Itoa(Vn.size(),10)+") != V["+TString::Itoa(i,10)+"]("+TString::Itoa(V[i].size(),10)+")");

                        this->Add(Vn, this->Power(this->Subtract(V[i], mean[i]), 2));
                }

                return this->Sqrt(this->Multiply(Vn, 1./V.size()));
        }

        template <typename T>
        TH1* TKFR<T>::Impl::Abs(TH1 *_h0)
        {
                if(_h0 == NULL) return NULL;

                TH1 *h0 = (TH1*) _h0->Clone((TString) + _h0->GetName()+":abs");
                for(int i = 0, N = h0->GetNcells(); i < N; i++) {

                        if(h0->IsBinOverflow(i)) continue;
                        if(h0->IsBinUnderflow(i)) continue;

                        double content = abs(h0->GetBinContent(i));

                        h0->SetBinContent(i, content);
                }

                return h0;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Range(TAxis *axis)
        {
                const Double_t *v = axis->GetXbins()->GetArray();
                if(v == NULL) return Range(axis->GetNbins(), axis->GetXmin(), axis->GetXmax());

                std::vector<T> x(axis->GetNbins()+1, 0);
                for(int i = 0, N = axis->GetNbins(); i <= N; i++)
                        x[i] = v[i];

                return x;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Range(TH1D *h)
        {
                std::vector<T> x;
                for(int i = 0, N = h->GetNbinsX(); i <= N; i++)
                        x.push_back(h->GetBinContent(i+1));

                return x;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Scale(const Vector1C &z, const NumericT &k) 
        { 
                ComplexT mean = this->Mean(z);
                return Transform(z, [&mean, &k](const ComplexT &x) { return k * (x-mean) + mean; });
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Scale(const Vector1N &a, const NumericT &k) 
        {
                NumericT mean = this->Mean(a);
                return Transform(a, [&mean, &k](const NumericT &x) { return k * (x-mean) + mean; });
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Max( const Vector1N &V)
        {
                if(V.size() == 0) 
                        return 0;

                NumericT max = V[0];
                for(int i = 1, N = V.size(); i < N; i++) {
                        max = TMath::Max(max, V[i]);
                }

                return max;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Min( const Vector1N &V)
        {
                if(V.size() == 0) 
                        return 0;

                NumericT min = V[0];
                for(int i = 1, Ni = V.size(); i < Ni; i++) {
                        min = TMath::Min(min, V[i]);
                }

                return min;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Median( const Vector1N &_V)
        {
                if(_V.size() == 0) 
                        return 0;

                Vector1N V = _V;

                NumericT Vi = V.size();
                sort(V.begin(), V.end());

                if (this->MathMod(Vi, 2) == 0) return (V[Vi / 2 - 1] + V[Vi / 2]) / 2;
                return V[Vi / 2];
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Median(const Vector1C &_V)
        {
                if(_V.size() == 0) return 0;

                Vector1C V = _V;
                int Ni = V.size();
                sort(V.begin(), V.end(), sort_complex<T>);

                if (Ni % 2 == 0) return (V[Ni / 2 - 1] + V[Ni / 2]) / ComplexT(2,0);
                return V[Ni / 2];
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Median( const Vector2N &_V, bool transpose)
        {
                if(_V.size() == 0) 
                        return Vector1N({});

                Vector2N V = _V;
                if(transpose) V = Transpose(V);

                Vector1N Vi(V.size(), 0);
                for(int i = 0, Ni = V.size(); i < Ni; i++) {

                        int Nij = V[i].size();
                        sort(V[i].begin(), V[i].end());

                        if (Nij % 2 == 0) Vi[i] = (V[i][Nij / 2 - 1] + V[i][Nij / 2]) / 2;
                        else Vi[i] = V[i][Nij / 2];
                }

                return Vi;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Median( const Vector2C &_V, bool transpose)
        {
                if(_V.size() == 0) 
                        return Vector1C({});

                Vector2C V = _V;
                if(transpose) V = Transpose(V);

                Vector1C Vi(V.size(), 0);
                for(int i = 0, Ni = V.size(); i < Ni; i++) {

                        int Nij = V[i].size();
                        sort(V[i].begin(), V[i].end(), sort_complex<T>);

                        if (Nij % 2 == 0) Vi[i] = (V[i][Nij / 2 - 1] + V[i][Nij / 2]) / ComplexT(2,0);
                        else Vi[i] = V[i][Nij / 2];
                }

                return Vi;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Mode( const Vector1N &V)
        {
                int index = 0;
                NumericT highest = 0;

                for (int a = 0; a < V.size(); a++) {

                        int count = 1;
                        T position = V.at(a);
                        for (int b = a + 1; b < V.size(); b++) {

                                if (ROOT::IOPlus::Helpers::EpsilonEqualTo(V.at(b), position)) count++;
                        }
                        
                        if (count >= index) {

                                index = count;
                                highest = position;
                        }
                }

                return highest;
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Mode( const Vector1C &V)
        {
                int index = 0;
                ComplexT highest = 0;

                for (int a = 0; a < V.size(); a++) {

                        int count = 1;
                        ComplexT position = V.at(a);
                        for (int b = a + 1; b < V.size(); b++) {

                                if (ROOT::IOPlus::Helpers::EpsilonEqualTo(V.at(b), position)) count++;
                        }
                        
                        if (count >= index) {

                                index = count;
                                highest = position;
                        }
                }

                return highest;
                
        }

        template <typename T>
        bool  TKFR<T>::Impl::IsPowerOf(int n, int base) { return TMath::Ceil(TMath::Log(n)/TMath::Log(base)) == TMath::Floor(TMath::Log(n)/TMath::Log(base)); }

        template <typename T>
        int TKFR<T>::Impl::NextPowerOf(int n, int base) { return IsPowerOf(n, base) ? n*base : std::pow(2, TMath::Ceil(TMath::Log(n)/TMath::Log(base))); }

        template <typename T>
        bool TKFR<T>::Impl::ValidSizeDFT(const Vector1C &input)
        {
                return IsPowerOf(input.size(), 2) || IsPowerOf(2*(input.size()-1), 2);
        }

        template <typename T>
        bool TKFR<T>::Impl::IsOneSidedDFT(const Vector1N &input)
        {
                if(input.size() == 0) return true;
                if(input.size() == 1) return true;
                if(input.size() == 2) return true;

                if(IsPowerOf(   input.size()   , 2)) return false;
                if(IsPowerOf(2*(input.size()-1), 2)) return true;

                throw std::invalid_argument("One-sided check requires a power-of-two length (two-sided) input.");
        }
        
        template <typename T>
        bool TKFR<T>::Impl::IsOneSidedDFT(const Vector1C &input)
        {
                if(input.size() == 0) return true;
                if(input.size() == 1) return true;
                if(input.size() == 2) return true;

                if(IsPowerOf(   input.size()   , 2)) return false;
                if(IsPowerOf(2*(input.size()-1), 2)) return true;

                throw std::invalid_argument("One-sided check requires a power-of-two length (two-sided) input.");
        }
        
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::DFT(const Vector1C &input)
        {
                if(input.size() == 0) return {};
                if(input.size() == 1) return input;

                kfr::dft_plan_ptr<T> dft = kfr::dft_cache::instance().get(kfr::ctype_t<T>(), input.size());
                Vector1C output(input.size(), std::numeric_limits<T>::quiet_NaN());
                std::vector<kfr::u8> temp(dft->temp_size);

                dft->execute(&output[0], &input[0], &temp[0]);
                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::iDFT(const Vector1C &input)
        {
                if(input.size() == 0) return {};
                if(input.size() == 1) return input;

                kfr::dft_plan_ptr<T> dft = kfr::dft_cache::instance().get(kfr::ctype_t<T>(), input.size());
                Vector1C output(input.size(), std::numeric_limits<T>::quiet_NaN());
                std::vector<kfr::u8> temp(dft->temp_size);

                dft->execute(&output[0], &input[0], &temp[0], kfr::ctrue);
                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::RealDFT(const Vector1N &input)
        {
                if(input.size() == 0) return {};
                if(input.size() == 1) return Complex(input);
                if(input.size() % 2 != 0) {

                        Vector1C output = DFT( Complex(input) );
                                 output.resize(input.size()/2+1);

                        return output;
                }
                
                kfr::dft_plan_real_ptr<T> dft = kfr::dft_cache::instance().getreal(kfr::ctype_t<T>(), input.size());
                Vector1C output(input.size(), std::numeric_limits<T>::quiet_NaN());
                std::vector<kfr::u8> temp(dft->temp_size);

                dft->execute(&output[0], &input[0], &temp[0]);

                output.resize(input.size() / 2 + 1);
                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::iRealDFT(const Vector1C &input)
        {
                if(input.size() == 0) return {};
                if(input.size() == 1) return Real(input);
                if(input.size() %  2) {

                        Vector1C output = iDFT(MirroringDFT(input));
                                 output.resize(input.size()/2+1);

                        return Real(output);
                }
                return this->Real(iDFT(MirroringDFT(input)));
                
                kfr::dft_plan_real_ptr<T> dft = kfr::dft_cache::instance().getreal(kfr::ctype_t<T>(), (input.size() - 1) * 2);
                Vector1N output((input.size() - 1) * 2, std::numeric_limits<T>::quiet_NaN());
                std::vector<kfr::u8> temp(dft->temp_size);

                dft->execute(&output[0], &input[0], &temp[0], kfr::ctrue);
                std::cout << "INPUT("<< input.size() << ") = " << input << "; OUTPUT("<<output.size()<<") = " << output << std::endl;
                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Rotate(const Vector1N &_v, int n)
        {
                Vector1N v = _v;
                std::rotate(v.begin(), v.begin() + this->MathMod(n, v.size()), v.end());
                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Rotate(const Vector1C &_v, int n)
        {
                Vector1C v = _v;
                std::rotate(v.begin(), v.begin() + this->MathMod(n, v.size()), v.end());
                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Shift(const Vector1N &_v, int n)
        {
                if(_v.size() < 1) return Vector1N({});

                Vector1N v = Rotate(_v, n);
                if(n > 0) std::fill(v.end() - n, v.end(), 0); 
                else if(n < 0) std::fill(v.begin(), v.begin() - n, 0); 

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Shift(const Vector1C &_v, int n)
        {
                if(_v.size() < 1) return Vector1C({});

                Vector1C v = Rotate(_v, n);
                if(n > 0) std::fill(v.end() - n, v.end(), 0); 
                else if(n < 0) std::fill(v.begin(), v.begin() - n, 0); 

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Repeat(const Vector1N &v, double length)
        {
                Vector1N vN(v.size() * length);
                for (std::size_t i = 0; i < (std::size_t) length; ++i)
                        std::copy(v.begin(), v.end(), vN.begin() + i * v.size());

                if(!ROOT::IOPlus::Helpers::IsInteger(length)) {

                        int rest = v.size() + (length - (int) length);
                        for(int i = 0; i < rest; i++)
                                vN[v.size() * ((int) length) + i] = v[i];
                }

                return vN;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Repeat(const Vector1C &v, double length)
        {
                Vector1C vN(v.size() * length);
                for (std::size_t i = 0; i < (std::size_t) length; ++i)
                        std::copy(v.begin(), v.end(), vN.begin() + i * v.size());

                if(!ROOT::IOPlus::Helpers::IsInteger(length)) {

                        int rest = v.size() + (length - (int) length);
                        for(int i = 0; i < rest; i++)
                                vN[v.size() * ((int) length) + i] = v[i];
                }
                
                return vN;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Mirroring(const Vector1C &input, unsigned int offset, unsigned int foldingOffset, bool bConjugate)
        {
                int N = 2*(input.size()-offset-foldingOffset)+foldingOffset;
                Vector1C output = input;
                
                if(offset > input.size()) return output;
                output.resize(offset+N, 0);

                for(unsigned int i = offset; i < input.size()-foldingOffset; i++) {
                        output[offset+output.size()-i-1] = ComplexT(input[i].real(), (bConjugate ? -1 : 1) * input[i].imag());
                }

                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::MirroringDFT(const Vector1C &input, bool bConjugate) 
        { 
                unsigned int inputSize = input.size();
                Vector1C output = this->Mirroring(input, 1, this->MathMod(inputSize-1, 2) ? 0 : 1, bConjugate);
                return output;
        }
        
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::MirroringPSD(const Vector1N &input)  
        {
                Vector1N output = this->MirroringDFT(input);
                         output = this->Multiply(1/2., output);

                if(output.size()) output[0] = 2*output[0];
                if(output.size() > 1) output[output.size()/2+1] = 2 * output[output.size()/2+1];
                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::GetVectorAxis(TH1* h, int iAxis)
        {
                Vector1N v;
                if(h == NULL) return v;

                TAxis *axis = NULL;
                if( iAxis == 0) axis = h->GetXaxis();
                else if( iAxis == 1) axis = h->GetYaxis();
                else if( iAxis == 2) axis = h->GetZaxis();
                else {

                        TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                        return v;
                }
                
                for(int i = 1, N = axis->GetNbins(); i <= N; i++)
                        v.push_back(axis->GetBinLowEdge(i));

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::GetVectorAxis(TH1* h, Option_t *axis) 
        {
                TString opt = axis;
                        opt.ToLower();

                int iAxis;
                if(opt == "x") iAxis = 0;
                else if(opt == "y") iAxis = 1;
                else if(opt == "z") iAxis = 2;
                else iAxis = -1; // invalid value on purpose

                return GetVectorAxis(h, axis);
        }

        template <typename T>
        template <typename Tx>
        std::vector<Tx> TKFR<T>::Impl::GetVectorContent(TH1* h)
        {
                std::vector<Tx> v;

                if(h == NULL) return v;
                for(int i = 0, N = h->GetNcells(); i < N; i++) {

                        if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                        v.push_back(h->GetBinContent(i));
                }

                return v;
        }
        
        template <typename T>
        std::vector<double> TKFR<T>::Impl::GetVectorContent(TH1D* h) { return GetVectorContent<double>((TH1*) h); }
        template <typename T>
        std::vector<float> TKFR<T>::Impl::GetVectorContent(TH1F* h) { return GetVectorContent<float>((TH1*) h); }

        template <typename T>
        template <typename Tx>
        std::vector<Tx> TKFR<T>::Impl::GetVectorError(TH1* h)
        {
                std::vector<Tx> v;

                if(h == NULL) return v;
                for(int i = 0, N = h->GetNcells(); i < N; i++) {

                        if(h->IsBinUnderflow(i) || h->IsBinOverflow(i)) continue;
                        v.push_back(h->GetBinError(i));
                }

                return v;
        }

        template <typename T>
        std::vector<double> TKFR<T>::Impl::GetVectorError(TH1D* h) { return GetVectorError<double>((TH1*) h); }
        template <typename T>
        std::vector<float> TKFR<T>::Impl::GetVectorError(TH1F* h) { return GetVectorError<float>((TH1*) h); }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::StripNaN(const NumericT &a, T replacement) { return this->StripNaN(a, replacement); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::StripNaN(const Vector1N &v, T replacement) {  return this->StripNaN(v, replacement); }
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::StripNaN(const Vector1C &z, T replacement)
        {
                Vector1C result;
                for(int i = 0, N = z.size(); i < N; i++)
                        result.push_back(ComplexT((ROOT::IOPlus::Helpers::IsNaN(z[i].real()) || ROOT::IOPlus::Helpers::IsNaN(z[i].imag())) ? replacement : z[i]));

                return result;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::StripInf(const NumericT &a, T replacement) { return this->StripInf(a, replacement); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::StripInf(const Vector1N &v, T replacement) { return this->StripInf(v, replacement); }
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::StripInf(const Vector1C &z, T replacement)
        {
                Vector1C result;
                for(int i = 0, N = z.size(); i < N; i++)
                        result.push_back(ComplexT((ROOT::IOPlus::Helpers::IsInf(z[i].real()) || ROOT::IOPlus::Helpers::IsInf(z[i].imag())) ? replacement : z[i]));

                return result;
        }

        template <typename T>
        bool TKFR<T>::Impl::Contains(const NumericT &a, const Vector1N &b )
        {
                if(b.size() == 0) return false;
                return b[0] <= a && a <= b[b.size() - 1];
        }
        
        template <typename T>
        ROOT::Math::Interpolator TKFR<T>::Impl::Interpolator(Vector1N x, Vector1N y, ROOT::Math::Interpolation::Type type) {

                return ROOT::Math::Interpolator(
                        std::vector<double>(x.begin(), x.end()), 
                        std::vector<double>(y.begin(), y.end()),
                        type
                );
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::MathMod(NumericT a, NumericT b) {
                return a - b * TMath::Floor(a / b);
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Flatten(Vector2N const &vec)
        {
                Vector1N flattened;
                for (auto const &v: vec)
                        flattened.insert(flattened.end(), v.begin(), v.end());

                return flattened;
        }


        template <typename T>
        int TKFR<T>::Impl::Closest(Vector1N v, T value)
        {
                for (int i = 0, N = v.size(); i < N; i++)
                        v[i] = std::abs(v[i] - value);

                return min_element(v.begin(), v.end()) - v.begin();
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Remove(Vector1N v, const NumericT &element)
        {
                v.erase(std::remove_if(v.begin(), v.end(), [element](T value) { return element == value; }), v.end());
                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::RemoveLeading(Vector1N v, const NumericT &element)
        {
                for (auto it = v.begin(); it != v.end();) {

                        if (!ROOT::IOPlus::Helpers::EpsilonEqualTo(*it, element)) break;
                        it = v.erase(it);
                }

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::RemoveLeadingZeros(Vector1N v) { return RemoveLeading(v, 0.0); }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::RemoveTrailing(Vector1N v, const NumericT &element)
        {
                while (!v.empty()) {

                        if (!ROOT::IOPlus::Helpers::EpsilonEqualTo(v.back(), element)) break;
                        v.pop_back();
                }

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::RemoveTrailingZeros(Vector1N v) { return RemoveTrailing(v, 0.0); }

        template <typename T>
        typename TKFR<T>::Vector2N TKFR<T>::Impl::Inflatten(Vector1N const &vec, int length)
        {
                int N = vec.size();
                if (N % length != 0)
                        throw std::invalid_argument("Length must be a multiple of the total length of the flatten vector");

                Vector2N inflattened;
                for (int i = 0, I = N/length; i < I; i++)
                        inflattened.push_back(Vector1N(vec.begin()+i*length, vec.begin()+(i+1)*length));

                return inflattened;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Q1( const Vector1N &_v) 
        {
                Vector1N v = _v;
                std::sort(v.begin(), v.end()); 
                int i = 1/4. * (v.size() + 1); 
                return v[i];
        }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Q2( const Vector1N &_v) 
        { 
                Vector1N v = _v;
                std::sort(v.begin(), v.end()); 
                int i = 2/4. * (v.size() + 1); 
                return v[i];
        }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Q3( const Vector1N &_v) 
        {
                Vector1N v = _v;
                std::sort(v.begin(), v.end()); 
                int i = 3/4. * (v.size() + 1);
                return v[i]; 
        }
        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::IQR( const Vector1N &_v) 
        {
                Vector1N v = _v;
                std::sort(v.begin(), v.end()); 

                int i1 = 1/4. * (v.size() + 1); 
                T Q1 = v[i1];

                int i3 = 3/4. * (v.size() + 1); 
                T Q3 = v[i3];

                return Q3 - Q1;
        }

        template <typename T>
        typename TKFR<T>::Vector2C TKFR<T>::Impl::Transpose(const Vector2C &v)
        {
                if (v.size() == 0)
                        return {};

                Vector2C vT(v[0].size(), Vector1C());

                for (int i = 0; i < v.size(); i++)
                        for (int j = 0; j < v[i].size(); j++)
                                vT[j].push_back(v[i][j]);

                return vT;
        }
        template <typename T>
        typename TKFR<T>::Vector2N TKFR<T>::Impl::Transpose(const Vector2N &v)
        {
                if (v.size() == 0)
                        return {};

                Vector2N vT(v[0].size(), Vector1N());

                for (int i = 0; i < v.size(); i++)
                        for (int j = 0; j < v[i].size(); j++)
                                vT[j].push_back(v[i][j]);

                return vT;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::EigenValues(const TMatrixT<NumericT> &matrix)
        {
                if(!matrix.GetNrows()) return {};
                if(!matrix.GetNcols()) return {};

                TMatrixDEigen eigenValues(matrix);
                return this->Complex(stdvector(eigenValues.GetEigenValuesRe()), stdvector(eigenValues.GetEigenValuesIm()));
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Slice(const Vector1N &u, int start, int length)
        {
                if(length < 0) length = u.size()-start;

                Vector1N v(u.begin() + start, u.begin() + start + TMath::Min(length, (int) u.size()-start));
                                v.resize(length);

                return v;
        }
        
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Slice(const Vector1C &u, int start, int length)
        {
                if(length < 0) length = u.size()-start;

                Vector1C v(u.begin() + start, u.begin() + start + TMath::Min(length, (int) u.size()-start));
                                                v.resize(length);

                return v;
        }

        template <typename T> // Y = X[a*k+b], b = [0..a-1]
        typename TKFR<T>::Vector1C TKFR<T>::Impl::SliceXn(const Vector1C &Xn, int a, int b)
        {
                b = this->MathMod(b, a);

                Vector1C Y(Xn.size() / a);
                for(int k = 0, N = Y.size(); k < N; k++)
                        Y[k] = Xn[a*k+b];

                return Y;
        }

        template <typename T> // Y = X[a*k+b], b = [0..a-1]
        typename TKFR<T>::Vector1N TKFR<T>::Impl::SliceXn(const Vector1N &Xn, int a, int b)
        {
                b = this->MathMod(b, a);

                Vector1N Yn(Xn.size()/a);
                for(int k = 0, N = Yn.size(); k < N; k++)
                        Yn[k] = Xn[a*k+b];
                        
                return Yn;
        }

        template <typename T> // Y[a*k+b] = X[k], b = [0..a-1]
        typename TKFR<T>::Vector1C TKFR<T>::Impl::InsetXn(const Vector1C &Xn, int a, int b, const ComplexT& v)
        {
                b = this->MathMod(b, a);

                Vector1C Y(a*Xn.size(), v);
                for(int k = 0, N = Xn.size(); k < N; k++)
                        Y[a*k+b] = Xn[k];

                return Y;
        }

        template <typename T> // Y[a*k+b] = X[k], b = [0..a-1]
        typename TKFR<T>::Vector1N TKFR<T>::Impl::InsetXn(const Vector1N &Xn, int a, int b, const NumericT &v)
        {
                b = this->MathMod(b, a);

                Vector1N Y(a*Xn.size(), v);
                for(int k = 0, N = Xn.size(); k < N; k++)
                        Y[a*k+b] = Xn[k];
                        
                return Y;
        }


        template <typename T>
        int TKFR<T>::Impl::Index(int index, const Vector1N &input) {
                return this->MathMod(index, input.size());
        }

        template <typename T>
        int TKFR<T>::Impl::Index(int index, const Vector1C &input) {
                return this->MathMod(index, input.size());
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Resize(const Vector1N &input, int N, int offset)
        {
                if(input.size() == 0) return {};
                
                Vector1N output = input;
                output.resize(this->MathMod(offset, output.size()+1) + N, 0);
                return output;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Resize(const Vector1C &input, unsigned int N, int offset)
        {
                if(input.size() == 0) return {};

                Vector1C output = input;
                                output.resize(this->MathMod(offset, output.size()+1) + N, 0);
                
                return output;
        }

        template <typename T>
        inline std::vector<std::complex<T>> TKFR<T>::Impl::ResizeDFT(const std::vector<std::complex<T>> &inputDFT, SpectrumProperty spectrum, unsigned int outputSize)
        {
                if(inputDFT.size() == 0) return {};

                unsigned int inputSize = (spectrum == SpectrumProperty::OneSided) ? 2*(inputDFT.size()-1) : inputDFT.size();        
                outputSize = TMath::Max((int) (spectrum == SpectrumProperty::OneSided ? outputSize/2+1 : outputSize) - 1, (int) 0);
                if(inputSize == outputSize) return inputDFT;
                                        
                // Create minimal vector and handle positive frequency
                unsigned int nFFT = TMath::Min(inputSize, outputSize);
                int iN = nFFT/2+1; // Nyquist frequency candidate
                std::vector<std::complex<T>> outputDFT(inputDFT.begin(), inputDFT.begin() + TMath::Min(iN, (int) inputDFT.size()));
                outputDFT.resize(outputSize+1, 0);
        
                // Handle negative frequencies
                for(int i = 1; nFFT > 2 && spectrum != SpectrumProperty::OneSided && i < iN; i++) {
                        outputDFT[this->Index(outputDFT.size()-i, outputDFT)] = inputDFT[this->Index(inputDFT.size()-i, inputDFT)];
                }

                // Adjust nyquist frequency, if found
                if( ROOT::IOPlus::Helpers::MathMod(nFFT, 2) == 0 && inputSize > 0)
                {
                        if(inputSize < outputSize) outputDFT[iN-1] = Complex(0.5,0)*outputDFT[iN-1]; // Upsample
                        else { // Downsample
                        
                                if(spectrum == SpectrumProperty::OneSided) outputDFT[iN-1] = Complex(2.0,0) * outputDFT[iN-1];
                                else {
                                        if(iN == 2) outputDFT[iN-1] = 0; // Special termination case 
                                        else outputDFT[iN-1] = this->Conj(outputDFT[iN-1]);

                                        outputDFT[iN-1] = outputDFT[iN-1] + inputDFT[this->Index(inputDFT.size()-iN+1, inputDFT)];
                                }
                        }

                        if(spectrum != SpectrumProperty::OneSided) outputDFT[this->Index(outputDFT.size()-iN+1, outputDFT)] = outputDFT[iN-1]; // Unfold nyquist frequency onto negative freq
                }

                return this->Multiply(outputDFT, ((double) outputSize) / inputSize);
        }


        template <typename T>
        double TKFR<T>::Impl::Length(const Vector1N &v) { return v[v.size()-1] - v[0]; }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::TimeShift(const Vector1C &Xn, T fs, T time) 
        {
                Vector1C Yn(Xn.size());
                for(int f = 0, N = Xn.size(); f < N; f++) {
                        T x = 2*TMath::Pi() * (f/fs) * time;
                        Yn[f] = Xn[f] * ComplexT(TMath::Cos(x), TMath::Sin(x));
                }

                return Yn;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::PhaseShift(const Vector1C &Xn, T phase) 
        { 
                Vector1C Yn(Xn.size());
                for(int f = 0, N = Xn.size(); f < N; f++) {
                        T x = phase;
                        Yn[f] = Xn[f] * ComplexT(TMath::Cos(x), TMath::Sin(x));
                }

                return Yn;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Roll(const Vector1N &Xn, int nbins) 
        {
                Vector1N Yn(Xn.size());
                for(int i = 0, N = Xn.size(); i < N; i++)
                        Yn[i] = Xn[this->MathMod(i+nbins, Xn.size())];

                
                return Yn;
        }
        
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Roll(const Vector1C &Xn, int nbins) 
        { 
                Vector1C Yn(Xn.size());
                for(int i = 0, N = Xn.size(); i < N; i++)
                        Yn[i] = Xn[this->MathMod(i+nbins, Xn.size())];

                return Yn;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::FrequencyShift(const Vector1N &x, T fs, T fc) 
        {
                Vector1C y(x.size());
                for(int k = 0, N = x.size(); k < N; k++) {
                        T z = 2*TMath::Pi() * (fc/fs) * k;
                        y[k] = x[k] * ComplexT(TMath::Cos(z), TMath::Sin(z));
                }

                return y;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::FrequencyShift(const Vector1C &x, T fs, T fc) 
        {
                Vector1C y(x.size());
                for(int k = 0, N = x.size(); k < N; k++) {
                        T z  = 2*TMath::Pi() * (fc/fs) * k;
                        y[k] = x[k] * ComplexT(TMath::Cos(z), TMath::Sin(z));
                }

                return y;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Flip(Vector1N v)
        {
                std::reverse(v.begin(), v.end());
                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Flip(Vector1C v)
        {
                std::reverse(v.begin(), v.end());
                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Filter(const Vector1N &_v, std::vector<int> indexes)
        {
                Vector1N v;
                for(int i = 0, N = indexes.size(); i < N; i++)
                        v.push_back(_v[indexes[i]]);

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Filter(const Vector1C &_v, std::vector<int> indexes)
        {
                Vector1C v;
                for(unsigned int i = 0, N = indexes.size(); i < N; i++)
                        v.push_back(_v[indexes[i]]);

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Range(unsigned int N, const NumericT &dx)
        {
                Vector1N v;
                for(unsigned int i = 0; i < N; i++)
                        v.push_back(i*dx);

                return v;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Range(unsigned int N, const NumericT &min, const NumericT &max)
        {
                if(N == 0) return {};
                if(N == 1) return {min, max};

                Vector1N v;
                T dx = (max-min)/(N-1);
                for(unsigned int i = 0; i < N; i++)
                        v.push_back(min + i*dx);

                return v;
        }
                
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Convolve(const Vector1N &x, const Vector1N &y)
        {
                int N = x.size();
                int M = y.size();

                Vector1N z(N + M - 1, 0);
                for (int i = 0; i < N; i++) {
                        for (int j = 0; j < M; j++) {
                                z[i+j] += x[i] * y[j];
                        }
                }

                return z;
        }
        
        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Convolve(const Vector1C &x, const Vector1C &y)
        {
                int N = x.size();
                int M = y.size();

                Vector1C z(N + M - 1, 0);
                for (int i = 0; i < N; i++) {
                        for (int j = 0; j < M; j++) {
                                z[i+j] = z[i+j] + x[i] * y[j];
                        }
                }

                return z;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Correlate(Vector1N x, const Vector1N &y)
        {
                std::reverse(x.begin(), x.end());
                return Convolve(x,y);
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Correlate(Vector1C x, const Vector1C &y)
        {
                std::reverse(x.begin(), x.end());
                return Convolve(x,y);
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Autocorrelate(const Vector1N & x) { return Correlate(x,x); }
        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Autocorrelate(const Vector1C & x) { return Real(Correlate(x,x)); }

        template<typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Distance(const Vector1N &a, const Vector1N &b) 
        {
                if(a.size() != b.size())
                        throw std::invalid_argument("Vectors are not same size: a("+TString::Itoa(a.size(),10)+") != b("+TString::Itoa(b.size(),10)+")");

                NumericT distance = 0;
                for(int i = 0, N = a.size(); i < N; i++)
                        distance += std::pow(b[i] - a[i], 2);

                return TMath::Sqrt(distance); 
        }

        template<typename T>
        std::pair<typename TKFR<T>::NumericT,typename TKFR<T>::NumericT> /* a,b */ TKFR<T>::Impl::Regression(const Vector1N &x, const Vector1N &y)
        {
                assert(x.size() == y.size());

                NumericT N     = x.size();
                NumericT sumX  = this->Sum(x);
                NumericT sumY  = this->Sum(y); 
                NumericT sumXY = this->Sum(this->Multiply(x, y)); 
                NumericT sumX2 = this->Sum(this->Multiply(x, x));

                NumericT a = (   N*sumXY - sumX*sumY) / (N*sumX2 - sumX*sumX);
                NumericT b = (sumY*sumX2 - sumX*sumXY) / (N*sumX2 - sumX*sumX);

                return std::make_pair(ROOT::IOPlus::Helpers::IsNaN(a) ? 0 : a, ROOT::IOPlus::Helpers::IsNaN(b) ? 0 : b);
        }

        template<typename T>
        std::pair<typename TKFR<T>::NumericT, typename TKFR<T>::NumericT> /* a,b */ TKFR<T>::Impl::Regression(const Vector1N &x)
        {
                Vector1N y = this->Range(x.size(), 1.0);
                return Regression(x,y);
        }

        template <typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const kfr::zpk<T> &zpk) { return stdzpk(zpk); }
        template <typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const kfr::zpk<T> &zpk) { return SecondOrderSections(stdzpk(zpk)); }
        template <typename T>
        typename TKFR<T>::SS  TKFR<T>::Impl::StateSpace(const kfr::zpk<T> &zpk) { return StateSpace(stdzpk(zpk)); }
        template <typename T>
        typename TKFR<T>::TF  TKFR<T>::Impl::TransferFunction(const kfr::zpk<T> &zpk) { return TransferFunction(stdzpk(zpk)); }

        template <typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const kfr::biquad_params<T> &params) { return ZeroPoleGain(stdbiquad(params)); }
        template <typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const kfr::biquad_params<T> &params) { return SecondOrderSections(stdbiquad(params)); }
        template <typename T>
        typename TKFR<T>::SS  TKFR<T>::Impl::StateSpace(const kfr::biquad_params<T> &params) { return StateSpace(stdbiquad(params)); }
        template <typename T>
        typename TKFR<T>::TF  TKFR<T>::Impl::TransferFunction(const kfr::biquad_params<T> &params) { return TransferFunction(stdbiquad(params)); }

        template <typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const TBiquad &params) { return ZeroPoleGain(TransferFunction(params)); }
        template <typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const TBiquad &params) { return SecondOrderSections(TransferFunction(params)); }
        template <typename T>
        typename TKFR<T>::SS  TKFR<T>::Impl::StateSpace(const TBiquad &params) { return StateSpace(TransferFunction(params)); }
        template <typename T>
        typename TKFR<T>::TF  TKFR<T>::Impl::TransferFunction(const TBiquad &params)
        {
                TF tf;
                        tf.B  = { params.B.x2, params.B.x1, params.B.x0 };
                        tf.A = { params.A.x2, params.A.x1, params.A.x0 };

                return tf;
        }

        
        template <typename T>
        typename TKFR<T>::TF TKFR<T>::Impl::TransferFunction(const SOS &sos)
        {
                TF tf;

                int n_sections = sos.size();
                tf.B.resize(1, 1);
                tf.A.resize(1, 1);

                for(int i = 0; i < n_sections; i++) {

                        Vector1N b;
                                        b.push_back(sos[i].B.x2);
                                        b.push_back(sos[i].B.x1);
                                        b.push_back(sos[i].B.x0);
                
                        Vector1N a;
                                        a.push_back(sos[i].A.x2);
                                        a.push_back(sos[i].A.x1);
                                        a.push_back(sos[i].A.x0);

                        tf.B  = Multiply(tf.B, b);
                        tf.A = Multiply(tf.A, a);
                }

                return tf;
        }

        template <typename T>
        typename TKFR<T>::TF TKFR<T>::Impl::TransferFunction(const TF &_tf)
        {
                TF tf = _tf;
                if(!tf.A.size() || !tf.B.size()) return tf;
                tf.B  = this->RemoveTrailingZeros(tf.B);
                tf.A = this->RemoveTrailingZeros(tf.A);

                for(int i = 0, I = tf.B.size(); i < I; i++)
                        tf.B[i] /= tf.A[tf.A.size()-1];
                for(int i = 0, I = tf.A.size(); i < I; i++)
                        tf.A[i] /= tf.A[tf.A.size()-1];

                return tf;
        }

        template <typename T>
        typename TKFR<T>::TF TKFR<T>::Impl::TransferFunction(const ZPK &zpk)
        {
                Vector1C z = zpk.zeros;
                Vector1C p = zpk.poles;
                NumericT k = zpk.gain;

                Vector1C Bi = PolynomialCoefficients(z);
                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(this->Sum(this->Abs(this->Imag(Bi))), (T) 0.0)) {
                        throw std::runtime_error("Transfer function numerator must have real coefficients");
                }
                Vector1C Ai = PolynomialCoefficients(p);
                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(this->Sum(this->Abs(this->Imag(Ai))), (T) 0.0)) {
                        throw std::runtime_error("Transfer function denominator must have real coefficients");
                }

                TF tf;
                        tf.B  = Multiply(Real(PolynomialCoefficients(z)), k);
                        tf.A = Real(PolynomialCoefficients(p));              

                // ROOT::Polynomial definition
                std::reverse(tf.B.begin(), tf.B.end());
                std::reverse(tf.A.begin(), tf.A.end());

                return tf;
        }

        template <typename T>
        typename TKFR<T>::TF TKFR<T>::Impl::TransferFunction(const SS &_ss)
        {
                SS ss = _ss;
                TMatrixT<NumericT> &A = ss.A;
                TMatrixT<NumericT> &B = ss.B;
                TMatrixT<NumericT> &C = ss.C;
                TMatrixT<NumericT> &D = ss.D;

                TF tf;
                if(!A.GetNrows()) return tf;

                if(A.GetNcols() != A.GetNrows()) throw std::invalid_argument("Unexpected dimension provided for A matrix");
                if(A.GetNcols() != B.GetNrows()) throw std::invalid_argument("Unexpected dimension provided for B matrix");
                if(A.GetNcols() != C.GetNcols()) throw std::invalid_argument("Unexpected dimension provided for C matrix");
                if(B.GetNcols() != D.GetNcols()) throw std::invalid_argument("Unexpected col dimension provided for D matrix");
                if(C.GetNrows() != D.GetNrows()) throw std::invalid_argument("Unexpected row dimension provided for D matrix");

                int N = A.GetNrows();

                tf.A.resize(N+1,0);
                tf.A = this->Real(this->Solve(A));
                tf.A = this->RemoveLeadingZeros(tf.A);

                TVectorT<NumericT> V1 = rootvector(tf.A);
                TVectorT<NumericT> V2 = rootvector(this->Real(this->Solve(A - B * C)));
                TVectorT<NumericT> V3 = V2 + (D(0,0) - 1) * V1;

                tf.B.resize(N+1,0);
                tf.B = stdvector(V2 + (D(0,0) - 1) * V1);
                tf.B  = this->RemoveLeadingZeros(tf.B);

                std::reverse(tf.B.begin(), tf.B.end());
                std::reverse(tf.A.begin(), tf.A.end());
                
                return tf;
        }

        template<typename T>
        std::vector<typename TKFR<T>::TF> TKFR<T>::Impl::TransferFunction(const std::vector<ZPK> &zpk) { return Transform(zpk, [this](const ZPK &x) { return TransferFunction(x); }); }
        template<typename T>
        std::vector<typename TKFR<T>::TF> TKFR<T>::Impl::TransferFunction(const std::vector<SOS> &sos) { return Transform(sos, [this](const SOS &x) { return TransferFunction(x); }); }
        template<typename T>
        std::vector<typename TKFR<T>::TF> TKFR<T>::Impl::TransferFunction(const std::vector<SS>  &ss ) { return Transform(ss,  [this](const SS  &x) { return TransferFunction(x); }); }
        template<typename T>
        std::vector<typename TKFR<T>::TF> TKFR<T>::Impl::TransferFunction(const std::vector<TF>  &tf ) { return Transform(tf,  [this](const TF  &x) { return TransferFunction(x); }); }

        template<typename T>
        typename TKFR<T>::SS TKFR<T>::Impl::StateSpace(const TF &tf) 
        {
                SS ss;
                TMatrixT<NumericT> &A = ss.A;
                TMatrixT<NumericT> &B = ss.B;
                TMatrixT<NumericT> &C = ss.C;
                TMatrixT<NumericT> &D = ss.D;

                int N = tf.B.size() - 1;
                int M = tf.A.size() - 1;
                if(N < 0 || M < 0 || M < N)
                        return ss;

                A.ResizeTo(M, M);
                A = this->Eye(M, -1);
                for (int i = 0; i < M; i++) {
                        A(0, i) = -tf.A[M - (i + 1)] / tf.A[M];
                }

                B.ResizeTo(M, 1);
                B = this->Identity(M, 1);

                C.ResizeTo(1, M);
                for (int i = 0; i < M; i++) {

                        T K = (M != N) ? 0 : tf.B[tf.B.size()-1];
                        C(0, M-1-i) = (i <= N ? tf.B[i] - K*tf.A[i] : 0);
                }
                
                D.ResizeTo(1,1);
                D(0, 0) = N < M ? 0 : tf.B[tf.B.size()-1];
                return ss; 
        }

        template<typename T>
        typename TKFR<T>::SS TKFR<T>::Impl::StateSpace(const ZPK &zpk) { return StateSpace(TransferFunction(zpk)); }
        template<typename T>
        typename TKFR<T>::SS TKFR<T>::Impl::StateSpace(const SOS &sos) { return StateSpace(TransferFunction(sos)); }
        template<typename T>
        typename TKFR<T>::SS TKFR<T>::Impl::StateSpace(const SS  &ss) { return ss; }
        
        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const TF &tf)
        {
                Vector1N B = tf.B;
                Vector1N A = tf.A;

                ZPK zpk;
                if(!A.size() || !B.size()) return zpk;
                
                zpk.gain = B[0];
                for(int i = 0, N = B.size(); i < N; i++)
                        B[i] /= B[B.size()-1];

                ROOT::Math::Polynomial zeros = ROOT::Math::Polynomial(B.size()-1);
                                        zeros.SetParameters(&std::vector<double>(B.begin(), B.end())[0]);
                ROOT::Math::Polynomial poles = ROOT::Math::Polynomial(A.size()-1);
                                        poles.SetParameters(&std::vector<double>(A.begin(), A.end())[0]);

                auto zeroRoots = zeros.FindRoots();
                auto poleRoots = poles.FindRoots();

                zpk.zeros = std::vector<ComplexT>(zeroRoots.begin(), zeroRoots.end());
                zpk.poles = std::vector<ComplexT>(poleRoots.begin(), poleRoots.end());

                return zpk;
        }

        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const SOS &sos)
        {
                int n_sections = sos.size();

                ZPK zpk;
                        zpk.zeros.resize(2*n_sections);
                        zpk.poles.resize(2*n_sections);
                        zpk.gain = 1;

                for(int i = 0; i < n_sections; i++) {

                        ZPK _zpk = this->ZeroPoleGain(this->TransferFunction(sos[i]));
                        for(int j = 0, J = _zpk.zeros.size(); j < J; j++)
                                zpk.zeros[j+i*n_sections] = _zpk.zeros[j];
                        for(int j = 0, J = _zpk.poles.size(); j < J; j++)
                                zpk.poles[j+i*n_sections] = _zpk.poles[j];

                        zpk.gain *= _zpk.gain;
                }

                return zpk;
        }

        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const SS &ss) { return ZeroPoleGain(TransferFunction(ss)); }
        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const ZPK &zpk) { return zpk; }

        template<typename T>
        std::vector<typename TKFR<T>::ZPK> TKFR<T>::Impl::ZeroPoleGain(const std::vector<TF>   &tf) { return Transform(tf,  [this](const TF  &x) { return ZeroPoleGain(x); }); }
        template<typename T>
        std::vector<typename TKFR<T>::ZPK> TKFR<T>::Impl::ZeroPoleGain(const std::vector<SOS> &sos) { return Transform(sos, [this](const SOS &x) { return ZeroPoleGain(x); }); }
        template<typename T>
        std::vector<typename TKFR<T>::ZPK> TKFR<T>::Impl::ZeroPoleGain(const std::vector<SS>   &ss) { return Transform(ss,  [this](const SS  &x) { return ZeroPoleGain(x); }); }
        template<typename T>
        std::vector<typename TKFR<T>::ZPK> TKFR<T>::Impl::ZeroPoleGain(const std::vector<ZPK> &zpk) { return zpk; }

        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const ZPK &zpk) { return stdsos(kfr::to_sos(kfrzpk(zpk)));     }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const TF  &tf)  { return SecondOrderSections(ZeroPoleGain(tf)); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const SS  &ss)  { return SecondOrderSections(ZeroPoleGain(ss)); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const SOS &sos) { return sos; }

        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const std::vector<SS>   &ss) { return SecondOrderSections(Transform(ss,  [this](const SS  &x) { return SecondOrderSections(x); })); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const std::vector<ZPK> &zpk) { return SecondOrderSections(Transform(zpk, [this](const ZPK &x) { return SecondOrderSections(x); })); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const std::vector<TF>   &tf) { return SecondOrderSections(Transform(tf,  [this](const TF  &x) { return SecondOrderSections(x); })); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const std::vector<SOS> &sos) 
        {
                TKFR<T>::SOS _sos;
                                _sos.reserve(std::accumulate(
                                sos.begin(), sos.end(), 0, 
                                [](size_t acc, const SOS& x) { return acc + x.size(); })
                                );

                for (const auto& x : sos) {
                        _sos.insert(_sos.end(), x.sections.begin(), x.sections.end());
                }
        
                return _sos;
        }

        template<typename T>
        typename TKFR<T>::TF  TKFR<T>::Impl::TransferFunction (const Vector1N &B, const Vector1N &A) { return TF(B,A);  }
        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const Vector1N &B, const Vector1N &A) { return ZeroPoleGain(TF(B,A)); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const Vector1N &B, const Vector1N &A) { return SecondOrderSections(TF(B,A)); }
        template<typename T>
        typename TKFR<T>::SS  TKFR<T>::Impl::StateSpace (const Vector1N &B, const Vector1N &A) { return StateSpace (TF(B,A)); }

        template<typename T>
        typename TKFR<T>::TF  TKFR<T>::Impl::TransferFunction (const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return TransferFunction (SS(A,B,C,D));  }
        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return ZeroPoleGain(SS(A,B,C,D)); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return SecondOrderSections(SS(A,B,C,D)); }
        template<typename T>
        typename TKFR<T>::SS  TKFR<T>::Impl::StateSpace (const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return SS(A,B,C,D); }

        template<typename T>
        typename TKFR<T>::TF  TKFR<T>::Impl::TransferFunction (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return TransferFunction (Vector1N(B.Parameters(), B.Parameters()+B.NPar()), Vector1N(A.Parameters(), A.Parameters()+A.NPar())); }       
        template<typename T>
        typename TKFR<T>::ZPK TKFR<T>::Impl::ZeroPoleGain(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return ZeroPoleGain(Vector1N(B.Parameters(), B.Parameters()+B.NPar()), Vector1N(A.Parameters(), A.Parameters()+A.NPar())); }
        template<typename T>
        typename TKFR<T>::SOS TKFR<T>::Impl::SecondOrderSections(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return SecondOrderSections(Vector1N(B.Parameters(), B.Parameters()+B.NPar()), Vector1N(A.Parameters(), A.Parameters()+A.NPar())); }
        template<typename T>
        typename TKFR<T>::SS  TKFR<T>::Impl::StateSpace (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return StateSpace (Vector1N(B.Parameters(), B.Parameters()+B.NPar()), Vector1N(A.Parameters(), A.Parameters()+A.NPar())); }

        template<typename T>
        ROOT::Math::Polynomial TKFR<T>::Impl::Polynomial(const Vector1N &ai)
        {
                ROOT::Math::Polynomial P = ROOT::Math::Polynomial(ai.size()-1);
                                        P.SetParameters(&std::vector<double>(ai.begin(), ai.end())[0]);

                return P;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::Multiply(ROOT::Math::Polynomial &A, ROOT::Math::Polynomial &B)
        {
                Vector1N _A = PolynomialCoefficients(A);
                Vector1N _B = PolynomialCoefficients(B);
                Vector1N _C(_A.size()+_B.size()-1, 0); 
        
                for (int i = 0, I = _A.size(); i < I; i++) {

                        for (int j = 0, J = _B.size(); j < J; j++) {

                                _C[i+j] += _A[i]*_B[j];
                        }
                }
                
                return _C;
        }

        template <typename T>
        typename TKFR<T>::Vector1N TKFR<T>::Impl::PolynomialCoefficients(ROOT::Math::Polynomial &P)
        {
                std::vector<std::complex<double>> findRoots = P.FindRoots(); // ROOT::Math::Polynomial only handles real coefficients.
                Vector1C findRootsC(findRoots.begin(), findRoots.end());

                return Real(PolynomialCoefficients(findRootsC));
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::PolynomialCoefficients(TKFR<T>::Vector1C &roots)
        {
                int degree = roots.size();
                Vector1C ai(degree + 1, 0.0);
                ai[0] = 1.0; // Initialize the coefficient of x^0 (constant term)

                for (int i = 0; i < degree; i++) {
                        for (int j = degree; j >= 1; j--) {
                                ai[j] = ai[j] - ai[j - 1] * roots[i];
                        }
                }

                return ai;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Solve(ROOT::Math::Polynomial &P) { 
                
                std::vector<std::complex<double>> vroots = P.FindRoots();
                if constexpr (std::is_same<T, double>::value) return vroots;

                Vector1C roots(vroots.size());
                for (size_t i = 0; i < vroots.size(); ++i) {

                        roots[i] = std::complex<NumericT>(static_cast<NumericT>(vroots[i].real()),
                                                                static_cast<NumericT>(vroots[i].imag()));
                }

                return roots;
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Solve(const Vector1N &ai) 
        { 
                ROOT::Math::Polynomial P = this->Polynomial(ai);
                return this->Solve(P);
        }

        template <typename T>
        typename TKFR<T>::Vector1C TKFR<T>::Impl::Solve(const TMatrixT<NumericT> &M) 
        {
                Vector1C lambda = this->EigenValues(M);

                Vector1C v = {1};
                for(int i = 0, N = M.GetNrows(); i < N; i++) {

                        Vector1C w = {1, -lambda[i]};
                        v = this->Convolve(v, w);
                }

                return v;
        }

        template <typename T>
        typename TKFR<T>::ComplexT TKFR<T>::Impl::Eval(const ComplexT &z, const Vector1N &par)
        {
                ComplexT polz = 0;
                for(int i = 0, i0 = 1, N = par[0]; i < N; i++) {
                        polz += par[i+i0] * std::pow(z, ComplexT(i,0));
                }

                return polz;
        }

        template <typename T>
        typename TKFR<T>::NumericT TKFR<T>::Impl::Eval(const NumericT &x, const Vector1N &par)
        {
                NumericT polz = 0;
                for(int i = 0, i0 = 1, N = par[0]; i < N; i++) {
                        polz += par[i+i0] * std::pow(x, i);
                }

                return polz;
        }
} }

template class ROOT::Signal::TKFR<float>::Impl;
template class ROOT::Signal::TKFR<double>::Impl;
