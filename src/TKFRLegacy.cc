#include "ROOT/Signal/Impl/TKFRLegacyImpl.h"

TKFRLegacy *gKFRLegacy = new TKFRLegacy();

ClassImp(ROOT::Signal::TKFRLegacy<double>)
ClassImp(ROOT::Signal::TKFRLegacy<float>)
    
#if KFR_VERSION_MAJOR < 5
    #define KFR__EXPRESSION_HANDLE kfr::expression_pointer
    #define KFR__TO_HANDLE kfr::to_pointer
#else
    #define KFR__EXPRESSION_HANDLE kfr::expression_handle
    #define KFR__TO_HANDLE kfr::to_handle
#endif

template<typename T>
bool ROOT::Signal::TKFRLegacy<T>::IsAlreadyRunningMT = false;
template<typename T>
std::atomic<int> ROOT::Signal::TKFRLegacy<T>::finishedWorkers = 0;
template<typename T>
const std::complex<T> ROOT::Signal::TKFRLegacy<T>::i = std::complex<T>(0.0,1.0);

namespace ROOT {
        namespace Signal {

                template <typename T>
                TKFRLegacy<T>::TKFRLegacy() {

                        this->pImpl = new TKFRLegacy<T>::Impl();
                        if constexpr (std::is_same<T, double>::value) {
                                if(gKFRLegacy == nullptr) gKFRLegacy = this;
                        }
                }

                template <typename T>
                TKFRLegacy<T>::~TKFRLegacy() {

                        if(this->pImpl) {
                        
                                delete this->pImpl;
                                this->pImpl = NULL;
                        }

                        if constexpr (std::is_same<T, double>::value) {
                                if(gKFRLegacy == this) gKFRLegacy = nullptr;
                        }
                }

                template <typename T>
                const char *TKFRLegacy<T>::Version()
                {
                        return this->pImpl->Version();
                }

                template <typename T> 
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Get(ComplexPart part, const ComplexT &z)
                {
                        switch(part) {

                                case ComplexPart::Real     : return this->pImpl->Real(z);
                                case ComplexPart::Imag     : return this->pImpl->Imag(z);
                                case ComplexPart::Magnitude: return this->pImpl->Mag(z);
                                case ComplexPart::Decibel  : return this->pImpl->dB(z);
                                case ComplexPart::Phase    : return this->pImpl->Phase(z);
                                case ComplexPart::Argument : return this->pImpl->Argument(z);
                                default:
                                        throw std::invalid_argument("Unexpected complex prop requested..");
                        }
                }

                template <typename T> 
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Get(ComplexPart part, const Vector1C &z)
                {
                        switch(part) {

                                case ComplexPart::Real     : return this->pImpl->Real(z);
                                case ComplexPart::Imag     : return this->pImpl->Imag(z);
                                case ComplexPart::Magnitude: return this->pImpl->Mag(z);
                                case ComplexPart::Decibel  : return this->pImpl->dB(z);
                                case ComplexPart::Phase    : return this->pImpl->Phase(z);
                                case ComplexPart::Argument : return this->pImpl->Argument(z);
                                default:
                                        throw std::invalid_argument("Unexpected complex part requested..");
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Eval(const Vector1N &x, const TF &H) { return this->pImpl->Transform(x, [&H, this](const NumericT &xi) { return this->Eval(xi, H); }); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Eval(const Vector1C &x, const TF &H) { return this->pImpl->Transform(x, [&H, this](const ComplexT &xi) { return this->Eval(xi, H); }); }
              
                template <typename T> 
                typename TKFRLegacy<T>::Vector2N TKFRLegacy<T>::Get(ComplexPart part, const Vector2C &x)
                {
                        return this->pImpl->Transform(x, [&part, this](const Vector1C &xi) { return this->Get(part, xi); });
                }

                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::Complex(const NumericT &a, const NumericT &b) { return this->pImpl->Complex(a,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Complex(const Vector1N &a, const Vector1N &b) { return this->pImpl->Complex(a,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector2C TKFRLegacy<T>::Complex(const Vector2N &a, const Vector2N &b) { return this->pImpl->Complex(a,b); }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::DFT(const Vector1C &_xk, Domain domain, Option_t *option, int nSize)
                {
                        Vector1C xk = _xk;
                        TString opt = option;
                                opt.ToLower();

                        //
                        // Reshape input based on nSize
                        // NB: Numpy is doing -> nSize = nSize < 1 ? xk.size() : nSize;
                        nSize = nSize < 1 ? xk.size() : TMath::Min((int) nSize, (int) xk.size());
                        
                        int Nslices = nSize = 0 ? 0 : xk.size()/nSize;

                        //
                        // Generic case.
                        std::vector<Vector1C> xpk;
                        for(int i = 0, I = Nslices; i < I; i++) // Slice into [k*n+p] pairs
                                xpk.push_back(this->pImpl->SliceXn(xk, I, i));

                        Vector1C Xk;
                        switch(domain) {

                                case Domain::ComplexFrequency :
                                        Xk.resize(xk.size());
                                break;

                                case Domain::Frequency :
                                        Xk.resize(xk.size() > 0 ? xk.size()/2+1 : 0); 
                                break;

                                default:
                                        throw std::invalid_argument("Wrong domain provided: expected \"Domain::Frequency\" or \"Domain::ComplexFrequency\".");
                        }

                        std::vector<Vector1C> Xpk;
                        for(int i = 0, I = Nslices; i < I; i++) {

                                if(domain == Domain::Frequency) {
                                        Xpk.push_back(this->pImpl->MirroringDFT( this->pImpl->RealDFT(this->pImpl->Real(xpk[i]))));
                                } else {
                                        Xpk.push_back(this->pImpl->DFT(xpk[i]));
                                }
                        }

                        // Combine segments using Euler formula
                        for(int k = 0, K = Xk.size(); k < K; k++) {

                                Xk[k] = 0;
                                for(int p = 0; p < Nslices; p++) {

                                        double cos = TMath::Cos(2*TMath::Pi()*p*k/(Nslices*nSize));
                                        double sin = TMath::Sin(2*TMath::Pi()*p*k/(Nslices*nSize));
                                        Xk[k] = Xk[k] + Complex(cos, -sin) * Xpk[p][ROOT::IOPlus::Helpers::MathMod(k, Xpk[p].size())];
                                }
                        }

                        // Concatenate segments using Euler formula
                        if (opt.Contains((char) Normalizer::Orthonormal) || opt.Contains((char) Normalizer::Unitary)) Xk = this->pImpl->Multiply(Xk, 1./TMath::Sqrt(xk.size()));
                        else if(opt.Contains((char) Normalizer::Forward) || opt.Contains((char) Normalizer::Scalar)) Xk = this->pImpl->Multiply(Xk, 1./(xk.size()));

                        return Xk;
                }

                // template <typename T>
                // typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::DFT(TH1 *h, Option_t *option, Domain domain, int nSize)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h == NULL) return {};

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h->GetXaxis();
                //         else if(opt.Contains("y")) axis = h->GetYaxis();
                //         else if(opt.Contains("z")) axis = h->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return {};
                //         }

                //         TString freqStr = nSize < 1 ? "" : "-"+TString::Itoa(nSize, 10); // Don't move this line (use input parameter for user's sanity, although it might change in between..)
                //         TString actionStr = "";
                //                 actionStr = opt.Contains((char) Normalizer::Concatenate) ? "-"+enum2str(Normalizer::Concatenate) : actionStr;
                //                 actionStr = opt.Contains((char) Normalizer::Average) ? "-"+enum2str(Normalizer::Average)     : actionStr;

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                        
                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute DFT from histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");

                //         std::vector<T> xn = this->pImpl->template GetVectorContent<NumericT>(h);
                //         return this->DFT(xn, domain, option, nSize);
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::DFT(ComplexPart part, TH1 *h, Option_t *option, int nSize)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         TString freqStr = nSize < 1 ? "" : "-"+TString::Itoa(nSize, 10); // Don't move this line (use input parameter for user's sanity, although it might change in between..)
                //         TString actionStr = "";
                //                 actionStr = opt.Contains((char) Normalizer::Concatenate) ? "-"+enum2str(Normalizer::Concatenate) : actionStr;
                //                 actionStr = opt.Contains((char) Normalizer::Average) ? "-"+enum2str(Normalizer::Average)     : actionStr;

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                        
                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());
                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute DFT from histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                        
                //         std::vector<T> xn = this->pImpl->template GetVectorContent<NumericT>(h);
                //         Vector1C Xn = this->DFT(xn, Domain::Frequency, option, nSize);
                //         if(opt.Contains((char) Normalizer::Concatenate)) nSize = axis->GetNbins();
                        
                //         double df = fs/nSize;              // Spectral resolution

                //         h0->SetName(h->GetName() + (TString) ":"+enum2str(part) + freqStr + actionStr);
                //         h0->SetTitle(Form("Corrected Frequency Spectrum (f_{s} = %.2f Hz, nSize = %d d_{f} = %.2f); Frequency (Hz)", fs, nSize, df));

                //         // Make sure outgoing histogram ranges between [0;fNyquist]
                //         h0->SetBins(Xn.size(), 0, fs/2+1);
                //         axis->SetRangeUser(0, fs/2+1);
                        
                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, Get(part, Xn));
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }
                        
                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::iDFT(const Vector1C &_Xk, Domain domain, Option_t *option, int nSize)
                {
                        Vector1C Xk = _Xk;
                        TString opt = option;
                                opt.ToLower();

                        //
                        // Reshape input based on nSize
                        if(this->pImpl->IsOneSidedDFT(_Xk)) domain = Domain::Frequency;
                        switch(domain) {

                                case Domain::ComplexFrequency :
                                        nSize = nSize < 1 ? Xk.size() : TMath::Min((int) nSize, (int) Xk.size());

                                break;

                                case Domain::Frequency :
                                        nSize = nSize < 1 ? (Xk.size()-1)*2 : TMath::Min((int) nSize, (int) ((Xk.size()-1)*2));
                                break;

                                default:
                                throw std::invalid_argument("Unexpected domain provided..");
                        }
                        
                        //
                        // Generic case
                        Vector1C xk;
                        switch(domain) {

                                case Domain::ComplexFrequency : xk = this->pImpl->iDFT(Xk);
                                        break;

                                case Domain::Frequency : xk = Complex(this->pImpl->iRealDFT(Xk));
                                        break;

                                default:
                                        throw std::invalid_argument("Wrong domain provided: expected \"Domain::Frequency\" or \"Domain::ComplexFrequency\".");
                        }

                        xk.resize(nSize, 0);
                        
                        // Concatenate segments using Euler formula
                        if (opt.Contains((char) Normalizer::Orthonormal) || opt.Contains((char) Normalizer::Unitary))  xk = this->pImpl->Multiply(xk, 1./TMath::Sqrt(xk.size()));
                        else if(!opt.Contains((char) Normalizer::Forward) || opt.Contains((char) Normalizer::Scalar)) xk = this->pImpl->Multiply(xk, 1./(xk.size()));

                        return xk;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector2C TKFRLegacy<T>::SFT(const Vector1C &x, NumericT fs, Domain domain, Option_t *option, int nSize, NumericT noverlap, WindowType window_hamming, NumericT ab, WindowSymmetry symmetry)
                {
                        throw std::invalid_argument("SFT is not implemented yet.");
                        return {};
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::iSFT(const Vector2C &X, NumericT fs, Domain domain, Option_t *option, int nSize, NumericT noverlap, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {
                        throw std::invalid_argument("iSFT is not implemented yet.");
                        return {};
                }

                // template <typename T>
                // TH2* TKFRLegacy<T>::SFT(ComplexPart part, TH1 *h, Domain domain, Option_t *option, int nSize, NumericT noverlap, WindowType window, NumericT ab , WindowSymmetry symmetry)
                // {
                //         Vector2C sft = SFT(this->pImpl->template GetVectorContent<T>(h), h->GetXaxis()->GetBinWidth(1), domain, option, nSize, noverlap, window, ab, symmetry);
                //         throw std::invalid_argument("iSFT is not implemented yet.");
                //         return NULL;
                // }

                template <typename T>
                bool TKFRLegacy<T>::NOLA(double nSize, NumericT noverlap, WindowType window, NumericT ab, WindowSymmetry symmetry, double tolerance)
                {
                        Vector1N w = this->Window(nSize, window, ab, symmetry);

                        int H = nSize*(1 - noverlap);
                        Vector1N nola(H,0);

                        for(int i = 0, N = nSize/H + (ROOT::IOPlus::Helpers::MathMod(nSize, H) ? 1 : 0); i < nSize/H; i++) {

                                Vector1N _w(w.begin() + i*H, w.begin() + TMath::Min((int) (i+1)*H, (int) w.size()));
                                                    _w = this->pImpl->Power(_w, 2);
                                                    _w.resize(H, 0);

                                nola = this->pImpl->Add(nola, _w);
                        }

                        return this->pImpl->Min(nola) > tolerance;
                }

                template <typename T>
                bool TKFRLegacy<T>::COLA(double nSize, NumericT noverlap, WindowType window, NumericT ab, WindowSymmetry symmetry, double tolerance)
                {
                        Vector1N w = this->Window(nSize, window, ab, symmetry);

                        int H = nSize*(1 - noverlap);
                        Vector1N cola(H,0);

                        for(int i = 0, N = nSize/H + (ROOT::IOPlus::Helpers::MathMod(nSize, H) ? 1 : 0); i < nSize/H; i++) {

                                Vector1N _w(w.begin() + i*H, w.begin() + TMath::Min((int) (i+1)*H, (int) w.size()));
                                                    _w.resize(H, 0);

                                cola = this->pImpl->Add(cola, _w);
                        }

                        Vector1N dx = this->pImpl->Subtract(cola, this->pImpl->Median(cola));
                        return this->pImpl->Max(this->pImpl->Abs(dx)) < tolerance;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Inject(const Vector1C &x, const Vector1C &y, Domain domain, Option_t *option, int nSize)
                {
                        return iDFT(this->pImpl->Add(
                               DFT(x, domain, option, nSize),
                               DFT(y, domain, option, nSize))
                        );
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Inject(TH1 *h1, TH1 *h2, Option_t *option, int nSize) 
                // { 
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h1 == NULL) return NULL;
                //         if(h2 == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute convolution from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                //         Vector1N xn = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1N zn = TKFRLegacy<T>::Inject(xn, (h1 == h2 ? xn :  this->pImpl->template GetVectorContent<T>(h2)), option, nSize);

                //         double df = fs/nSize;              // Spectral resolution

                //         h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":inject");
                //         h0->SetTitle(Form("Convolution distribution (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+"); #tau", fs, nSize, df));
                //         h0->SetBins(zn.size(), 0, fs/2+1);
                //         axis->SetRangeUser(0, fs/2+1);

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, zn);
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Biquad(FilterType filter, NumericT f, NumericT fs, NumericT Q, NumericT gain)
                {
                        SOS sos;
                        switch(filter)
                        {
                                case FilterType::LowPass:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_lowpass (f/fs, Q)));
                                break;

                                case FilterType::HighPass:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_highpass(f/fs, Q)));
                                break;

                                case FilterType::BandPass:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_bandpass(f/fs, Q)));
                                break;

                                case FilterType::AllPass:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_allpass(f/fs, Q)));
                                break;

                                case FilterType::HighShelf:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_highshelf(f/fs, gain)));
                                break;

                                case FilterType::LowShelf:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_lowshelf(f/fs, gain)));
                                break;

                                case FilterType::Peak:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_peak(f/fs, Q, gain)));
                                break;
                                
                                case FilterType::Notch: [[fallthrough]];
                                case FilterType::BandStop:
                                sos.push_back(this->pImpl->SecondOrderSections(kfr::biquad_notch(f/fs, Q)));
                                break;
                                
                                default: [[fallthrough]];
                                case FilterType::None:
                                break;
                        }

                        return sos;
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Biquad(int N, FilterDesign design, const std::pair<NumericT, NumericT> &RpRs, FilterType filter, const Vector1N &f, NumericT fs)
                {
                        switch(design) {

                                case FilterDesign::Butterworth:
                                        return TKFRLegacy<T>::Butterworth(N, filter, f, fs);
                                case FilterDesign::Elliptic: 
                                        return TKFRLegacy<T>::Elliptic(N, filter, RpRs.first, RpRs.second, f, fs);
                                case FilterDesign::ChebyshevI: 
                                        return TKFRLegacy<T>::ChebyshevI(N, filter, RpRs.second, f, fs);
                                case FilterDesign::ChebyshevII: 
                                        return TKFRLegacy<T>::ChebyshevII(N, filter, RpRs.first, f, fs);
                                case FilterDesign::Bessel: 
                                        return TKFRLegacy<T>::Bessel(N, filter, f, fs);

                                default: [[fallthrough]];
                                case FilterDesign::None: return SOS();
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Biquad(const Vector1N &wp, const Vector1N &ws, FilterDesign design, const std::pair<NumericT,NumericT> &RpRs, FilterType type, const Vector1N &f, NumericT fs)
                {
                        switch(design) {

                                case FilterDesign::Butterworth: 
                                        return TKFRLegacy<T>::Butterworth(wp, ws, type, RpRs, f, fs);
                                case FilterDesign::Elliptic: 
                                        return TKFRLegacy<T>::Elliptic(wp, ws, type, RpRs, f, fs);
                                case FilterDesign::ChebyshevI: 
                                        return TKFRLegacy<T>::ChebyshevI(wp, ws, type, RpRs, f, fs);
                                case FilterDesign::ChebyshevII: 
                                        return TKFRLegacy<T>::ChebyshevII(wp, ws, type, RpRs, f, fs);
                                case FilterDesign::Bessel: 
                                        throw std::invalid_argument("Bessel order calculation not implemented.");
                                        //return TKFRLegacy<T>::Bessel(wp, ws, filter, f, fs);

                                default: [[fallthrough]];
                                case FilterDesign::None: return SOS();
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Notch(const Vector1N &f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, const std::pair<NumericT, NumericT> &filterRpRs, int N)
                {
                        SOS sos;
                        for(int i = 0; i < f.size(); i++) {

                                NumericT f0  = f[i];
                                NumericT fLow   = (f0 - df.first ) / (fs/2);
                                NumericT fHigh  = (f0 + df.second ) / (fs/2);

                                SOS _sos = TKFRLegacy<T>::Biquad(N, filterDesign, filterRpRs, FilterType::BandStop, Vector1N({fLow, fHigh}), fs);
                                sos.push_back(_sos);
                        }

                        return sos;
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Notch(const Vector1N &f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, const std::pair<NumericT, NumericT> &filterRpRs)
                {
                        SOS sos;
                        for(int i = 0; i < f.size(); i++) {

                                NumericT f0  = f[i];
                                NumericT df1 = df.first;
                                NumericT df2 = df.second;
                                
                                if(df1 < df2) {

                                        NumericT tmp = df2;
                                        df2 = df1; df1 = tmp;
                                }

                                NumericT fLow   = (f0 - df1) / (fs/2);
                                NumericT fHigh  = (f0 + df1) / (fs/2);
                                NumericT fLow2  = (f0 - df2) / (fs/2);
                                NumericT fHigh2 = (f0 + df2) / (fs/2);

                                SOS _sos = TKFRLegacy<T>::Biquad(Vector1N({fLow, fHigh}), Vector1N({fLow2, fHigh2}), filterDesign, filterRpRs, FilterType::BandStop, Vector1N({fLow, fHigh}), fs);
                                sos.insert(sos.end(), _sos.begin(), _sos.end());
                        }

                        return sos;
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Elliptic(int N, FilterType filter, NumericT rp, NumericT rs, const Vector1N &f, NumericT fs)
                {
                        switch(filter)
                        {
                                case FilterType::LowPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_lowpass (kfr::elliptic<NumericT>(N, rp, rs), f[0], fs));
                                case FilterType::HighPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_highpass(kfr::elliptic<NumericT>(N, rp, rs), f[0], fs));
                                case FilterType::BandPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandpass(kfr::elliptic<NumericT>(N, rp, rs), f[0], f[1], fs));
                                case FilterType::BandStop:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandstop(kfr::elliptic<NumericT>(N, rp, rs), f[0], f[1], fs));

                                default: [[fallthrough]];
                                        case FilterType::None:
                                        throw std::invalid_argument("Unexpected filter provided.");

                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Elliptic(const Vector1N &wp, const Vector1N &ws, FilterType filter, const std::pair<NumericT,NumericT> &RpRs, const Vector1N &f, NumericT fs)
                {
                        int N = kfr::elliptic_order(wp, ws, RpRs.first, RpRs.second);
                        return TKFRLegacy<T>::Elliptic(N, filter, RpRs.first, RpRs.second, f, fs);
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Butterworth(int N, FilterType filter, const Vector1N &f, NumericT fs)
                {
                        switch(filter)
                        {
                                case FilterType::LowPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_lowpass (kfr::butterworth<T>(N), f[0], fs));
                                case FilterType::HighPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_highpass(kfr::butterworth<T>(N), f[0], fs));
                                case FilterType::BandPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandpass(kfr::butterworth<T>(N), f[0], f[1], fs));
                                case FilterType::BandStop:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandstop(kfr::butterworth<T>(N), f[0], f[1], fs));

                                default: [[fallthrough]];
                                case FilterType::None:
                                throw std::invalid_argument("Unexpected filter provided.");
                                return SOS();
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Butterworth(const Vector1N &wp, const Vector1N &ws, FilterType filter, const std::pair<NumericT,NumericT> &RpRs, const Vector1N &f, NumericT fs)
                {
                        int N = kfr::butterworth_order(wp, ws, RpRs.first, RpRs.second);
                        return TKFRLegacy<T>::Butterworth(N, filter, f, fs);
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::ChebyshevI(int N, FilterType filter, NumericT rp, const Vector1N &f, NumericT fs)
                {
                        switch(filter) {
                                case FilterType::LowPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_lowpass (kfr::chebyshev1<NumericT>(N, rp), f[0], fs));
                                case FilterType::HighPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_highpass(kfr::chebyshev1<NumericT>(N, rp), f[0], fs));
                                case FilterType::BandPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandpass(kfr::chebyshev1<NumericT>(N, rp), f[0], f[1], fs));
                                case FilterType::BandStop:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandstop(kfr::chebyshev1<NumericT>(N, rp), f[0], f[1], fs));

                                default: [[fallthrough]];
                                case FilterType::None:
                                throw std::invalid_argument("Unexpected filter provided.");
                                return SOS();
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::ChebyshevI(const Vector1N &wp, const Vector1N &ws, FilterType filter, const std::pair<NumericT,NumericT> &RpRs, const Vector1N &f, NumericT fs)
                {
                        int N = kfr::chebyshev_order(wp, ws, RpRs.first, RpRs.second);
                        return TKFRLegacy<T>::ChebyshevI(N, filter, RpRs.first, f, fs);
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::ChebyshevII(int N, FilterType filter, NumericT rs, const Vector1N &f, NumericT fs)
                {
                        switch(filter) {
                                case FilterType::LowPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_lowpass (kfr::chebyshev2<NumericT>(N, rs), f[0], fs));
                                case FilterType::HighPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_highpass(kfr::chebyshev2<NumericT>(N, rs), f[0], fs));
                                case FilterType::BandPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandpass(kfr::chebyshev2<NumericT>(N, rs), f[0], f[1], fs));
                                case FilterType::BandStop:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandstop(kfr::chebyshev2<NumericT>(N, rs), f[0], f[1], fs));

                                default: [[fallthrough]];
                                case FilterType::None:
                                throw std::invalid_argument("Unexpected filter provided.");
                                return SOS();
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::ChebyshevII(const Vector1N &wp, const Vector1N &ws, FilterType filter, const std::pair<NumericT,NumericT> &RpRs, const Vector1N &f, NumericT fs)
                {
                        int N = kfr::chebyshev_order(wp, ws, RpRs.first, RpRs.second);
                        return TKFRLegacy<T>::ChebyshevII(N, filter, RpRs.second, f, fs);
                }

                template <typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::Bessel(int N, FilterType filter, const Vector1N &f, NumericT fs)
                {
                        switch(filter) {
                                case FilterType::LowPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_lowpass(kfr::bessel<NumericT>(N), f[0], fs));
                                case FilterType::HighPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_highpass(kfr::bessel<NumericT>(N), f[0], fs));
                                case FilterType::BandPass:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandpass(kfr::bessel<NumericT>(N), f[0], f[1], fs));
                                case FilterType::BandStop:
                                return this->pImpl->SecondOrderSections(kfr::iir_bandstop(kfr::bessel<NumericT>(N), f[0], f[1], fs));

                                default: [[fallthrough]];
                                case FilterType::None:
                                throw std::invalid_argument("Unexpected filter provided.");
                                return SOS();
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Detrend(const Vector1N &Xn, Trend detrend)
                {
                        if(detrend == Trend::None) return Xn;

                        std::pair<NumericT, NumericT> ab = this->pImpl->Regression(Xn);

                        Vector1N regression(Xn.size(), 0);
                        for(int i = 0, N = regression.size(); i < N; i++)
                                regression[i] = i*ab.first + ab.second;

                        return this->pImpl->Subtract(Xn, regression);
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Detrend(const Vector1C &Zn, Trend detrend)
                {
                        if(detrend == Trend::None) return Zn;

                        return Complex(
                                TKFRLegacy<T>::Detrend(Real(Zn), detrend),
                                TKFRLegacy<T>::Detrend(Imag(Zn), detrend)
                        );
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Noise(int N, NumericT amplitude, NumericT mean0, NumericT sigma0, NumericT alpha, NumericT mean1, NumericT sigma1)
                {
                        alpha = TMath::Min((NumericT) 1.0, TMath::Max((NumericT) 0.0, alpha));
                        double xMin = TMath::Min(
                                mean0 - 6*TMath::Abs(sigma0), 
                                mean1 - 6*TMath::Abs(sigma1)
                        );

                        double xMax = TMath::Max(
                                mean0 + 6*TMath::Abs(sigma0), 
                                mean1 + 6*TMath::Abs(sigma1)
                        );

                        NumericT amplitude0 = ROOT::IOPlus::Helpers::IsNaN(mean0) || ROOT::IOPlus::Helpers::IsNaN(sigma0) || ROOT::IOPlus::Helpers::EpsilonEqualTo(sigma0, (T) 0.0) ? 0 : amplitude;
                                 mean0      = ROOT::IOPlus::Helpers::IsNaN(mean0) ? 0 : mean0;
                                 sigma0     = ROOT::IOPlus::Helpers::IsNaN(sigma0) || ROOT::IOPlus::Helpers::EpsilonEqualTo(sigma0, (T) 0.0) ? 1 : sigma0;

                        NumericT amplitude1 = ROOT::IOPlus::Helpers::IsNaN(mean1) || ROOT::IOPlus::Helpers::IsNaN(sigma1) || ROOT::IOPlus::Helpers::EpsilonEqualTo(sigma1, (T) 0.0) ? 0 : amplitude;
                                 mean1      = ROOT::IOPlus::Helpers::IsNaN(mean1) ? 0 : mean1;
                                 sigma1     = ROOT::IOPlus::Helpers::IsNaN(sigma1) || ROOT::IOPlus::Helpers::EpsilonEqualTo(sigma1, (T) 0.0) ? 1 : sigma1;

                        TF1 *formula = new TF1("white_noise["+ROOT::IOPlus::Helpers::GetRandomStr(6)+"]", "[6]*gaus(x,[0],[1],[2]) + (1 - [6])*gaus(x,[3],[4],[5])", xMin, xMax);
                                formula->SetParameter(0, amplitude0);
                                formula->SetParameter(1, mean0);
                                formula->SetParameter(2, sigma0);
                                formula->SetParameter(3, amplitude1);
                                formula->SetParameter(4, mean1);
                                formula->SetParameter(5, sigma1);
                                formula->SetParameter(6, alpha);

                        Vector1N noise(N, 0);
                        bool bNoise0 = !ROOT::IOPlus::Helpers::EpsilonEqualTo(amplitude0, (T) 0.0) && !ROOT::IOPlus::Helpers::EpsilonEqualTo(alpha, (T) 0.0) && !ROOT::IOPlus::Helpers::EpsilonEqualTo(sigma0, (T) 0.0);
                        bool bNoise1 = !ROOT::IOPlus::Helpers::EpsilonEqualTo(amplitude1, (T) 0.0) && !ROOT::IOPlus::Helpers::EpsilonEqualTo(alpha, (T) 1.0) && !ROOT::IOPlus::Helpers::EpsilonEqualTo(sigma1, (T) 0.0);
                        if(bNoise0 || bNoise1) noise = ROOT::IOPlus::Helpers::RandomVector<T>(N, formula);
                        else noise = this->pImpl->Add(noise, amplitude0*alpha*mean0 + amplitude1*(1-alpha)*mean1);

                        formula->Delete();
                        return noise;
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Noise(TString name, TString title, NumericT fs, NumericT duration, NumericT amplitude, NumericT mean1, NumericT sigma1, NumericT alpha, NumericT mean2, NumericT sigma2)
                // {
                //         TH1D *h = new TH1D(name, title, fs * duration, 0, duration);
                //         return (TH1*) this->pImpl->AddVectorContent((TH1*) h, TKFRLegacy<T>::Noise(fs*duration, amplitude, mean1, sigma1, alpha, mean2, sigma2));
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Filtering(const Vector1N &x, std::vector<SOS> sos, Direction direction)
                {
                        switch(direction)
                        {
                                case Direction::Forward:
                                {
                                        // Prepare cascade filter based on SOS input
                                        SOS cascade;
                                        for(int i = 0, I = sos.size(); i < I; i++) {       
                                                for(int j = 0, J = sos[i].size(); j < J; j++) {

                                                        cascade.push_back(sos[i][j]);
                                                }
                                        }
                                
                                        kfr::univector<T> y = this->pImpl->kfrvector(x);

                                             if(cascade.size() <= 1    ) y = kfr::biquad<1>    (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 2    ) y = kfr::biquad<2>    (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 4    ) y = kfr::biquad<4>    (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 8    ) y = kfr::biquad<8>    (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 16   ) y = kfr::biquad<16>   (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 32   ) y = kfr::biquad<32>   (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 64   ) y = kfr::biquad<64>   (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 128  ) y = kfr::biquad<128>  (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 256  ) y = kfr::biquad<256>  (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 512  ) y = kfr::biquad<512>  (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 1024 ) y = kfr::biquad<1024> (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 2048 ) y = kfr::biquad<2048> (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 4096 ) y = kfr::biquad<4096> (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 8192 ) y = kfr::biquad<8192> (this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 16384) y = kfr::biquad<16384>(this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 32768) y = kfr::biquad<32768>(this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else if(cascade.size() <= 65536) y = kfr::biquad<65536>(this->pImpl->kfrsos(cascade), KFR__TO_HANDLE(y));
                                        else throw std::invalid_argument("Too high biquad order");
                                        
                                        return this->pImpl->stdvector(y);
                                }

                                case Direction::Backward:
                                {
                                        Vector1N y = this->pImpl->Flip(x);
                                                 y = TKFRLegacy<T>::Filtering(y, sos, Direction::Forward);

                                        return this->pImpl->Flip(y);
                                }

                                case Direction::Both: [[fallthrough]];
                                case Direction::Orthonormal: [[fallthrough]];
                                case Direction::BackwardForward: [[fallthrough]];
                                case Direction::ForwardBackward: 
                                {
                                        Vector1N y = x;
                                                 y = TKFRLegacy<T>::Filtering(y, sos, Direction::Forward);
                                                 y = TKFRLegacy<T>::Filtering(y, sos, Direction::Backward);

                                        return y;
                                }

                                default:
                                {
                                        throw std::invalid_argument("Unexpected filter provided.");
                                        return Vector1N();
                                }
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Whitening(const Vector1C &x, const Vector1N &ASD, NumericT fs, Option_t *option, int nSize, NumericT timeshift, NumericT phase)
                {
                        return Complex(
                                TKFRLegacy<T>::Whitening(Real(x), ASD, fs, option, nSize, timeshift, phase),
                                TKFRLegacy<T>::Whitening(Imag(x), ASD, fs, option, nSize, timeshift, phase)
                        );
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Whitening(const Vector1N &x, const Vector1N &ASD, NumericT fs, Option_t *option, int nSize, NumericT timeshift, NumericT phase)
                {
                        Vector1C Xn = TKFRLegacy<T>::DFT(x, Domain::Frequency, option, nSize);
                        if(!ROOT::IOPlus::Helpers::IsNaN(timeshift)) Xn = this->pImpl->TimeShift(Xn, fs, timeshift);
                        if(!ROOT::IOPlus::Helpers::IsNaN(phase)) Xn = this->pImpl->PhaseShift(Xn, phase);
                        
                        Vector1N interpASD = Interpolate(
                                this->pImpl->Range( Xn.size(), 0., fs/2.), 
                                this->pImpl->Range(ASD.size(), 0., fs/2.), ASD
                        );

                        interpASD = this->pImpl->Multiply(interpASD, TMath::Sqrt(fs/2)); // .. get normalization right
                        // @TODO, please consider to recompute DC component..
                        
                        return Real(TKFRLegacy::iDFT(this->pImpl->Divide(Xn, interpASD), Domain::Frequency, option));
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Whitening(const Vector1C &x, NumericT fs, Option_t *option, int nSize, NumericT timeshift, NumericT phase, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {
                        Vector1N ASD = TKFRLegacy<T>::AmplitudeSpectralDensity(x, fs, Domain::Frequency, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                        return Complex(
                                TKFRLegacy<T>::Whitening(Real(x), ASD, fs, option, 0, timeshift, phase),
                                TKFRLegacy<T>::Whitening(Imag(x), ASD, fs, option, 0, timeshift, phase)
                        );
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Whitening(const Vector1N &x, NumericT fs, Option_t *option, int nSize, NumericT timeshift, NumericT phase, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {
                        Vector1N ASD = TKFRLegacy<T>::AmplitudeSpectralDensity(x, fs, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                        return TKFRLegacy<T>::Whitening(x, ASD, fs, option, 0, timeshift, phase);
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Whitening(TH1 *h, TH1 *hASD, Option_t *option, int nSize, NumericT timeshift, NumericT phase)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":"+opt+"-whiten");

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }
                        
                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Whitening histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                        
                //         Vector1N x         = this->pImpl->template GetVectorContent<T>(h);
                //         Vector1N xWhiteninged = TKFRLegacy<T>::Whitening(x, this->pImpl->template GetVectorContent<T>(hASD), fs, option, 0, timeshift, phase);
                //         if(opt.Contains((char) Normalizer::Concatenate)) nSize = axis->GetNbins();

                //         if(opt.Contains("x")) h0 = this->pImpl->SetVectorContent(h0, xWhiteninged);
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::WienerFiltering(const Vector1C &x, NumericT fs, Vector1N PSD /* noise PSD */, Domain domain, Option_t *option, int nSize)
                {
                        Vector1C Xn = TKFRLegacy<T>::DFT(x, domain, option, nSize);
                        if(PSD.size()) {

                                switch(domain) {

                                case Domain::Frequency: break;
                                case Domain::ComplexFrequency: PSD = this->pImpl->MirroringPSD(PSD);
                                        break;
                                default:
                                        throw std::invalid_argument( "Unexpected domain provided.");
                                }  

                                PSD = Interpolate(this->pImpl->Range( Xn.size(), 0., fs), this->pImpl->Range(PSD.size(), 0., fs), PSD);
                        }

                        Vector1N Cn = TKFRLegacy<T>::PowerSpectralDensity(x, fs, domain, option, nSize);
                        Vector1N Sn = this->pImpl->Subtract(Cn, PSD);
                        
                        return TKFRLegacy<T>::iDFT(this->pImpl->Multiply(Xn, this->pImpl->Divide(Sn, Cn)));
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Whitening(TH1 *h, Option_t *option, int nSize, NumericT timeshift, NumericT phase, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry symmetry)
                // {
                //         TH1D *hASD = (TH1D*) TKFRLegacy<T>::AmplitudeSpectralDensity(h, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                //         return TKFRLegacy<T>::Whitening(h, hASD, option, nSize, timeshift, phase);
                // }

                template <typename T>
                std::pair<typename TKFRLegacy<T>::Vector1C, double> TKFRLegacy<T>::SignalNoiseRatio(const Vector1C &x, const Vector1C &y, NumericT fs, Vector1N PSD, Domain domain, Option_t *option, int nSize, double fLow, double fHigh)
                {
                        if(x.size() != y.size())
                                throw std::invalid_argument("Vectors are not same size: x("+TString::Itoa(x.size(),10)+") != y("+TString::Itoa(y.size(),10)+")");
                        
                        TString opt = option;
                                opt.ToLower();

                        //
                        // Noise weighted inner product x and y
                        Vector1C Xn = this->pImpl->Divide(TKFRLegacy<T>::DFT(x, domain, option, nSize), fs);
                                 Xn = TKFRLegacy<T>::FrequencyCut(Xn, fLow, fHigh);

                        Vector1C Yn = this->pImpl->Divide(TKFRLegacy<T>::DFT(y, domain, option, nSize), fs);
                                 Yn = TKFRLegacy<T>::FrequencyCut(Yn, fLow, fHigh);

                        switch(domain) {

                                case Domain::Frequency: break;
                                case Domain::ComplexFrequency: PSD = this->pImpl->MirroringPSD(PSD);
                                        break;
                                default:
                                        throw std::invalid_argument( "Unexpected domain provided.");
                        }  

                        PSD = Interpolate(this->pImpl->Range( Xn.size(), 0., fs), this->pImpl->Range(PSD.size(), 0., fs), PSD);

                        Vector1C optimalSignalDFT   = this->pImpl->Multiply(Xn, this->pImpl->Conj(Yn));
                        if(PSD.size()) optimalSignalDFT = this->pImpl->Divide(optimalSignalDFT, PSD, true);
                        
                        Vector1C optimalSignal = this->pImpl->Multiply(fs, TKFRLegacy<T>::iDFT(optimalSignalDFT, domain, option, nSize));
                        
                        //
                        // Optimal inner product
                        // NB: no need to apply iDFT in that case.. just renormalize properly and integrate
                        Vector1C optimalTemplateDFT = this->pImpl->Multiply(Yn, this->pImpl->Conj(Yn));
                        if(PSD.size()) optimalTemplateDFT = this->pImpl->Divide(optimalTemplateDFT, PSD, true);
                        nSize = nSize == 0 ? optimalTemplateDFT.size() : nSize;

                        double a = 1 + (domain == Domain::Frequency ? 1 : 0); // one-sided/two-sided unfolding
                        
                        double df = fs/nSize;
                        double sigma2 = a * df * this->pImpl->Mag(this->pImpl->Sum(optimalTemplateDFT));
                        double sigma  = TMath::Sqrt(sigma2)/TMath::Sqrt(2);

                        //
                        // Signal-to-noise ratio
                        Vector1C SignalNoiseRatio = this->pImpl->Divide(optimalSignal, sigma);
                        return std::make_pair(this->pImpl->Roll(SignalNoiseRatio, optimalSignal.size()/2.), sigma);
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Add(const Vector1N &a, const Vector1N &b ) { return this->pImpl->Add(a,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Add(const Vector1C &z1, const Vector1N &z2 ) { return this->pImpl->Add(z1,z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Add(const Vector1N &z1, const Vector1C &z2 ) { return this->pImpl->Add(z1,z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Add(const Vector1N &A, const NumericT &b) { return this->pImpl->Add(A,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Add(const Vector1C &z1, const NumericT &z2) { return this->pImpl->Add(z1,z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Add(const NumericT &z2, const Vector1C &z1) { return this->pImpl->Add(z2,z1); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Add(const Vector1C &z1, const ComplexT &z2) { return this->pImpl->Add(z1,z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Add(const ComplexT &z2, const Vector1C &z1) { return this->pImpl->Add(z2,z1); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Add(const Vector1C &z1, const Vector1C &z2) { return this->pImpl->Add(z1,z2); }

                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Abs(const NumericT &z) { return this->pImpl->Abs(z); }
                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Abs(const ComplexT &z) { return this->pImpl->Abs(z); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Abs(const Vector1N &x) { return this->pImpl->Abs(x); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Abs(const Vector1C &z) { return this->pImpl->Abs(z); }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Divide(const Vector1C &z, const Vector1N &x, bool discardNaN) { return this->pImpl->Divide(z, x, discardNaN); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Divide(const Vector1N &z, const Vector1C &x, bool discardNaN) { return this->pImpl->Divide(z, x, discardNaN); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Divide(const Vector1C &z, const NumericT &k, bool discardNaN) { return this->pImpl->Divide(z, k, discardNaN); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Divide(const Vector1N &x, const Vector1N &y, bool discardNaN) { return this->pImpl->Divide(x, y, discardNaN); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Divide(const Vector1N &a, const NumericT &k, bool discardNaN) { return this->pImpl->Divide(a, k, discardNaN); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Divide(const Vector1C &z1, const Vector1C &z2, bool discardNaN) { return this->pImpl->Divide(z1, z2, discardNaN); }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Multiply(const Vector1C &z, const Vector1N &x) { return this->pImpl->Multiply(z, x); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Multiply(const Vector1N &x, const Vector1C &z) { return this->pImpl->Multiply(x, z); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Multiply(const Vector1C &z, const NumericT &k) { return this->pImpl->Multiply(z, k); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Multiply(const NumericT &k, const Vector1C &z) { return this->pImpl->Multiply(k, z); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Multiply(const Vector1N &a, const Vector1N &b) { return this->pImpl->Multiply(a, b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Multiply(const Vector1C &z1, const Vector1C &z2) { return this->pImpl->Multiply(z1, z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Multiply(const Vector1N &a, const NumericT &k) { return this->pImpl->Multiply(a, k); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Multiply(const NumericT &k, const Vector1N &a) { return this->pImpl->Multiply(k, a); }

                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Sum(const Vector1N &v) { return this->pImpl->Sum(v); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::Sum(const Vector1C &v) { return this->pImpl->Sum(v); }
                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::SumX(const Vector1N &v) { return this->pImpl->SumX(v); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::SumX(const Vector1C &v) { return this->pImpl->SumX(v); }

                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::SumX2(const Vector1N &a) { return this->pImpl->SumX2(a); };
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::SumX2(const Vector1C &a) { return this->pImpl->SumX2(a); };
                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::SumXY(const Vector1N &a, const Vector1N &b) { return this->pImpl->SumXY(a, b); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::SumXY(const Vector1N &a, const Vector1C &b) { return this->pImpl->SumXY(a, b); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::SumXY(const Vector1C &a, const Vector1N &b) { return this->pImpl->SumXY(a, b); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::SumXY(const Vector1C &a, const Vector1C &b) { return this->pImpl->SumXY(a, b); }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const Vector1C &z1, const Vector1C &z2) { return this->pImpl->Subtract(z1,z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const Vector1N &z1, const Vector1C &z2) { return this->pImpl->Subtract(z1,z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const Vector1C &z1, const Vector1N &z2) { return this->pImpl->Subtract(z1,z2); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Subtract(const Vector1N &a, const Vector1N &b ) { return this->pImpl->Subtract(a,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Subtract(const Vector1N &a, const NumericT &b) { return this->pImpl->Subtract(a,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const Vector1C &a, const ComplexT &b) { return this->pImpl->Subtract(a,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const Vector1C &a, const NumericT &b) { return this->pImpl->Subtract(a,b); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const NumericT &b, const Vector1C &a) { return this->pImpl->Subtract(b,a); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const ComplexT &b, const Vector1C &a) { return this->pImpl->Subtract(b,a); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Subtract(const Vector1N &a, const ComplexT &b) { return this->pImpl->Subtract(a,b); }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Power(const Vector1C &X, const NumericT &y, bool handmade) { return this->pImpl->Power(X, y, handmade); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Power(const Vector1N &X, const NumericT &y, bool handmade) { return this->pImpl->Power(X, y, handmade); }
                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Power(const NumericT &x, const NumericT &y, bool handmade) { return this->pImpl->Power(x, y, handmade); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::Power(const ComplexT &x, const NumericT &y, bool handmade) { return this->pImpl->Power(x, y, handmade); }
                
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Sqrt(const Vector1C &X) { return this->pImpl->Sqrt(X); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Sqrt(const Vector1N &X) { return this->pImpl->Sqrt(X); }
                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Sqrt(const NumericT &x) { return this->pImpl->Sqrt(x); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::Sqrt(const ComplexT &X) { return this->pImpl->Sqrt(X); }

                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Mean(const Vector1N &V) { return this->pImpl->Mean(V); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::Mean(const Vector1C &V) { return this->pImpl->Mean(V); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Mean(const Vector2N &V) { return this->pImpl->Mean(V); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Mean(const Vector2C &V) { return this->pImpl->Mean(V); }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Scale(const NumericT &k, const Vector1C &z) { return this->pImpl->Scale(k, z); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Scale(const Vector1C &z, const NumericT &k) { return this->pImpl->Scale(z, k); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Scale(const NumericT &k, const Vector1N &a) { return this->pImpl->Scale(k, a); }
                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Scale(const Vector1N &a, const NumericT &k) { return this->pImpl->Scale(a, k); }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::FrequencyCut(const Vector1C &input, NumericT fs, double fLow, double fHigh)
                {
                        if(input.size() == 0) return {};
                        
                        Vector1C output(input.begin(), input.end());
                        bool isComplex = ROOT::IOPlus::Helpers::MathMod(input.size(), 2) == 0;
                        
                        int nSize = isComplex ? input.size() : (input.size()-1)*2;
                        double df = fs / nSize;

                        for(int i = 1; i <= nSize/2; i++) {

                                if(!ROOT::IOPlus::Helpers::IsNaN(fLow) && i < fLow/df) {
                                        output[i] = 0;
                                        if(isComplex) output[output.size()-i] = 0;
                                }

                                if(!ROOT::IOPlus::Helpers::IsNaN(fHigh) && i >= fHigh/df) {
                                        output[i] = 0;
                                        if(isComplex) output[output.size()-i] = 0;
                                }
                        }

                        return input;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::FrequencyMoment(int n, const Vector1C &h, NumericT fs, Vector1N  PSD, Domain domain, Option_t *option, int nSize, double fLow, double fHigh)
                {
                        Vector1C Hn = this->pImpl->Divide(TKFRLegacy<T>::DFT(h, domain, option, nSize), fs);
                                 Hn = TKFRLegacy<T>::FrequencyCut(Hn, fs, fLow, fHigh);

                        switch(domain) {

                                case Domain::Frequency: break;
                                case Domain::ComplexFrequency: PSD = this->pImpl->MirroringPSD(PSD);
                                        break;
                                default:
                                        throw std::invalid_argument( "Unexpected domain provided.");
                        }  

                        PSD = Interpolate(this->pImpl->Range( Hn.size(), 0., fs), this->pImpl->Range(PSD.size(), 0., fs), PSD);
                        
                        Vector1N fn = this->pImpl->Power(this->pImpl->Range( Hn.size(), 0., fs), n);
                        Vector1C fnHn = this->pImpl->Multiply(fn, Hn);
                        Vector1C optimalSignalDFT   = this->pImpl->Multiply(fnHn, this->pImpl->Conj(Hn));
                        if(PSD.size()) optimalSignalDFT = this->pImpl->Divide(optimalSignalDFT, PSD, true);

                        return this->pImpl->Multiply(4.*fs, Real(TKFRLegacy<T>::iDFT(optimalSignalDFT, Domain::Frequency, option, nSize)));
                }

                // template <typename T>
                // std::pair<typename TKFRLegacy<T>::Vector1C, double> TKFRLegacy<T>::SignalNoiseRatio(TH1 *h1, TH1 *h2, TH1 *hPSD, Option_t *option, int nSize, double fLow, double fHigh)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h1->GetXaxis();
                //         else if(opt.Contains("y")) axis = h1->GetYaxis();
                //         else if(opt.Contains("z")) axis = h1->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + ((TString) h1->GetName()+ ":"+opt+"-snr") + " (Please use \"x\",\"y\",\"z\")");
                //                 return std::make_pair(Vector1C(), NAN);
                //         }
                        
                //         double dt = axis->GetBinWidth(1);  // Time resolution
                //         NumericT fs = 1./dt;                 // Sample frequency

                //         Vector1N x    = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1N y    = this->pImpl->template GetVectorContent<T>(h2);
                //         Vector1N PSD  = this->pImpl->template GetVectorContent<T>(hPSD);

                //         return TKFRLegacy<T>::SignalNoiseRatio(x, y, fs, PSD, option, nSize, fLow, fHigh);
                // }

                // template <typename T>
                // std::pair<TH1*, double> TKFRLegacy<T>::SignalNoiseRatio (ComplexPart part, TH1 *h1, TH1 *h2, TH1 *hPSD, Option_t *option, int nSize, double fLow, double fHigh)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         TH1* h0 = (TH1*) h1->Clone((TString) h1->GetName()+ ":"+opt+"-snr");
                //              h0->SetTitle("Signal-To-Noise Ratio");
                        
                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return {NULL, NAN};
                //         }
                        
                //         double dt = axis->GetBinWidth(1);  // Time resolution
                //         NumericT fs = 1./dt;                 // Sample frequency
                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute SignalNoiseRatio from histogram "+h1->GetName()+" and "+h2->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");

                //         Vector1N x    = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1N y    = this->pImpl->template GetVectorContent<T>(h2);
                //         Vector1N PSD  = this->pImpl->template GetVectorContent<T>(hPSD);

                //         double sigma;
                //         Vector1C SignalNoiseRatio;

                //         std::tie(SignalNoiseRatio, sigma)  = TKFRLegacy<T>::SignalNoiseRatio(Complex(x), Complex(y), fs, PSD, Domain::ComplexFrequency, option, nSize, fLow, fHigh);
                        
                //         if(opt.Contains("x")) h0 = this->pImpl->SetVectorContent(h0, TKFRLegacy<T>::Get(part, SignalNoiseRatio));
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return {NULL, NAN};
                //         }

                //         h0->SetName((TString) h0->GetName() + ":"+opt+"-" + enum2str(part));
                //         return std::make_pair(h0, sigma);
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Downsample(const Vector1N &x, NumericT fs, NumericT newFs, int offset) 
                { 
                        // Extract subsample here..
                        if (fs <= newFs) return x;

                        if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(ROOT::IOPlus::Helpers::GCD<T>(fs, newFs), TMath::Min(fs, newFs)))
                                throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

                        Vector1N Xn = this->pImpl->SliceXn(x, fs/newFs, offset);
                                 Xn.push_back(x[x.size()-1]);

                        return Xn;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Downsample(const Vector1C &x, NumericT fs, NumericT newFs, int offset) 
                { 
                        // Extract subsample here..
                        if (fs <= newFs) return x;
                        
                        if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(ROOT::IOPlus::Helpers::GCD<T>(fs, newFs), TMath::Min(fs, newFs)))
                                throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

                        Vector1C Xn = this->pImpl->SliceXn(x, fs/newFs, offset);
                                 Xn.push_back(x[x.size()-1]);

                        return Xn;
                }


                // template <typename T>
                // TH1* TKFRLegacy<T>::Downsample(TH1 *h, Option_t *option, NumericT newFs, int offset)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h->GetXaxis();
                //         else if(opt.Contains("y")) axis = h->GetYaxis();
                //         else if(opt.Contains("z")) axis = h->GetZaxis();
                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         NumericT fs = 1./axis->GetBinWidth(1);
                //         if(ROOT::IOPlus::Helpers::EpsilonEqualTo(newFs, fs)) return h;

                //         TString property1 = Form("%.0f", newFs); 
                //         TString property2 = offset ? "["+TString::Itoa(offset,10)+"]" : "";
                        
                //         TH1* h0 = (TH1*) h->Clone((TString) h->GetName() + ":" + opt + "-" + Form("%s%s%s", "downsample", property1.Data(), property2.Data()));

                //         double dx = fs/newFs;
                //         Vector1N x       = TKFRLegacy<T>::Downsample(this->pImpl->Range(axis), fs, newFs, offset);
                //         Vector1N content = TKFRLegacy<T>::Downsample(this->pImpl->template GetVectorContent<T>(h0), fs, newFs, offset);

                //         h0->SetBins(x.size()-1, &std::vector<double>(x.begin(), x.end())[0]);
                //         axis->SetRangeUser(x[0], x[x.size()-1]);
                        
                //         if(opt.Contains("x")) {
                                
                //                 h0 = this->pImpl->SetVectorContent(h0, content);
                                
                //         } else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Upsample(const Vector1N &x, NumericT fs, NumericT newFs, int offset) 
                { 
                        // Extract subsample here..
                        if(fs >= newFs) return x;
                        if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(ROOT::IOPlus::Helpers::GCD<T>(fs, newFs), TMath::Min(fs, newFs)))
                                throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

                        return this->pImpl->InsetXn(x, newFs/fs, offset);
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Upsample(const Vector1C &x, NumericT fs, NumericT newFs, int offset) 
                { 
                        // Extract subsample here..
                        if(fs >= newFs) return x;
                        if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(ROOT::IOPlus::Helpers::GCD<T>(fs, newFs), TMath::Min(fs, newFs)))
                                throw std::invalid_argument("Invalid sampling conversion rate: " + TString::Itoa(fs,10) + " --> " + TString::Itoa(newFs,10));

                        return this->pImpl->InsetXn(x, newFs/fs, offset);
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Upsample(TH1 *h, Option_t *option, NumericT newFs, int offset)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h->GetXaxis();
                //         else if(opt.Contains("y")) axis = h->GetYaxis();
                //         else if(opt.Contains("z")) axis = h->GetZaxis();
                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         NumericT fs = 1./axis->GetBinWidth(1);
                //         if(ROOT::IOPlus::Helpers::EpsilonEqualTo(newFs, fs)) return h;

                //         TString property1 = Form("%.0f", newFs); 
                //         TString property2 = offset ? "["+TString::Itoa(offset,10)+"]" : "";
                        
                //         TH1* h0 = (TH1*) h->Clone((TString) h->GetName() + ":" + opt + "-" + Form("%s%s%s", "downsample", property1.Data(), property2.Data()));

                //         double dx = newFs / fs;
                //         int nbins = axis->GetNbins()*dx;
                //         Vector1N x       = Interpolate(nbins, this->pImpl->Range(axis));
                //         Vector1N content = Upsample(this->pImpl->template GetVectorContent<T>(h0), fs, newFs, offset);

                //         h0->SetBins(x.size()-1, &std::vector<double>(x.begin(), x.end())[0]);
                //         axis->SetRangeUser(x[0], x[x.size()-1]);
                        
                //         if(opt.Contains("x")) {
                                
                //                 h0 = this->pImpl->SetVectorContent(h0, content);
                                
                //         } else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Decimate(const Vector1N &x, NumericT fs, NumericT fdown, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<NumericT, NumericT> &filterRpRs) 
                {
                        if(fs <= fdown) return x;

                        double M = fs/fdown;
                        SOS filter = TKFRLegacy<T>::Biquad(filterOrder, filterDesign, filterRpRs, FilterType::LowPass, {static_cast<T>(fs/(2*M))}, fs);
                        
                        switch(method) {

                                case Method::Polyphase: 
                                throw std::invalid_argument("Polyphase filter not implemented yet..");

                                default:
                                case Method::DFT:
                                return Downsample(TKFRLegacy<T>::Filtering(x, filter), fs, fdown, offset);
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Decimate(const Vector1C &x, NumericT fs, NumericT fdown, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<NumericT, NumericT> &filterRpRs) 
                {
                        if(fs <= fdown) return x;

                        double M = fs/fdown;
                        SOS filter = TKFRLegacy<T>::Biquad(filterOrder, filterDesign, filterRpRs, FilterType::LowPass, {static_cast<T>(fs/(2*M))}, fs);
                        
                        switch(method) {

                                case Method::Polyphase: 
                                throw std::invalid_argument("Polyphase filter not implemented yet..");

                                default:
                                case Method::DFT:
                                return Downsample(TKFRLegacy<T>::Filtering(x, filter), fs, fdown, offset);
                        }
                };

                // template <typename T>
                // TH1* TKFRLegacy<T>::Decimate(TH1 *h, Option_t *option, NumericT fdown, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<NumericT, NumericT> &filterRpRs)
                // {
                //         if(h == NULL) return NULL;
                        
                //         TString opt = option;
                //                 opt.ToLower();

                //         TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":" + opt + "-decimate["+TString::Itoa(fdown,10)+","+TString::Itoa(offset,10)+"]");

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();
                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         NumericT fs = 1./axis->GetBinWidth(1);
                //         if(ROOT::IOPlus::Helpers::EpsilonEqualTo(fdown, fs)) return h;

                //         double dx = fdown / fs;
                        
                //         Vector1N x       = TKFRLegacy<T>::Downsample(this->pImpl->Range(axis), fs, fdown);
                //         Vector1N content = TKFRLegacy<T>::Decimate  (this->pImpl->template GetVectorContent<T>(h0), fs, fdown, offset, method, filterOrder, filterDesign);

                //         h0->SetBins(x.size()-1, &std::vector<double>(x.begin(), x.end())[0]);
                //         axis->SetRangeUser(x[0], x[x.size()-1]);
                        
                //         if(opt.Contains("x")) {
                                
                //                 h0 = this->pImpl->SetVectorContent(h0, content);
                                
                //         } else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Interpolate(int N, const Vector1N &y, ROOT::Math::Interpolation::Type type)
                {
                        Vector1N x0 = this->pImpl->Range(N+1, 1./(N));
                        Vector1N x  = this->pImpl->Range(y.size(), 1./(y.size()-1));

                        return Interpolate(x0, x, y, type);
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Interpolate(const Vector1N &x0, const ROOT::Math::Interpolator &itp)
                {
                        Vector1N y0;
                        for(int i = 0, N = x0.size(); i < N; i++)
                                y0.push_back(itp.Eval(x0[i]));

                        return y0;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Interpolate(const Vector1N &x0, const Vector1N &x, const Vector1N &y, ROOT::Math::Interpolation::Type type)
                {
                        return Interpolate(x0, this->pImpl->Interpolator(x, y, type)); 
                }

                // template <typename T>
                // TH1D *TKFRLegacy<T>::Interpolate(const Vector1N &x0, TH1D *h, ROOT::Math::Interpolation::Type type)
                // { 
                //         if(x0.size() == 0) return NULL;

                //         Vector1N x = (Vector1N) this->pImpl->GetVectorAxis((TH1*) h);
                //         Vector1N y = (Vector1N) this->pImpl->template GetVectorContent<NumericT>((TH1*) h);
                                
                //         if(!this->pImpl->Contains(x0[0], x)) return NULL;
                //         if(!this->pImpl->Contains(x0[x0.size() - 1], x)) return NULL;

                //         Vector1N y0 = Interpolate(x0, this->pImpl->Interpolator(x, y, type));
                //         return (TH1D*) ROOT::IOPlus::Helpers::Histogram((TString) h->GetName() + ":x-interpolate", h->GetTitle(), x0, y0);
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Interpolate(const Vector1N &x, NumericT fs, NumericT fup, int offset, Method method, int filterOrder, FilterDesign filterDesign, const std::pair<NumericT, NumericT> &filterRpRs) 
                {
                        if(fs >= fup) return x;

                        double L = fup/fs;
                        SOS filter = TKFRLegacy<T>::Biquad(filterOrder, filterDesign, filterRpRs, FilterType::LowPass, {static_cast<T>(fs/(2*L))}, fs);

                        switch(method) {

                                case Method::Polyphase: 
                                throw std::invalid_argument("Polyphase filter not implemented yet..");

                                default:
                                case Method::DFT:
                                return this->pImpl->Multiply(TKFRLegacy<T>::Filtering(Upsample(x, fs, fup, offset), filter), L);
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Resample(const Vector1C &x, NumericT fs, double fc, double bw, Method method) 
                {

                        ComplexT dc = this->pImpl->Mean(x);
                        Vector1C y = this->pImpl->Subtract(x, dc);

                        double df = fs/x.size();
                        if(bw < fs) {

                                if(fc-bw/2 < 0 || fc+bw/2 > fs) {
                                TPrint::Warning(__METHOD_NAME__, "Selected window [%.2f, %.2f] is out of range [0,%.2f]", (fc-bw/2.), (fc+bw/2.), fs);
                                }

                                y = this->pImpl->FrequencyShift(y, fs, -fc);
                                y = TKFRLegacy<T>::Resample(y, fs, bw, Domain::ComplexFrequency, method);
                                y = this->pImpl->FrequencyShift(y, bw, bw/2.);

                        } else if(bw > fs) {

                                y = this->pImpl->FrequencyShift(y, fs, fs/2);
                                y = TKFRLegacy<T>::Resample(y, fs, bw, Domain::ComplexFrequency, method);
                                y = this->pImpl->FrequencyShift(y, bw, fc);
                        }

                        return this->pImpl->Add(y, dc);
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Resample(const Vector1C &x, NumericT fs, NumericT newFs, Domain domain, Method method) 
                {
                        double duration = x.size()/fs;
                        switch(method) {

                                case Method::Polyphase: 
                                throw std::invalid_argument("Polyphase filter not implemented yet..");

                                default:
                                case Method::DFT: 
                                {
                                        Vector1C Xn = DFT(x, domain);                                
                                        return TKFRLegacy<T>::iDFT(this->pImpl->ResizeDFT(Xn, SpectrumProperty::TwoSided, duration*newFs), domain, "", duration*newFs);
                                }
                        }
                };

                // template <typename T>
                // TH1* TKFRLegacy<T>::Resample(TH1 *h, Option_t *option, NumericT newFs, Method method)
                // {
                //         if(h == NULL) return NULL;
                        
                //         TString opt = option;
                //                 opt.ToLower();

                //         TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":" + opt + "-resample["+TString::Itoa(newFs,10)+"]");

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();
                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         NumericT fs = 1./axis->GetBinWidth(1);
                //         if(ROOT::IOPlus::Helpers::EpsilonEqualTo(newFs, fs)) return h;

                //         Vector1N x;
                //         if(fs < newFs) {

                //                 double dx = newFs / fs;
                //                 int nbins = axis->GetNbins()*dx;
                //                 x = Interpolate(nbins, this->pImpl->Range(axis));
                        
                //         } else {

                //                 double dx = fs / newFs;
                //                 x = TKFRLegacy<T>::Downsample(this->pImpl->Range(axis), fs, newFs);
                //         }    
                        
                //         Vector1N content = TKFRLegacy<T>::Resample(this->pImpl->template GetVectorContent<T>(h0), fs, newFs, spectrum, method);
                        
                //         h0->SetBins(x.size()-1, &std::vector<double>(x.begin(), x.end())[0]);
                //         axis->SetRangeUser(x[0], x[x.size()-1]);
                        
                //         if(opt.Contains("x")) {
                                
                //                 h0 = this->pImpl->SetVectorContent(h0, content);
                                
                //         } else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::MatchedFiltering(const Vector1C &x, NumericT fs, Vector1N PSD, Domain domain, Option_t *option, int nSize)
                {
                        throw std::invalid_argument("Matched filtering is not implemented yet.");
                        return {};
                }

                // template <typename T>
                // typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::MatchedFiltering(TH1 *h, TH1 *hNoisePSD, Domain domain, Option_t *option, int nSize)
                // {
                //         throw std::invalid_argument("Matched filtering is not implemented yet.");
                //         return {};
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Filtering(TH1 *h, Option_t *option, TString alias, std::vector<SOS> sos, Direction direction)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         TString directionStr = enum2str(direction);
                //                 directionStr = !directionStr.EqualTo("") ? "-"+directionStr : "";
                //         TString aliasStr = alias;
                //                 aliasStr = !aliasStr.EqualTo("") ? "-"+aliasStr : "";

                //         TH1* h0 = (TH1*) h->Clone((TString) h->GetName()+ ":" + opt + aliasStr + directionStr);

                //         if(opt.Contains("x")) {

                //                 this->pImpl->SetVectorContent(h0, TKFRLegacy<T>::Filtering(this->pImpl->template GetVectorContent<T>(h), sos, direction));

                //         } else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Filtering(const Vector1N &Xn, FilterType filter, const Vector1N &cutoff, WindowType window, NumericT ab, WindowSymmetry  symmetry)
                {
                        kfr::univector<T> Yn = this->pImpl->kfrvector(Xn);

                        KFR__EXPRESSION_HANDLE<T> Wn = KFR__TO_HANDLE(this->pImpl->kfrvector(TKFRLegacy<T>::Window(Xn.size(), window, ab, symmetry)));

                        switch(filter) {
                                case FilterType::LowPass:
                                fir_lowpass(Yn, cutoff[0], Wn, true);
                                break;

                                case FilterType::HighPass:
                                fir_highpass(Yn, cutoff[0], Wn, true);
                                break;

                                case FilterType::BandPass:
                                fir_bandpass(Yn, cutoff[0], cutoff[1], Wn, true);
                                break;

                                case FilterType::Notch: [[fallthrough]];
                                case FilterType::BandStop:
                                fir_bandstop(Yn, cutoff[0], cutoff[1], Wn, true);
                                break;

                                case FilterType::None:
                                return Xn;

                                case FilterType::LowShelf: [[fallthrough]];
                                case FilterType::HighShelf: [[fallthrough]];
                                case FilterType::Peak: [[fallthrough]];
                                case FilterType::AllPass:
                                throw std::invalid_argument("Unexpected filter implemented.");
                                return Vector1N();
                        }

                        return this->pImpl->stdvector(Yn);
                        
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Filtering(TH1 *h, Option_t *option, FilterType filter, const Vector1N &cutoff, WindowType window, NumericT ab, WindowSymmetry  symmetry)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         // Apply windowing
                //         TH1* h0 = Windowing(h, opt, window, ab, symmetry);
                //                 h0->SetName((TString) h->GetName()+":"+opt+"-"+enum2str(filter));

                //         // Apply filtering
                //         TPrint::Debug(__METHOD_NAME__, (TString) "Filter histogram \""+h->GetName()+"\" using \""+enum2str(filter)+"\" with options \""+opt.Data()+"\""); 

                //         if(opt.Contains("x")) this->pImpl->SetVectorContent(h0, TKFRLegacy<T>::Filtering(this->pImpl->template GetVectorContent<T>(h0), filter, cutoff, window, ab, symmetry));
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 if(h != h0) delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Windowing(const Vector1N &x, double dutyCycle, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {
                        if(window == WindowType::None) return x;
                        return this->pImpl->Multiply(x, Window(x.size(), dutyCycle, window, ab, symmetry));
                }
                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Windowing(const Vector1C &x, double dutyCycle, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {
                        if(window == WindowType::None) return x;
                        return this->pImpl->Multiply(x, Complex(Window(x.size(), dutyCycle, window, ab, symmetry)));
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Windowing(TH1 *h, Option_t *option, double dutyCycle, WindowType window, NumericT ab, WindowSymmetry  symmetry)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(enum2str(window).EqualTo("")) return h;

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Windowing histogram "+h->GetName()+" using \""+enum2str(window)+"\" with options \""+opt+"\"");
                //         TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+opt+"-window-"+enum2str(window));
                //                 h0 = this->pImpl->Empty(h0);

                //         Vector1N y = TKFRLegacy<T>::Windowing(this->pImpl->template GetVectorContent<T>(h), dutyCycle, window, ab, symmetry);
                //         if(opt.Contains("x")) this->pImpl->SetVectorContent(h0, y);
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;
                //                 return NULL;
                //         }

                //         return h0;
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Detrend(TH1 *h, Trend detrend, Option_t *option)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         TFitResult *r = NULL;

                //         switch(detrend) {

                //                 case Trend::None:
                //                 return h;

                //                 case Trend::Constant:
                //                 {
                //                 TPrint::Debug(__METHOD_NAME__, (TString) "Detrend with \"constant\" histogram "+h->GetName()+" with fitting options \""+opt+"\"");

                //                 TPrint::RedirectTTY(__METHOD_NAME__);
                //                 h->Fit("pol0", opt).Get();
                //                 h->Add(h->GetFunction("pol0"), -1);
                //                 TPrint::DumpTTY(__METHOD_NAME__);

                //                 return h;
                //                 }

                //                 case Trend::Linear:
                //                 {
                //                 TPrint::Debug(__METHOD_NAME__, (TString) "Detrend with \"linear\" histogram "+h->GetName()+" with fitting options \""+opt+"\"");

                //                 TPrint::RedirectTTY(__METHOD_NAME__);
                //                 h->Fit("pol1", opt).Get();
                //                 h->Add(h->GetFunction("pol1"), -1);
                //                 TPrint::DumpTTY(__METHOD_NAME__);

                //                 return h;
                //                 }

                //                 default:
                //                 throw std::invalid_argument("Unknown detrending method.");
                //         }
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Impulse(NumericT fs, double duration, const Vector1N &amps, const Vector1N &freqs, Domain domain)
                {
                        int nSize = (domain == Domain::ComplexFrequency || domain == Domain::Time) ? fs*duration : fs*duration/2+1;

                        Vector1C Xk(nSize);
                        for(int i = 0, N = freqs.size(); i < N; i++) {

                                NumericT f = freqs[i]*duration;
                                if(freqs[i] >= fs) {
                                        TPrint::Warning(__METHOD_NAME__, "Impulse at %.2fHz is out of scope (fs = %.2fHz)", freqs[i], fs);
                                        continue;
                                }

                                Xk[f] = amps[i] * static_cast<NumericT>(fs) * Complex((NumericT) TMath::Cos(TMath::Pi()/2), (NumericT) TMath::Sin(-TMath::Pi()/2));
                                if(domain == Domain::Time || domain == Domain::ComplexFrequency)
                                Xk[Xk.size()-f] = this->pImpl->Conj(Xk[f]);
                        }

                        switch(domain) {

                                case Domain::Time:
                                return TKFRLegacy<T>::iDFT(Xk, Domain::ComplexFrequency, "");
                                
                                case Domain::Frequency: [[fallthrough]];
                                case Domain::ComplexFrequency: [[fallthrough]];
                                default: return Xk;
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Chirp(const NumericT fs, const double duration, double t0, double t1, double f0, double f1, Trend method, double taper, const NumericT phase)
                {
                        TString formulaStr;

                        double _buf;
                        if(t0 > t1) { 

                                _buf = f0;
                                f0 = f1;
                                f1 = _buf;

                                _buf = t0;
                                t0 = t1;
                                t1 = _buf;
                        }

                        if(f0 <= 0 || f1 <= 0) {

                                std::cerr << Form("Frequency (`f0`,`f1`) = (%.2f,%.2f) must be greater than 0", f0, f1) << std::endl;
                                throw std::exception();
                        }

                        TF1 *formula = NULL;

                        double beta = 0;
                        switch(method) {

                                case Trend::Linear:
                                beta       = (f1-f0) / t1;
                                formula = new TF1("formula-"+ROOT::IOPlus::Helpers::GetRandomStr(), "[1] + [0] * x", t0, t1);
                                formula->SetParName  (0, "beta");
                                formula->SetParameter(0, beta);
                                formula->SetParName  (1, "f0");
                                formula->SetParameter(1, f0);
                                break;
                                
                                case Trend::Quadratic:
                                case Trend::QuadraticConcave:
                                beta       = (f1-f0)/(t1*t1);
                                formula = new TF1("formula-"+ROOT::IOPlus::Helpers::GetRandomStr(), "[2] - [0] * ([1] - x)**2", t0, t1);
                                formula->SetParName  (0, "beta");
                                formula->SetParameter(0, beta);
                                formula->SetParName  (1, "t1");
                                formula->SetParameter(1, t1-t0);
                                formula->SetParName  (2, "f1");
                                formula->SetParameter(2, f1);
                                break;

                                case Trend::QuadraticConvex:
                                beta       = (f1-f0)/(t1*t1);
                                formula = new TF1("formula-"+ROOT::IOPlus::Helpers::GetRandomStr(), "[1] + [0] * x**2", t0, t1);
                                formula->SetParName  (0, "beta");
                                formula->SetParameter(0, beta);
                                formula->SetParName  (1, "f0");
                                formula->SetParameter(1, f0);
                                break;

                                case Trend::Logarithmic:

                                if (f0*f1 <= 0.0) {

                                        std::cerr << Form("Logarithmic chirp, must have be same frequency sign (`f0`,`f1`) = (%.2f,%.2f)", f0, f1) << std::endl;
                                        throw std::exception();
                                }
                        
                                formula = new TF1("formula-"+ROOT::IOPlus::Helpers::GetRandomStr(), ROOT::IOPlus::Helpers::EpsilonEqualTo(f0, f1) ? "[0]" : "[0] * ([2]/[0])**(x/[1])", t0, t1);
                                formula->SetParName  (0, "f0");
                                formula->SetParameter(0, f0);
                                formula->SetParName  (1, "t1");
                                formula->SetParameter(1, t1-t0);
                                formula->SetParName  (2, "f1");
                                formula->SetParameter(2, f1);
                                break;

                                case Trend::Hyperbolic: 
                                formula = new TF1("formula-"+ROOT::IOPlus::Helpers::GetRandomStr(), ROOT::IOPlus::Helpers::EpsilonEqualTo(f0, f1) ? "[0]" : "[0]*[2]*[1] / (([0] - [2])*x + [2]*[1])", t0, t1);
                                formula->SetParName  (0, "f0");
                                formula->SetParameter(0, f0);
                                formula->SetParName  (1, "t1");
                                formula->SetParameter(1, t1-t0);
                                formula->SetParName  (2, "f1");
                                formula->SetParameter(2, f1);
                                break;

                                case Trend::Constant:
                                std::cerr << "Chirp method `" << enum2str(method) << "` not implemented. Use TKFRLegacy::Impulse" << std::endl; 
                                throw std::exception();

                                default: 
                                std::cerr << "Chirp method `" << enum2str(method) << "` not implemented." << std::endl; 
                                throw std::exception();
                        }

                        Vector1N sweepPolynomial = SweepFormula(fs, duration, formula, {}, taper, phase);
                        
                        if(formula) formula->Delete();
                        return sweepPolynomial;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::SweepPolynomial(const NumericT fs, const double duration, double t0, double t1, const Vector1N coeff, double taper, NumericT phase)
                {
                        TF1 *formula = new TF1("formula-"+ROOT::IOPlus::Helpers::GetRandomStr(), Form("pol%lu", TMath::Max((ULong_t) 0, coeff.size()-1)), t0, t1);
                        Vector1N sweepPolynomial = SweepFormula(fs, duration, formula, coeff, taper, phase);

                        formula->Delete();
                        return sweepPolynomial;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::SweepFormula(const NumericT fs, const double duration, TF1 *formula, const Vector1N params, double taper, NumericT phase)
                {
                        Vector1N x(fs*duration, 0);
                        phase = ROOT::IOPlus::Helpers::MathMod(phase, 2*TMath::Pi());

                        for(int i = 0, N = params.size(); i < N; i++)
                                formula->SetParameter(i, params[i]);

                        double xMin = TMath::Max(formula->GetXmin(), 0.0);
                        int iMin = xMin*fs;
                        
                        double xMax = TMath::Min(formula->GetXmax(), duration);
                        int iMax = xMax*fs;

                        for(int i = iMin, N = iMax; i < N; i++) {
                                x[i] = TMath::Cos(2 * TMath::Pi() * formula->Integral(0, i/fs) + phase);
                        }

                        x = ROOT::IOPlus::Helpers::StripNaN(x);
                        if(!ROOT::IOPlus::Helpers::IsNaN(taper)) x = TKFRLegacy<T>::Taper(x, fs, xMin, xMax, taper);

                        return x;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Gaussian(const NumericT fs, const double duration, double mean, double sigma)
                {
                        unsigned int N = fs*duration;
                        Vector1N x(N, 0);
                        for(int i = 0; i < N; i++) {
                                x[i] = gRandom->Gaus(mean, sigma);
                        }

                        return x;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::AutoRegressive(const NumericT fs, double c, Vector1N p, Vector1N AR0, Vector1N w)
                {
                        unsigned int N = fs;
                        if(N < 1) return {};

                        Vector1N AR(N, 0);
                        if(w.size() != N) {
                                std::cerr << "Wrong random noise length (= " << w.size() <<  ") provided. N must be " << N << std::endl;
                                throw std::exception();
                        }

                        if(p.size() != AR0.size()) {
                                std::cerr << "Unexpected initial conditions provided (N = " << w.size() <<  "). N must be " << p.size() << std::endl;
                                throw std::exception();
                        }

                        for(int i = 0, I = p.size(); i < N; i++) {

                                if(i < I) { 
                                        AR[i] = AR0[i]; // Initial conditions
                                        continue;
                                }

                                AR[i] = c + w[i];
                                for(int k = 0, K = p.size(); k < K; k++) {  // AR model
                                        AR[i] += p[k]*AR[(i-1)-k];
                                }
                        }

                        return AR;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::MovingAverage(const NumericT fs, Vector1N q, Vector1N w)
                {
                        unsigned int N = fs;
                        if(N < 1) return {};

                        Vector1N MA(N, 0);
                        if(w.size() != N) {

                                std::cerr << "Wrong random noise length (= " << w.size() <<  ") provided. N must be " << N << std::endl;
                                throw std::exception();
                        }

                        for(int i = 0, I = q.size(); i < N; i++) {

                                if(i < I) {
                                        MA[i] = w[i]; // Initial conditions
                                        continue;
                                }

                                for(int k = 0, K = q.size(); k < K; k++) { // MA model
                                        MA[i] += q[k] * w[(i-1)-k];
                                }
                        }

                        return MA;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::FrequencySpectrum(NumericT fs, Domain domain, ComplexT dc, TString formulaReStr, TString formulaImStr, Vector1N params, Vector1N paramSigmas, int iterations)
                {
                        TString randomStr = ROOT::IOPlus::Helpers::GetRandomStr();
                        TFormula *formulaRe = new TFormula("formula-"+randomStr+"[Re]", formulaReStr);
                        TFormula *formulaIm = new TFormula("formula-"+randomStr+"[Im]", formulaImStr);

                        Vector1C spectrum = FrequencySpectrum(fs, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations);

                        formulaRe->Delete();
                        formulaIm->Delete();
                        
                        return spectrum;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::FrequencySpectrum(NumericT fs, Domain domain, ComplexT dc, TFormula *formulaRe, TFormula *formulaIm, Vector1N params, Vector1N paramSigmas, int iterations)
                {
                        if(!formulaRe->IsValid()) {

                                TPrint::Warning(__METHOD_NAME__, (TString) "Invalid formula \""+formulaRe->GetName()+"\" provided for spectrum generation.");
                                return {};
                        }

                        if(!formulaIm->IsValid()) {

                                TPrint::Warning(__METHOD_NAME__, (TString) "Invalid formula \""+formulaIm->GetName()+"\" provided for spectrum generation.");
                                return {};
                        }

                        int nSize;
                        switch(domain) {

                                case Domain::Frequency: nSize = fs/2+1;
                                break;

                                case Domain::ComplexFrequency: nSize = fs;
                                break;
                                default:
                                throw std::invalid_argument( "Unexpected domain provided.");
                        }  

                        if(params.size() < 1) iterations = 1; 
                        paramSigmas.resize(params.size(), 0);

                        Vector1C spectrum(nSize, 0);
                        spectrum[0] = dc;

                        for(int j = 0; j < iterations; j++) {

                                Vector1N _params(params.size(), 0);
                                for(int i = 0, N = _params.size(); i < N; i++) {
                                
                                _params[i] = params[i];
                                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(paramSigmas[i], (T) 0.0))
                                        _params[i] = gRandom->Gaus(params[i], paramSigmas[i]);
                                }

                                for(int i = 0; i < nSize; i++) {

                                        spectrum[i] = Complex(
                                                spectrum[i].real() + (NumericT) TVarexp::Eval(formulaRe, std::vector<double>(_params.begin(), _params.end()), i/fs)[params.size()] / iterations, 
                                                spectrum[i].imag() + (NumericT) TVarexp::Eval(formulaIm, std::vector<double>(_params.begin(), _params.end()), i/fs)[params.size()] / iterations
                                        );
                                }
                        }

                        return spectrum;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Waveform(NumericT fs, double duration, TString formulaStr, Vector1N params, Vector1N paramSigmas, int iterations)
                {
                        TFormula *formula = new TFormula("formula-"+ROOT::IOPlus::Helpers::GetRandomStr(), formulaStr);
                        Vector1N waveform = Waveform(fs, duration, formula, params, paramSigmas, iterations);
                        
                        formula->Delete();
                        return waveform;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Waveform(NumericT fs, double duration, TFormula *formula , Vector1N params, Vector1N paramSigmas, int iterations)
                {
                        if(!formula->IsValid()) {

                                TPrint::Warning(__METHOD_NAME__, (TString) "Invalid formula provided for waveform generation with \""+formula->GetName()+"\".");
                                return {};
                        }

                        if(ROOT::IOPlus::Helpers::IsNaN(duration)) {

                                TPrint::Warning(__METHOD_NAME__, (TString) "Unexpected duration provided for waveform generation with \""+formula->GetName()+"\".");
                                return {};
                        }

                        if(params.size() < 1) iterations = 1; 
                        paramSigmas.resize(params.size(), 0);

                        Vector1N waveform(duration*fs, 0);
                        for(int j = 0; j < iterations; j++) {

                                Vector1N _params(params.size(), 0);
                                for(int i = 0, N = _params.size(); i < N; i++) {
                                
                                _params[i] = params[i];
                                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(paramSigmas[i], (T) 0.0))
                                        _params[i] = gRandom->Gaus(params[i], paramSigmas[i]);
                                }

                                for(int i = 0, N = duration*fs; i < N; i++) {
                                        waveform[i] += (NumericT) TVarexp::Eval(formula, std::vector<double>(_params.begin(), _params.end()), i/fs)[params.size()] / iterations;
                                }
                        }

                        return waveform;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Waveform(NumericT fs, double duration, double dutyCycle, WindowType window, NumericT ab, WindowSymmetry  symmetry)
                {
                        double size = fs;
                        double sizeOn = size * dutyCycle;
                        
                        Vector1N _canonical = TKFRLegacy<T>::Window(sizeOn, dutyCycle, window, ab, symmetry);
                        if(symmetry != WindowSymmetry ::symmetric) _canonical = this->pImpl->SymmetricResize(_canonical, size);
                        
                        return this->pImpl->Repeat(_canonical, duration);
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Window(size_t N, double dutyCycle, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {
                        dutyCycle = TMath::Min(1., dutyCycle);
                        dutyCycle = TMath::Max(0., dutyCycle);
                        
                        int Ni = N * dutyCycle;
                        if(Ni == 0) return Vector1N(N, 0);

                        Vector1N _canonical;
                        kfr::univector<T> _unicanonical;

                        switch(window)
                        {
                                case WindowType::Bartlett:
                                _unicanonical = kfr::window_bartlett(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::BartlettHann:
                                _unicanonical = kfr::window_bartlett_hann(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Blackman:
                                _unicanonical = kfr::window_blackman(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::BlackmanHarris:
                                _unicanonical = kfr::window_blackman_harris(Ni, this->pImpl->kfrsymmetry(symmetry));
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Bohman:
                                _unicanonical = kfr::window_bohman(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Cosine:
                                _unicanonical = kfr::window_cosine(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                        case WindowType::Tukey:
                                _unicanonical = ROOT::IOPlus::Helpers::IsNaN(ab) ? kfr::window_tukey(Ni, TKFRLegacy<T>::Impl::WINDOW_DEFAULT_TUKEY_ALPHA) : kfr::window_tukey(Ni, ab);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                        case WindowType::PlanckTaper:
                                _unicanonical = ROOT::IOPlus::Helpers::IsNaN(ab) ? kfr::window_planck_taper(Ni, TKFRLegacy<T>::Impl::WINDOW_DEFAULT_PLANCK_TAPER_EPSILON) : kfr::window_planck_taper(Ni, ab);
                                _canonical = this->pImpl->stdvector(_unicanonical);

                                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(_canonical[0], (T) 1.)) _canonical[0] = 0;
                                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(_canonical[Ni-1], (T) 1.)) _canonical[Ni-1] = 0;
                                break;

                        case WindowType::PlanckTaperLeft:
                                _unicanonical = ROOT::IOPlus::Helpers::IsNaN(ab) ? kfr::window_planck_taper(Ni, TKFRLegacy<T>::Impl::WINDOW_DEFAULT_PLANCK_TAPER_EPSILON) : kfr::window_planck_taper(Ni, ab);
                                _canonical = this->pImpl->stdvector(_unicanonical);

                                for(int i = 0; i < Ni/2; i++) 
                                        _canonical[Ni-i - 1] = 1;
                                
                                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(_canonical[0], (T) 1.)) _canonical[0] = 0;
                                break;

                        case WindowType::PlanckTaperRight:
                                _unicanonical = ROOT::IOPlus::Helpers::IsNaN(ab) ? kfr::window_planck_taper(Ni, TKFRLegacy<T>::Impl::WINDOW_DEFAULT_PLANCK_TAPER_EPSILON) : kfr::window_planck_taper(Ni, ab);
                                _canonical = this->pImpl->stdvector(_unicanonical);

                                for(int i = 0; i < Ni/2; i++) 
                                        _canonical[i] = 1;

                                if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(_canonical[Ni-1], (T) 1.)) _canonical[Ni-1] = 0;
                                break;

                                case WindowType::Sine:
                                _unicanonical = kfr::window_cosine(Ni);
                                _canonical = this->pImpl->Rotate(this->pImpl->stdvector(_unicanonical), Ni/2);
                                break;

                                case WindowType::FlatTop:
                                _unicanonical = kfr::window_flattop(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Gaussian:
                                _unicanonical = ROOT::IOPlus::Helpers::IsNaN(ab) ? kfr::window_gaussian(Ni) : kfr::window_gaussian(Ni, ab);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Hamming:
                                _unicanonical = ROOT::IOPlus::Helpers::IsNaN(ab) ? kfr::window_hamming(Ni)  : kfr::window_hamming(Ni, ab);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Hann:
                                _unicanonical = kfr::window_hann(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Kaiser:
                                _unicanonical = ROOT::IOPlus::Helpers::IsNaN(ab) ? kfr::window_kaiser(Ni)   : kfr::window_kaiser(Ni, ab);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Lanczos:
                                _unicanonical = kfr::window_lanczos(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Triangle: [[fallthrough]];
                                case WindowType::Triangular:
                                _unicanonical = kfr::window_triangular(Ni);
                                _canonical = this->pImpl->stdvector(_unicanonical);
                                break;

                                case WindowType::Sawtooth:
                                _canonical.resize(Ni, 0);

                                for(int i = 0; i < Ni; i++)
                                        _canonical[i] = i/(Ni-1.);

                                break;

                                case WindowType::Rectangle: [[fallthrough]];
                                case WindowType::Rectangular:            
                                _canonical = Vector1N(Ni, !ROOT::IOPlus::Helpers::IsNaN(ab) ? ab : 1.);
                                break;

                                case WindowType::Square:
                                
                                _canonical = Vector1N(Ni, 0);
                                for(int i = 0; i < Ni; i++)
                                        _canonical[i] = (i+1)%2/(Ni-1);

                                break;

                                case WindowType::Zeros: [[fallthrough]];
                                case WindowType::ZeroPad:
                                _canonical = Vector1N(Ni, 0);
                                break;

                                default      : [[fallthrough]];
                                case WindowType::None: [[fallthrough]];
                                case WindowType::Unit:
                                _canonical = Vector1N(Ni, 1);
                        }

                        if(symmetry == WindowSymmetry::symmetric)
                                _canonical = this->pImpl->SymmetricResize(_canonical, N);

                        return _canonical;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Taper(Vector1C x, NumericT fs, double T0, double T1, NumericT ab, WindowSymmetry symmetry)
                {
                        if(T0 > T1) {

                                double _buf = T1;
                                T1 = T0;
                                T0 = T1;
                        }

                        Vector1N window = TKFRLegacy<T>::Window(TMath::Abs(T0-T1) * fs, WindowType::PlanckTaper, ab, symmetry);
                        window.resize(x.size(),0);
                        window = this->pImpl->Roll(window, -T0*fs);

                        return this->pImpl->Multiply(x, window);
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Taper(TH1 *h, double T0, double T1, Option_t *option, NumericT ab, WindowSymmetry symmetry)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Taper histogram "+h->GetName()+" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (T = ["+Form("%.2fs; %.2fs", T0, T1)+"])");

                //         Vector1N xn = TKFRLegacy<T>::Taper(this->pImpl->template GetVectorContent<T>(h), fs, T0, T1, ab, symmetry);  
                //         h0->SetName (h->GetName()  + (TString) Form(":%s-taper", opt.Data()));
                //         h0->SetTitle(h->GetTitle() + (TString) Form(" / Tapered [%.4f, %.4f]", T0, T1));

                //         return h0;
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::PowerSpectrum(TH1 *h1, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry symmetry)
                // {
                //         TH1 *h = TKFRLegacy<T>::CrossSpectrum(ComplexPart::Magnitude, h1, h1, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);

                //         TString hname = (TString) h->GetName();
                //                 hname = hname.ReplaceAll((TString) h1->GetName()+"_%_"+h1->GetName()+":cs-", (TString) h1->GetName()+":ps-");

                //         TString htitle  = (TString) h->GetTitle();
                //                 htitle  = htitle.ReplaceAll("Cross Spectrum", "Power Spectrum");
                //                 htitle += "; Frequency (Hz)";
                //                 htitle += "; Magnitude (Strain)";

                //         h->SetName((TString) hname);
                //         h->SetTitle((TString) htitle);

                //         return h;
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::CrossSpectrum(ComplexPart part, TH1 *h1,  TH1 *h2, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry symmetry)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h1 == NULL) return NULL;
                //         if(h2 == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency

                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute Cross-Spectrum from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                //         Vector1N xn  = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1N Sxy = this->Get(part, CrossSpectrum(xn, (h1 == h2 ? xn : this->pImpl->template GetVectorContent<T>(h2)), fs, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry));

                //         if(opt.Contains((char) Normalizer::Concatenate)) nSize = axis->GetNbins();
                //         double df = fs/nSize;              // Spectral resolution

                //         TString windowing = "";
                //         switch(estimator) {
                //                 case Estimator::Periodogram:
                //                         windowing = "";
                //                 break;

                //                 default:
                //                         windowing = enum2str(window);
                //                         windowing = !windowing.EqualTo("") ? "-"+windowing : "";
                //         }

                //         h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":cs-"+windowing);
                //         h0->SetTitle(Form("Cross Spectrum (f_{s} = %.2f Hz, nSize = %d, d_{f} = %.2f); Frequency (Hz)", fs, nSize, df));

                //         // Make sure outgoing histogram ranges between [0;fNyquist]
                //         h0->SetBins(Sxy.size(), 0, fs/2+1);
                //         axis->SetRangeUser(0, fs/2+1);

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, Sxy);
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                // template <typename T> TH1* TKFRLegacy<T>::CrossPeriodogram(TH1 *h1, TH1 *h2, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry) 
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h1 == NULL) return NULL;
                //         if(h2 == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency

                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute Cross-Periodogram from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                //         Vector1N xn  = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1N Sxy = TKFRLegacy<T>::CrossPeriodogram(xn, (h1 == h2 ? xn : this->pImpl->template GetVectorContent<T>(h2)), fs, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);

                //         if(opt.Contains((char) Normalizer::Concatenate)) nSize = axis->GetNbins();
                //         double df = fs/nSize;              // Spectral resolution

                //         TString windowing = enum2str(window);
                //                 windowing = !windowing.EqualTo("") ? "-"+windowing : "";
                       
                //         h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":crossperiodogram"+windowing);
                //         h0->SetTitle(Form("Cross Periodogram (f_{s} = %.2f Hz, nSize = %d, d_{f} = %.2f); Frequency (Hz)", fs, nSize, df));

                //         // Make sure outgoing histogram ranges between [0;fNyquist]
                //         h0->SetBins(Sxy.size(), 0, fs/2+1);
                //         axis->SetRangeUser(0, fs/2+1);

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, Sxy);
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::CrossSpectrum(const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain, Option_t *option,  int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {
                        if(x.size() != y.size())
                                throw std::invalid_argument( "vectors are not same size: x("+TString::Itoa(x.size(),10)+") != y("+TString::Itoa(y.size(),10)+")");

                        nSize = nSize < 1 ? x.size() : TMath::Min(nSize, (int) x.size());

                        switch(estimator) {

                                case Estimator::Periodogram: // NB: This estimator is not using Windowing, nor overlap
                                {
                                        return TKFRLegacy<T>::CrossSpectrum(
                                                x, y, fs, domain, option, nSize, 
                                                Estimator::ModifiedPeriodogram, noverlap, average, detrend, 
                                                WindowType::None, ab, symmetry
                                        );
                                }

                                case Estimator::ModifiedPeriodogram: // NB: This estimator is not using overlap
                                {
                                        Vector1C X = TKFRLegacy<T>::DFT(Windowing(Detrend(x, detrend), window, ab, symmetry), domain, "o", nSize);
                                        Vector1C Y = TKFRLegacy<T>::DFT(Windowing(Detrend(y, detrend), window, ab, symmetry), domain, "o", nSize);
                                        Vector1C ESD = this->pImpl->Multiply(this->pImpl->Conj(X), Y);

                                        // Apply window correction factor: This is squared sum of bins, rescaled by 1/nSize
                                        nSize = ESD.size();

                                        Vector1N _window = TKFRLegacy<T>::Window(nSize, window, ab, symmetry);
                                        double w2 = this->pImpl->SumX2(_window)/nSize;

                                        // Scaling is applied to correct for the histogram binning
                                        return this->pImpl->Multiply(ESD, 2. / (w2 * fs));  
                                }

                                case Estimator::LombScargle:
                                {
                                        // https://jakevdp.github.io/blog/2015/06/13/lomb-scargle-in-python/
                                        throw std::invalid_argument( "Lomb-Scargle PSD not implemented.");
                                        return {};
                                }

                                case Estimator::BlackmanTukey:
                                {   
                                        throw std::invalid_argument( "Blackman-Tukey PSD not implemented yet.");
                                }

                                case Estimator::Bartlett:
                                {
                                        return TKFRLegacy<T>::CrossSpectralDensity(x, y, fs, domain, option, nSize, Estimator::Welch, 0.0, average, detrend, window, ab, symmetry);
                                }

                                case Estimator::Welch:
                                {
                                        Vector2C X = SFT(x, fs, domain, option, nSize, noverlap, window, ab, symmetry);
                                        Vector2C Y = SFT(y, fs, domain, option, nSize, noverlap, window, ab, symmetry);
                                        // {    
                                        //         if(noverlap >= 1 && noverlap < 0)
                                        //                 throw std::invalid_argument("Unexpected number of overlap requested (0 <= noverlap < 1).");
                                                
                                        //         Vector2N spectrogram;
                                        //         std::vector<std::thread *> workers;

                                        //         int N = x.size();
                                        //         int L = nSize;
                                        //         int D = (1.-noverlap)*L;
                                        //         int K = (N-L)/D+1;

                                        //         auto _CSD = [this](int i, int K, int N, Vector1N &slice, ComplexPart part, const Vector1C xi, const Vector1C yi, NumericT fs, Domain domain, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry)
                                        //         {
                                        //                 slice = this->CrossSpectralDensity(xi, yi, fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                                        //                 if(i < 0) TKFRLegacy<T>::finishedWorkers++;

                                        //                 if(!TPrint::IsSuperQuiet() && K > 1) {
                                                        
                                        //                         TPrint::SetSingleCarriageReturn();
                                        //                         TPrint::ProgressBar(Form("Computing periodograms.." + TPrint::kNoColor + " (fs = %.2fHz; nSize = %d; t = %.2fs)", fs, nSize, N/fs), i < 0 ? (int) TKFRLegacy<T>::finishedWorkers : i+1, K);
                                        //                 }
                                        //         };

                                        //         unsigned int numThreads = std::thread::hardware_concurrency();
                                        //         for(int i = 0; i < K; i++) {

                                        //                 if(spectrogram.size() == 0) spectrogram.resize(K);
                                        //                 Vector1C xi = this->pImpl->Slice(x, i*D, nSize);
                                        //                 Vector1C yi = this->pImpl->Slice(y, i*D, nSize);

                                        //                 if(numThreads < 2 || !ROOT::IsImplicitMTEnabled() || !TKFRLegacy<T>::IsAlreadyRunningMT) {

                                        //                         _CrossSpectralDensity(i, K, N, spectrogram[i], property, xi, yi, fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry); 

                                        //                 } else { 

                                        //                         TKFRLegacy<T>::IsAlreadyRunningMT = false;
                                        //                         TKFRLegacy<T>::finishedWorkers = 0;

                                        //                         std::thread *t = new std::thread(_CSD, -1, K, N, std::ref(spectrogram[i]), property, xi, yi, fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                                        //                         workers.push_back(t);

                                        //                         int runningWorkers = workers.size() - TKFRLegacy<T>::finishedWorkers + 1;
                                        //                         while(runningWorkers >= numThreads) {
                                        //                                 runningWorkers = workers.size() - TKFRLegacy<T>::finishedWorkers + 1;
                                        //                                 gSystem->Sleep(10);
                                        //                         }
                                        //                 }
                                        //         }

                                        //         if(workers.size()) {

                                        //                 for(int i = 0, N = workers.size(); i < N; i++)
                                        //                 workers[i]->join();

                                        //                 TKFRLegacy<T>::IsAlreadyRunningMT = true;
                                        //         }

                                        //         return spectrogram;
                                        // }

                                        // switch(average) {

                                        //         case Average::Mean  : return this->pImpl->Mean(stft);
                                        //         case Average::Median: return this->pImpl->Median(stft);
                                        //         default: TPrint::Error(__METHOD_NAME__, "Unexpected average method passed as argument..");
                                        // }

                                        return {};
                                }

                                default: TPrint::Error(__METHOD_NAME__, "Unexpected estimator passed as argument..");
                        }

                        return {};
                }

                // template <typename T> TH1* TKFRLegacy<T>::Periodogram(TH1 *h0, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry) 
                // {
                //         TH1 *h = TKFRLegacy<T>::CrossPeriodogram(h0, h0, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);

                //         TString hname = (TString) h->GetName();
                //                 hname = hname.ReplaceAll((TString) h->GetName()+"_%_"+h->GetName()+":crossperiodogram-", (TString) h0->GetName()+":periodogram-");

                //         TString htitle  = (TString) h->GetTitle();
                //                 htitle  = htitle.ReplaceAll("Cross Periodogram", "Periodogram");
                //                 htitle += "; Frequency (Hz)";
                //                 htitle += "; Magnitude (Strain / Hz)";

                //         h->SetName((TString) hname);
                //         h->SetTitle((TString) htitle);

                //         return h;
                // }

                template <typename T> TKFRLegacy<T>::Vector1N TKFRLegacy<T>::CrossPeriodogram(const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry) 
                {
                        Vector1C CSD = CrossSpectralDensity(x, y, fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                        return this->Get(ComplexPart::Magnitude, CSD);
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::CrossSpectralDensity(const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry)
                {
                        Vector1C CS = CrossSpectrum(x, y, fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                        return this->Divide(CS, fs);
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::CrossSpectralDensity(ComplexPart part, TH1 *h1, TH1 *h2, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry) 
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h1 == NULL) return NULL;
                //         if(h2 == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency

                //         nSize = nSize < 1 ? axis->GetNbins() : TMath::Min(nSize, axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute CSD from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                //         Vector1N xn  = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1N Sxy = TKFRLegacy<T>::CrossSpectralDensity(part, xn, (h1 == h2 ? xn : this->pImpl->template GetVectorContent<T>(h2)), fs, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);

                //         if(opt.Contains((char) Normalizer::Concatenate)) nSize = axis->GetNbins();
                //         double df = fs/nSize;              // Spectral resolution

                //         TString windowing = "";
                //         switch(estimator) {
                //                 case Estimator::Periodogram: windowing = "";
                //                 break;

                //                 default:
                //                 windowing = enum2str(window);
                //                 windowing = !windowing.EqualTo("") ? "-"+windowing : "";
                //         }

                //         h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":csd-"+enum2str(estimator)+windowing);
                //         h0->SetTitle(Form("Cross Power Spectrum Density (f_{s} = %.2f Hz, nSize = %d, d_{f} = %.2f); Frequency (Hz)", fs, nSize, df));

                //         // Make sure outgoing histogram ranges between [0;fNyquist]
                //         h0->SetBins(Sxy.size(), 0, fs/2+1);
                //         axis->SetRangeUser(0, fs/2+1);

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, Sxy);
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::PowerSpectralDensity(TH1 *h0, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry) 
                // {
                //         TH1 *h = TKFRLegacy<T>::CrossSpectralDensity(ComplexPart::Magnitude, h0, h0, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);

                //         TString hname = (TString) h->GetName();
                //                 hname = hname.ReplaceAll((TString) h0->GetName()+"_%_"+h0->GetName()+":csd-", (TString) h0->GetName()+":psd-");

                //         TString htitle  = (TString) h->GetTitle();
                //                 htitle  = htitle.ReplaceAll("Cross Power Spectrum Density", "Power Spectrum Density");
                //                 htitle += "; Frequency (Hz)";
                //                 htitle += "; PSD (Strain / Hz)";

                //         h->SetName((TString) hname);
                //         h->SetTitle((TString) htitle);

                //         return h;
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::AmplitudeSpectralDensity(TH1 *h0, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry) 
                // {
                //         TH1 *h = this->pImpl->Sqrt(TKFRLegacy<T>::PowerSpectralDensity(h0, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry));

                //         TString hname = (TString) h->GetName();
                //                 hname = hname.ReplaceAll((TString) h0->GetName()+"_%_"+h0->GetName()+":csd-", (TString) h0->GetName()+":asd-");

                //         TString htitle  = (TString) h->GetTitle();
                //                 htitle  = htitle.ReplaceAll("Power Spectrum Density", "Amplitude Spectrum Density");
                //                 htitle += "; Frequency (Hz)";
                //                 htitle += "; ASD (Strain / #sqrt{Hz})";

                //         h->SetName((TString) hname);
                //         h->SetTitle((TString) htitle);
                //         return h;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1N TKFRLegacy<T>::Coherence(const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry symmetry)
                {   
                        Vector1N Pxy = TKFRLegacy<T>::CrossSpectralDensity(ComplexPart::Magnitude, x, y, fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                        Vector1N Pxx = TKFRLegacy<T>::PowerSpectralDensity(                        x,    fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                        Vector1N Pyy = TKFRLegacy<T>::PowerSpectralDensity(                           y, fs, domain, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);

                        return Divide(Power(Pxy, 2), Multiply(Pxx, Pyy));
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Coherence(TH1 *h1, TH1 *h2, Option_t *option, int nSize, Estimator estimator, NumericT noverlap, Average average, Trend detrend, WindowType window, NumericT ab, WindowSymmetry  symmetry)
                // {
                //         TH1* Pxy = TKFRLegacy<T>::CrossSpectralDensity(ComplexPart::Magnitude, h1, h2, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                //         TH1* Pxx = TKFRLegacy<T>::PowerSpectralDensity(                        h1,     option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);
                //         TH1* Pyy = TKFRLegacy<T>::PowerSpectralDensity(                            h2, option, nSize, estimator, noverlap, average, detrend, window, ab, symmetry);

                //         TString hname  = (TString) h1->GetName()+"_%_"+h2->GetName()+":coherence";
                //         TString htitle = "Coherence distribution";

                //         TH1 *h = (TH1*) Pxy->Clone(hname);
                //                 h->SetTitle(htitle);

                //         for(int i = 0; i <= h->GetNcells(); i++) {

                //                 if(ROOT::IOPlus::Helpers::EpsilonEqualTo(Pxx->GetBinContent(i)*Pyy->GetBinContent(i), 0.0)) h->SetBinContent(i, 0);
                //                 else h->SetBinContent(i, h->GetBinContent(i)*h->GetBinContent(i) / (Pxx->GetBinContent(i)*Pyy->GetBinContent(i)));
                                
                //                 h->SetBinError  (i, 0 /* to be computed, if needed */);
                //         }   
                        
                //         return h;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Hilbert(Vector1C x, Option_t *option, int nSize)
                {
                        Vector1C X = TKFRLegacy<T>::DFT(x, Domain::ComplexFrequency, option, nSize);
                        for(int i = 1, N = X.size(); i < N; i++)
                                X[i] = i < N/2 ? ComplexT(2.,0) * X[i] : ComplexT(0,0);
                        
                        return TKFRLegacy<T>::iDFT(X, Domain::ComplexFrequency, option, nSize);
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Hilbert(ComplexPart part, TH1 *h1, Option_t *option, int nSize) 
                // { 
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h1 == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                //         nSize = nSize < 1 ? 2*axis->GetNbins() : TMath::Min(nSize, 2*axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute Hilbert transform from histograms \"" + h1->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                //         Vector1N xn  = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1C Rxy = TKFRLegacy<T>::Hilbert(Complex(xn), option, nSize);
                //         double df = fs/nSize;              // Spectral resolution

                //         h0->SetName((TString) h1->GetName()+":hilbert");
                //         h0->SetTitle(Form("Hilbert transform (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+"); #tau", fs, nSize, df));

                //         double length = (axis->GetXmax()-axis->GetXmin());
                //         h0->SetBins(Rxy.size(), -length, length);
                //         axis->SetRangeUser(-length, length);

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, TKFRLegacy<T>::Get(part, Rxy));
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Envelope(TH1 *h0, Option_t *option, int nSize) 
                // { 
                //         TH1 *h = this->pImpl->Abs(TKFRLegacy<T>::Hilbert(h0, option, nSize));

                //         TString hname = (TString) h->GetName();
                //                 hname = hname.ReplaceAll((TString) h0->GetName()+":hilbert", (TString) h0->GetName()+":envelope");

                //         TString htitle  = (TString) h->GetTitle();
                //                 htitle  = htitle.ReplaceAll("Hilbert transform", "Envelope distribution");

                //         h->SetName((TString) hname);
                //         h->SetTitle((TString) htitle);
                //         return h;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Convolve(Vector1C x, Vector1C y, Domain domain, Option_t *option, int nSize)
                {
                        int max = TMath::Max(x.size(), y.size());

                        x.resize(2*max, 0);
                        y.resize(2*max, 0);

                        return  TKFRLegacy<T>::iDFT(this->pImpl->Multiply(
                                        TKFRLegacy<T>::DFT(x, domain, option, nSize),
                                        TKFRLegacy<T>::DFT(y, domain, option, nSize)), domain, option, nSize
                                );
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Convolve(ComplexPart part, TH1 *h1, TH1 *h2, Option_t *option, int nSize) 
                // { 
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h1 == NULL) return NULL;
                //         if(h2 == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                //         nSize = nSize < 1 ? 2*axis->GetNbins() : TMath::Min(nSize, 2*axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute convolution from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                //         Vector1N xn  = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1C Rxy = TKFRLegacy<T>::Convolve(xn, (h1 == h2 ? xn : this->pImpl->template GetVectorContent<T>(h2)), option, nSize);
                //         double df = fs/nSize;              // Spectral resolution

                //         h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":convolve");
                //         h0->SetTitle(Form("Convolution distribution (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+"); #tau", fs, nSize, df));

                //         double length = (axis->GetXmax()-axis->GetXmin());
                //         h0->SetBins(Rxy.size(), -length, length);
                //         axis->SetRangeUser(-length, length);

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, TKFRLegacy<T>::Get(part, Rxy));
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Correlate(Vector1C x, Vector1C y, Domain domain, Option_t *option, int nSize)
                {
                        int max = TMath::Max(x.size(), y.size());

                        x.resize(2*max, 0);
                        y.resize(2*max, 0);

                        return  TKFRLegacy<T>::iDFT(this->pImpl->Multiply(
                                        this->pImpl->Conj(TKFRLegacy<T>::DFT(this->pImpl->Rotate(x,max), domain, option, nSize)),
                                        TKFRLegacy<T>::DFT(y, domain, option, nSize)), domain, option, nSize
                                );
                }

                // template <typename T>
                // TH1* TKFRLegacy<T>::Correlate(ComplexPart part, TH1 *h1, TH1 *h2, Option_t *option, int nSize) 
                // { 
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h1 == NULL) return NULL;
                //         if(h2 == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h1->Clone(h1->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                //         nSize = nSize < 1 ? 2*axis->GetNbins() : TMath::Min(nSize, 2*axis->GetNbins());

                //         TPrint::Debug(__METHOD_NAME__, (TString) "Compute correlation from histograms \"" + h1->GetName() + "\" and \"" + h2->GetName() + "\" (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+")");
                //         Vector1N xn  = this->pImpl->template GetVectorContent<T>(h1);
                //         Vector1C Rxy = TKFRLegacy<T>::Correlate(xn, (h1 == h2 ? xn : this->pImpl->template GetVectorContent<T>(h2)), option, nSize);

                //         double df = fs/nSize;              // Spectral resolution

                //         h0->SetName((TString) h1->GetName()+"_%_"+h2->GetName()+":correlation");
                //         h0->SetTitle(Form("Correlation distribution (fs = "+TString::Itoa(fs,10)+") with options \""+opt+"\" (nSize = "+TString::Itoa(nSize,10)+"); #tau", fs, nSize, df));

                //         double length = (axis->GetXmax()-axis->GetXmin());
                //         h0->SetBins(Rxy.size(), -length, length);
                //         axis->SetRangeUser(-length, length);

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, TKFRLegacy<T>::Get(part, Rxy));
                //         else { //y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }

                //         return h0;
                // }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Adjust(const Vector1C &x, double scaling_factor, NumericT phase, double offset, NumericT fs, Domain domain, Option_t *option)
                {
                        Vector1C xn = this->pImpl->Multiply(x, scaling_factor);
                        if(ROOT::IOPlus::Helpers::IsNaN(phase) && ROOT::IOPlus::Helpers::IsNaN(offset)) return xn;

                        Vector1C Xn = TKFRLegacy<T>::DFT(xn, domain, option);
                        if(!ROOT::IOPlus::Helpers::IsNaN(phase) )  Xn = this->pImpl->PhaseShift(Xn, phase);
                        if(!ROOT::IOPlus::Helpers::IsNaN(offset))  Xn = this->pImpl->TimeShift (Xn, fs, offset);

                        return TKFRLegacy<T>::iDFT(Xn, domain, option);
                }

                // template <typename T>
                // typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::Adjust(TH1 *h, double scaling_factor, NumericT phase, double offset, Domain domain, Option_t *option)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h == NULL) return {};

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h->GetXaxis();
                //         else if(opt.Contains("y")) axis = h->GetYaxis();
                //         else if(opt.Contains("z")) axis = h->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return {};
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                        
                //         TPrint::Debug(__METHOD_NAME__, (TString) "Adjusting (scaling_factor, phase,offset)="+Form("(%.4f; %.4f; %.4f)", scaling_factor, phase, offset)+" for histogram "+h->GetName()+" with options \""+opt+"\"");

                //         Vector1N xn = this->pImpl->template GetVectorContent<T>(h);
                //         return TKFRLegacy<T>::Adjust(xn, scaling_factor, phase, offset, fs, domain, option);
                // }


                // template <typename T>
                // TH1* TKFRLegacy<T>::Adjust(ComplexPart part, TH1 *h, double scaling_factor, NumericT phase, double offset, Domain domain, Option_t *option)
                // {
                //         TString opt = option;
                //                 opt.ToLower();

                //         if(h == NULL) return NULL;

                //         TH1 *h0 = (TH1*) h->Clone(h->GetName() + (TString) ":"+ROOT::IOPlus::Helpers::GetRandomStr(4));
                //              h0 = this->pImpl->Empty(h0);

                //         TAxis *axis = NULL;
                //         if(opt.Contains("x")) axis = h0->GetXaxis();
                //         else if(opt.Contains("y")) axis = h0->GetYaxis();
                //         else if(opt.Contains("z")) axis = h0->GetZaxis();

                //         if(axis == NULL) {

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unknown option for " + h0->GetName() + " (Please use \"x\",\"y\",\"z\")");
                //                 return NULL;
                //         }

                //         double dt = axis->GetBinWidth(1); // Time resolution
                //         NumericT fs = 1./dt;                // Sample frequency
                        
                //         Vector1N xn = this->pImpl->template GetVectorContent<T>(h);
                //         Vector1C x = TKFRLegacy<T>::Adjust(xn, scaling_factor, phase, offset, fs, domain, option);
                        
                //         h0->SetName(h->GetName() + (TString) ":"+opt+"-adjust:"+enum2str(part));
                //         h0->SetTitle(Form("Corrected Time Distribution (A_{0} = %.2f, #phi = %.2f, t_{0} = %.2f)", scaling_factor, phase, offset));

                //         if(opt.Contains("x")) this->pImpl->AddVectorContent(h0, TKFRLegacy<T>::Get(part, x));
                //         else { // y,z ..

                //                 TPrint::Warning(__METHOD_NAME__, (TString) "Unsupported option.. please implement");
                //                 delete h0;

                //                 return NULL;
                //         }
                        
                //         return h0;
                // }

                template <typename T>
                TCanvas* TKFRLegacy<T>::BodePlot(TString name, TString title, Option_t *opt, NumericT fs, std::vector<SOS> sos)
                {
                        TVirtualPad *gParentPad = gPad;
                        TCanvas* bodePlot = new TCanvas(name, title, 500, 1000);
                                 bodePlot->Divide(1,2);

                        bodePlot->cd(1);
                        bodePlot->cd(1)->SetLogx(1);
                        bodePlot->cd(1)->SetGridx(1);
                        bodePlot->cd(1)->SetGridy(1);
                        
                        TH1 *hMag = TKFRLegacy<T>::MagnitudeResponse(name+"[m"+enum2str(ComplexPart::Magnitude)+"]", "Magnitude response", fs, sos);
                        if (hMag) {
                                hMag = this->pImpl->ScaleX(hMag, 2*TMath::Pi());
                                hMag->GetXaxis()->SetRangeUser(10, hMag->GetXaxis()->GetXmax());
                                hMag->GetXaxis()->SetTitle("Angular frequency (rad/s)");
                                hMag->Draw(opt);
                        }

                        bodePlot->cd(2);
                        bodePlot->cd(2)->SetLogx(1);
                        bodePlot->cd(2)->SetGridx(1);
                        bodePlot->cd(2)->SetGridy(1);

                        TH1 *hPhase = TKFRLegacy<T>::PhaseResponse(name+"["+enum2str(ComplexPart::Phase)+"]", "Phase response", fs, sos);
                        if (hPhase) {

                                hPhase = this->pImpl->ScaleX(hPhase, 2*TMath::Pi());
                                hPhase->GetYaxis()->SetRangeUser(-180, 180);
                                hPhase->GetXaxis()->SetRangeUser(10, hPhase->GetXaxis()->GetXmax());
                                hPhase->GetXaxis()->SetTitle("Angular frequency (rad/s)");
                                hPhase->Draw(opt);
                        }

                        gParentPad->cd();    
                        return bodePlot;
                }

                template <typename T>
                TH1* TKFRLegacy<T>::MagnitudeResponse(TString name, TString title, NumericT fs, const std::vector<SOS> &sos)
                {
                        TH1 *h = FrequencyResponse(name, title, ComplexPart::Decibel, fs, sos, Domain::Frequency);
                                h->GetXaxis()->SetTitle("Frequency [Hz]");
                                h->GetYaxis()->SetTitle("Amplitude [dB]");

                        return h;
                }

                template <typename T>
                TH1* TKFRLegacy<T>::PhaseResponse(TString name, TString title, NumericT fs, const std::vector<SOS> &sos)
                {
                        TH1 *h = FrequencyResponse(name, title, ComplexPart::Argument, fs, sos, Domain::Frequency);
                                h->GetXaxis()->SetTitle("Frequency [Hz]");
                                h->GetYaxis()->SetTitle("Phase (deg)");

                        return h;
                }

                template <typename T>
                typename TKFRLegacy<T>::Vector1C TKFRLegacy<T>::FrequencyResponse(NumericT fs, const std::vector<SOS> &sos, Domain domain)
                {
                        int nSize = fs;
                        switch(domain) {

                                case Domain::Frequency: nSize = fs/2+1;
                                break;
                                case Domain::ComplexFrequency: nSize = fs;
                                break;
                                default:
                                throw std::invalid_argument("Unexpected domain provided.");
                        }  

                        Vector1C y;
                        for(int f = 0; f < nSize; f++)
                                y.push_back(Complex(TMath::Cos(2*TMath::Pi()*f/fs), TMath::Sin(2*TMath::Pi()*f/fs)));

                        for(int k = 0, K = sos.size(); k < K; k++) {

                                y = this->Eval(y, this->pImpl->TransferFunction(sos[k]));
                        }

                        return y;
                }

                template <typename T>
                std::vector<typename TKFRLegacy<T>::TF> TKFRLegacy<T>::TransferFunction(const std::vector<ZPK> &zpk) { return this->pImpl->TransferFunction(zpk); }
                template <typename T>
                std::vector<typename TKFRLegacy<T>::TF> TKFRLegacy<T>::TransferFunction(const std::vector<SOS> &sos) { return this->pImpl->TransferFunction(sos); }
                template <typename T>
                std::vector<typename TKFRLegacy<T>::TF> TKFRLegacy<T>::TransferFunction(const std::vector<SS>   &ss) { return this->pImpl->TransferFunction(ss);  }
                template <typename T>
                std::vector<typename TKFRLegacy<T>::TF> TKFRLegacy<T>::TransferFunction(const std::vector<TF>   &tf) { return this->pImpl->TransferFunction(tf);  }
                        
                template <typename T>
                typename TKFRLegacy<T>::TF TKFRLegacy<T>::TransferFunction(const SOS &sos) { return this->pImpl->TransferFunction(sos); }
                template <typename T>
                typename TKFRLegacy<T>::TF TKFRLegacy<T>::TransferFunction(const TF  &tf ) { return this->pImpl->TransferFunction(tf);  }
                template <typename T>
                typename TKFRLegacy<T>::TF TKFRLegacy<T>::TransferFunction(const ZPK &zpk) { return this->pImpl->TransferFunction(zpk); }
                template <typename T>
                typename TKFRLegacy<T>::TF TKFRLegacy<T>::TransferFunction(const SS  &ss ) { return this->pImpl->TransferFunction(ss);  }

                template<typename T>
                typename TKFRLegacy<T>::SS TKFRLegacy<T>::StateSpace(const SOS &sos) { return this->pImpl->StateSpace(sos); }
                template<typename T>
                typename TKFRLegacy<T>::SS TKFRLegacy<T>::StateSpace(const TF  &tf ) { return this->pImpl->StateSpace(tf);  }
                template<typename T>
                typename TKFRLegacy<T>::SS TKFRLegacy<T>::StateSpace(const ZPK &zpk) { return this->pImpl->StateSpace(zpk); }
                template<typename T>
                typename TKFRLegacy<T>::SS TKFRLegacy<T>::StateSpace(const SS  &ss ) { return this->pImpl->StateSpace(ss);  }
                
                template<typename T>
                typename TKFRLegacy<T>::ZPK TKFRLegacy<T>::ZeroPoleGain(const TF  &tf ) { return this->pImpl->ZeroPoleGain(tf);  }
                template<typename T>
                typename TKFRLegacy<T>::ZPK TKFRLegacy<T>::ZeroPoleGain(const SOS &sos) { return this->pImpl->ZeroPoleGain(sos); }
                template<typename T>
                typename TKFRLegacy<T>::ZPK TKFRLegacy<T>::ZeroPoleGain(const ZPK &zpk) { return this->pImpl->ZeroPoleGain(zpk); }
                template<typename T>
                typename TKFRLegacy<T>::ZPK TKFRLegacy<T>::ZeroPoleGain(const SS  &ss ) { return this->pImpl->ZeroPoleGain(ss);  }
                
                template<typename T>
                typename std::vector<typename TKFRLegacy<T>::ZPK> TKFRLegacy<T>::ZeroPoleGain(const std::vector<TF>  &tf)  { return this->pImpl->ZeroPoleGain(tf);  }
                template<typename T>
                typename std::vector<typename TKFRLegacy<T>::ZPK> TKFRLegacy<T>::ZeroPoleGain(const std::vector<SOS> &sos) { return this->pImpl->ZeroPoleGain(sos); }
                template<typename T>
                typename std::vector<typename TKFRLegacy<T>::ZPK> TKFRLegacy<T>::ZeroPoleGain(const std::vector<SS>  &ss)  { return this->pImpl->ZeroPoleGain(ss);  }
                template<typename T>
                typename std::vector<typename TKFRLegacy<T>::ZPK> TKFRLegacy<T>::ZeroPoleGain(const std::vector<ZPK> &zpk) { return this->pImpl->ZeroPoleGain(zpk); }

                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const TF  &tf)  { return this->pImpl->SecondOrderSections(tf);  }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const SOS &sos) { return this->pImpl->SecondOrderSections(sos); }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const SS  &ss)  { return this->pImpl->SecondOrderSections(ss);  }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const ZPK &zpk) { return this->pImpl->SecondOrderSections(zpk); }

                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const std::vector<SS>  &ss ) { return this->pImpl->SecondOrderSections(ss);  }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const std::vector<ZPK> &zpk) { return this->pImpl->SecondOrderSections(zpk); }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const std::vector<TF>  &tf ) { return this->pImpl->SecondOrderSections(tf);  }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const std::vector<SOS> &sos) { return this->pImpl->SecondOrderSections(sos);  }

                template<typename T>
                typename TKFRLegacy<T>::TF  TKFRLegacy<T>::TransferFunction (const Vector1N &B, const Vector1N &A) { return this->pImpl->TransferFunction (B, A); }
                template<typename T>
                typename TKFRLegacy<T>::ZPK TKFRLegacy<T>::ZeroPoleGain(const Vector1N &B, const Vector1N &A) { return this->pImpl->ZeroPoleGain(B, A); }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const Vector1N &B, const Vector1N &A) { return this->pImpl->SecondOrderSections(B, A); }
                template<typename T>
                typename TKFRLegacy<T>::SS  TKFRLegacy<T>::StateSpace (const Vector1N &B, const Vector1N &A) { return this->pImpl->StateSpace (B, A); }
                        
                template<typename T>
                typename TKFRLegacy<T>::TF  TKFRLegacy<T>::TransferFunction (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return this->pImpl->TransferFunction (B, A); }
                template<typename T>
                typename TKFRLegacy<T>::ZPK TKFRLegacy<T>::ZeroPoleGain(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return this->pImpl->ZeroPoleGain(B, A); }
                template<typename T>
                typename TKFRLegacy<T>::SS  TKFRLegacy<T>::StateSpace (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return this->pImpl->StateSpace (B, A); }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A) { return this->pImpl->SecondOrderSections(B, A); }

                template<typename T>
                typename TKFRLegacy<T>::TF  TKFRLegacy<T>::TransferFunction (const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return this->pImpl->TransferFunction (A, B, C, D); }
                template<typename T>
                typename TKFRLegacy<T>::ZPK TKFRLegacy<T>::ZeroPoleGain(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return this->pImpl->ZeroPoleGain(A, B, C, D); }
                template<typename T>
                typename TKFRLegacy<T>::SS  TKFRLegacy<T>::StateSpace (const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return this->pImpl->StateSpace (A, B, C, D); }
                template<typename T>
                typename TKFRLegacy<T>::SOS TKFRLegacy<T>::SecondOrderSections(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D) { return this->pImpl->SecondOrderSections(A, B, C, D); }

                template <typename T>
                typename TKFRLegacy<T>::NumericT TKFRLegacy<T>::Eval(const NumericT &x, const TF &H) { return this->pImpl->Eval(x, H.B) / this->pImpl->Eval(x, H.A); }
                template <typename T>
                typename TKFRLegacy<T>::ComplexT TKFRLegacy<T>::Eval(const ComplexT &x, const TF &H) { return this->pImpl->Eval(x, H.B) / this->pImpl->Eval(x, H.A); }

                template <typename T>
                TH1* TKFRLegacy<T>::FrequencyResponse(TString name, TString title, ComplexPart part, NumericT fs, const std::vector<SOS> &sos, Domain domain)
                {
                        int nSize = fs;
                        switch(domain) {

                                case Domain::Frequency: nSize = fs/2+1;
                                break;
                                case Domain::ComplexFrequency: nSize = fs;
                                break;
                                default:
                                throw std::invalid_argument("Unexpected domain provided.");
                        }

                        TH1D *hTransfer = new TH1D(name, title, nSize, 1, nSize);
                              hTransfer->SetEntries(nSize);
                        
                        return (TH1*) this->pImpl->AddVectorContent((TH1*) hTransfer, FrequencyResponse(part, fs, sos, domain));
                }

                template <typename T>
                typename TKFRLegacy<T>::SS TKFRLegacy<T>::Analog(const SS &digital, NumericT Ts, FilterTransform method, NumericT alpha)  
                {
                        SS analog;

                        switch(method) {

                                case FilterTransform::Euler: [[fallthrough]];
                                case FilterTransform::ForwardEuler:
                                        return TKFRLegacy<T>::Analog(digital, Ts, FilterTransform::GBT, 0.0);

                                case FilterTransform::Tustin: [[fallthrough]];
                                case FilterTransform::Bilinear: 
                                        return TKFRLegacy<T>::Analog(digital, Ts, FilterTransform::GBT, 0.5);

                                case FilterTransform::Backward: [[fallthrough]];
                                case FilterTransform::BackwardEuler:
                                        return TKFRLegacy<T>::Analog(digital, Ts, FilterTransform::GBT, 1.0);

                                case FilterTransform::GBT: [[fallthrough]];
                                case FilterTransform::GeneralizedBilinear:
                                {
                                        if(!ROOT::IOPlus::Helpers::IsNaN(alpha)) {
                                                alpha = TMath::Max((NumericT) 1.0, TMath::Min((NumericT) 0.0, alpha));
                                        }

                                        throw std::invalid_argument("Digital-to-Analog conversion with `generalized bilinear transform` method is not implemented yet.");
                                        return analog;
                                }

                                default:
                                case FilterTransform::ZOH: [[fallthrough]];
                                case FilterTransform::ZeroOrderHold:
                                {
                                        throw std::invalid_argument("Digital-to-Analog conversion with `zero order hold` method is not implemented yet.");
                                        return analog;
                                }

                                case FilterTransform::FOH: [[fallthrough]];
                                case FilterTransform::FirstOrderHold:
                                {
                                        throw std::invalid_argument("Digital-to-Analog conversion with `first order hold` method is not implemented yet.");
                                        return analog;
                                }

                                case FilterTransform::Impulse: [[fallthrough]];
                                case FilterTransform::ImpulseResponse:
                                {
                                        throw std::invalid_argument("Digital-to-Analog conversion with `impulse response` method is not implemented yet.");
                                        return analog;
                                }
                        }
                }

                template <typename T>
                typename TKFRLegacy<T>::SS TKFRLegacy<T>::Digital(const SS &analog, NumericT Ts, FilterTransform method, NumericT alpha)
                {
                        SS digital;

                        const TMatrixT<T> &A = analog.A;
                        const TMatrixT<T> &B = analog.B;
                        const TMatrixT<T> &C = analog.C;
                        const TMatrixT<T> &D = analog.D;

                        switch(method) {

                                case FilterTransform::Euler: [[fallthrough]];
                                case FilterTransform::ForwardEuler:
                                     digital = TKFRLegacy<T>::Digital(analog, Ts, FilterTransform::GBT, 0.0);

                                case FilterTransform::Tustin: [[fallthrough]];
                                case FilterTransform::Bilinear: 
                                     digital = TKFRLegacy<T>::Digital(analog, Ts, FilterTransform::GBT, 0.5);

                                case FilterTransform::Backward: [[fallthrough]];
                                case FilterTransform::BackwardEuler:
                                     digital = TKFRLegacy<T>::Digital(analog, Ts, FilterTransform::GBT, 1.0);

                                case FilterTransform::GBT: [[fallthrough]];
                                case FilterTransform::GeneralizedBilinear:
                                {
                                        if(!ROOT::IOPlus::Helpers::IsNaN(alpha)) {
                                                alpha = TMath::Min((NumericT) 1.0, TMath::Max((NumericT) 0.0, alpha));
                                        }
                                
                                        TMatrixT<double> M = this->pImpl->template Matrix<NumericT>(
                                                this->pImpl->template Eye<NumericT>(A.GetNrows()) - alpha * Ts * A
                                        );
                                        TMatrixT<double> Mt = M;
                                                Mt.T();

                                        TDecompLU LU(M);
                                        TDecompLU LUt(Mt);

                                        TMatrixT<double> AA = this->pImpl->template Matrix<double>(this->pImpl->template Eye<NumericT>(A.GetNrows()) + (((NumericT) 1.0) - alpha) * Ts * A);
                                        TMatrixT<double> BB = this->pImpl->template Matrix<double>(Ts*B);
                                        TMatrixT<double> CC = this->pImpl->template Matrix<double>(C);
                                                         CC.T();
                                        
                                        LU.MultiSolve(AA);
                                        LU.MultiSolve(BB);       
                                        LUt.MultiSolve(CC);

                                        CC = CC.T();
                                        TMatrixT<double> DD = this->pImpl->template Matrix<double>(D + alpha * C * this->pImpl->template Matrix<T>(BB));

                                        digital = this->pImpl->StateSpace(AA,BB,CC,DD);
                                }

                                default:
                                case FilterTransform::ZOH: [[fallthrough]];
                                case FilterTransform::ZeroOrderHold:
                                {
                                        TMatrixT<T> M = this->pImpl->GetHorizontalBlockMatrix(analog.A, analog.B);
                                                    M.ResizeTo(M.GetNcols(), M.GetNcols());

                                        TMatrixT<T> MM = this->pImpl->Exp(Ts * M);
                                        TMatrixT<T> AA = MM.GetSub(0, A.GetNrows()-1, 0,            A.GetNcols()-1);
                                        TMatrixT<T> BB = MM.GetSub(0, B.GetNrows()-1, A.GetNcols(), A.GetNcols()+B.GetNcols()-1);
                                        TMatrixT<T> CC = C;
                                        TMatrixT<T> DD = D;

                                        digital = this->pImpl->StateSpace(AA,BB,CC,DD);
                                }

                                case FilterTransform::FOH: [[fallthrough]];
                                case FilterTransform::FirstOrderHold:
                                {   
                                        int a = A.GetNrows();
                                        int b = B.GetNcols();

                                        TMatrixT<T> M = this->pImpl->GetDiagonalBlockMatrix(Ts * this->pImpl->GetBlockMatrix(A, B), this->pImpl->Eye(b));
                                        int m = TMath::Max(M.GetNrows(), M.GetNcols());
                                        M.ResizeTo(m,m);

                                        TMatrixT<T> MM = this->pImpl->Exp(M);
                                        TMatrixT<T> MA = MM.GetSub(0, a-1, 0,   a-1);
                                        TMatrixT<T> MB = MM.GetSub(0, a-1, a,   a+b-1);
                                        TMatrixT<T> MC = MM.GetSub(0, a-1, a+b, m-1);
                                        
                                        TMatrixT<T> AA = MA;
                                        TMatrixT<T> BB = MB - MC + MA * MC;
                                        TMatrixT<T> CC = C;
                                        TMatrixT<T> DD = D + C * MC;
                                        
                                        digital = this->pImpl->StateSpace(AA,BB,CC,DD);
                                }

                                case FilterTransform::Impulse: [[fallthrough]];
                                case FilterTransform::ImpulseResponse:
                                {
                                        if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(D, (T) 0.0)) 
                                                throw std::invalid_argument("Impulse method is only applicable to strictly proper systems");

                                        TMatrixT<T> AA = this->pImpl->Exp(Ts * A);
                                        TMatrixT<T> BB = Ts * AA * B;
                                        TMatrixT<T> CC = C;
                                        TMatrixT<T> DD = Ts * C * B;

                                        digital = this->pImpl->StateSpace(AA,BB,CC,DD);
                                }
                        }

                        return digital;
                }
        }
}

template class ROOT::Signal::TKFRLegacy<float>;
template class ROOT::Signal::TKFRLegacy<double>;
