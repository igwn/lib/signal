
                // template <typename T>
                // template <typename Tx>
                // void TKFRLegacy *<T>::Print(std::map<Tx, T> m)
                // {
                //         if(m.size() == 0) std::cout << "-- Empty map" << std::endl;
                //         else std::cout << "-- {" << std::endl;
                        
                //         for(typename std::map<Tx, T>::const_iterator it = m.begin(); it != m.end(); ++it)
                //                 std::cout << "--      `" << it->first << "` : `" << it->second << "`\n";
                        
                //         std::cout << "-- }" << std::endl;
                // }

                // template <typename T>
                // template <typename Tx>
                // void TKFRLegacy *<T>::Print(std::vector<std::map<Tx, T>> vm)
                // {
                //         if(vm.size() == 0) std::cout << "-- Empty map" << std::endl;
                //         for(int i = 0, N = vm.size(); i < N; i++) Print(vm[i]);
                // }
                
                // template <typename T>
                // void TKFRLegacy *<T>::Print(const T &real, double epsilon)
                // {
                //         int epsilon_init = std::cout.precision();
                //         int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
                //         std::cout << std::setprecision(epsilon_exp)  << "`" << real << "`" ;
                //         std::cout << std::setprecision(epsilon_init) << std::flush;
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(const std::complex<T> &z, double epsilon)
                // {
                //         double real = z.real();
                //         double imag = z.imag();

                //         int epsilon_init = std::cout.precision();
                //         int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
                //         std::cout << std::setprecision(epsilon_exp)  << "`" << real << (imag < 0 ? "" : "+") << imag << "i" << "` " ;
                //         std::cout << std::setprecision(epsilon_init) << std::flush;
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(const Vector1N &v, double epsilon, int n, int even, int odd)
                // {
                //         if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
                //         else std::cout << "-- ";

                //         int epsilon_init = std::cout.precision();
                //         int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
                //         for(int i = 0, N = v.size(); i < N; i++) {

                //                 T d = v[i];
                //                 std::cout << TPrint::Spacer((2*epsilon_exp+8)*odd, ' ');
                //                 std::cout << std::setprecision(epsilon_exp) << "`" << d << "` ";
                //                 std::cout << TPrint::Spacer((2*epsilon_exp+8)*even, ' ');

                //                         if((i+1)   == N) std::cout << std::endl;
                //                 else if((i+1)%n == 0) std::cout << std::endl << "-- ";
                //         }

                //         std::cout << std::setprecision(epsilon_init) << std::flush;
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(const Vector2N &v, double epsilon, int n, int even, int odd)
                // {
                //         if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
                //         for(int i = 0, N = v.size(); i < N; i++) 
                //                 Print(v[i], epsilon, n, even, odd);
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(const Vector1C &v, double epsilon, int n, int even, int odd)
                // {
                //         if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
                //         else std::cout << "-- ";

                //         int epsilon_init = std::cout.precision();
                //         int epsilon_exp = TMath::Abs(TMath::Log10(epsilon))-1;
                //         for(int i = 0, N = v.size(); i < N; i++) {
                                
                //                 double real = v[i].real();
                //                 double imag = v[i].imag();
                                
                //                 std::cout << TPrint::Spacer((2*epsilon_exp+8)*odd, ' ');
                //                 std::cout << std::setprecision(epsilon_exp) << "`" << real << (imag < 0 ? "" : "+")  << imag << "i` " ;
                //                 std::cout << TPrint::Spacer((2*epsilon_exp+8)*even, ' ');

                //                         if((i+1)   == N) std::cout << std::endl;
                //                 else if((i+1)%n == 0) std::cout << std::endl << "-- ";
                //         }
                //         std::cout << std::setprecision(epsilon_init) << std::flush;
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(const Vector2C &v, double epsilon, int n, int even, int odd)
                // {
                //         if(v.size() == 0) std::cout << "-- Empty vector" << std::endl;
                //         for(int i = 0, N = v.size(); i < N; i++) 
                //                 Print(v[i], epsilon, n, even, odd);
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(const TVectorT<NumericT> &V, double epsilon, int n, int even, int odd)
                // {
                //         const T *array = V.GetMatrixArray();
                //         std::vector<T> v(&array[0], &array[0] + V.GetNoElements());
                        
                //         Print(v, epsilon, n, even, odd);
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(const TMatrixT<NumericT> &M, double epsilon, int n, int even, int odd)
                // {
                //         Vector2N m(M.GetNrows());
                //         if(m.size() == 0) std::cout << "-- Rank 0 matrix" << std::endl;

                //         for(int i = 0, I = M.GetNrows(); i < I; i++)
                //                 m[i] = this->pImpl->GetMatrixRow(M, i);

                //         Print(m, epsilon, n, even, odd);
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(ZPK zpk)
                // {
                //         std::cout << "-- Zero-Pole-Gain model (z,p,k):" << std::endl;
                        
                //         std::cout << Form("-- z = [");
                //         for(int i = 0, I = zpk.zeros.size(); i < I; i++) {
                        
                //                 double real = zpk.zeros[i].real();
                //                 double imag = zpk.zeros[i].imag();

                //                 if(i > 0) std::cout << ", ";
                //                 std::cout << Form("%.4e + %.4ei", real, imag);
                //         }
                //         std::cout << "]" << std::endl;

                //         std::cout << Form("-- p = [");
                //         for(int i = 0, I = zpk.poles.size(); i < I; i++) {
                        
                //                 double real = zpk.poles[i].real();
                //                 double imag = zpk.poles[i].imag();

                //                 if(i > 0) std::cout << ", ";
                //                 std::cout << Form("%.4e + %.4ei", real, imag);
                //         }
                //         std::cout << "]" << std::endl;

                //         std::cout << Form("-- k = `%.4e`", zpk.gain) << std::endl;
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(SS ss, TString prefix)
                // {
                //         std::cout << "-- State-system representation (A,B,C,D):" << std::endl;
                //         std::cout << prefix << "-- x'(t) = A*x(t) + B*u(t)" << std::endl;
                //         std::cout << prefix << "--  y(t) = C*x(t) + D*u(t)" << std::endl << std::endl;
                        
                //         std::cout << prefix << "-- Matrix A:" << std::endl;
                //         Print(ss.A);
                //         std::cout << prefix << "-- Matrix B:" << std::endl;
                //         Print(ss.B);
                //         std::cout << prefix << "-- Matrix C:" << std::endl;
                //         Print(ss.C);
                //         std::cout << prefix << "-- Matrix D:" << std::endl;
                //         Print(ss.D);
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(TBiquad biquad, TString prefix)
                // {
                //         if(!prefix.EqualTo("")) prefix = prefix + " ";
                //         std::cout << prefix << "-- Biquad parameters (b,a):" << std::endl;
                //         std::cout << prefix << Form("-- b0 = `%.4e`, b1 = `%.4e`, b2 = `%.4e`", std::get<0>(biquad.first),  std::get<1>(biquad.first),  std::get<2>(biquad.first) ) << std::endl;
                //         std::cout << prefix << Form("-- a0 = `%.4e`, a1 = `%.4e`, a2 = `%.4e`", std::get<0>(biquad.second), std::get<1>(biquad.second), std::get<2>(biquad.second)) << std::endl;
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(SOS sos)
                // {
                //         if(sos.size() == 0) std::cout << "-- Empty SOS representation" << std::endl;
                //         else {

                //                 std::cout << "-- SOS representation ("<< sos.size() << " sections)" << std::endl;
                //                 for(int i = 0, n_sections = sos.size(); i < n_sections; i++)
                //                 Print(sos[i], "-- ");
                //         }
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(std::vector<SOS> sos)
                // {
                //         if(sos.size() == 0) std::cout << "-- Empty SOS representation" << std::endl;
                //         else {

                //                 for(int i = 0, N = sos.size(); i < N; i++)
                //                 Print(sos[i]);
                //         }
                // }
                
                // template <typename T>
                // void TKFRLegacy *<T>::Print(std::vector<TF> tf)
                // {
                //         if(tf.size() == 0) std::cout << "-- Empty TF representation" << std::endl;
                //         else {

                //                 for(int i = 0, N = tf.size(); i < N; i++)
                //                 Print(tf[i]);
                //         }
                // }
                
                // template <typename T>
                // void TKFRLegacy *<T>::Print(std::vector<SS> ss)
                // {
                //         if(ss.size() == 0) std::cout << "-- Empty SS representation" << std::endl;
                //         else {

                //                 for(int i = 0, N = ss.size(); i < N; i++)
                //                 Print(ss[i]);
                //         }
                // }
                
                // template <typename T>
                // void TKFRLegacy *<T>::Print(std::vector<ZPK> zpk)
                // {
                //         if(zpk.size() == 0) std::cout << "-- Empty ZPK representation" << std::endl;
                //         else {

                //                 for(int i = 0, N = zpk.size(); i < N; i++)
                //                 Print(zpk[i]);
                //         }
                // }

                // template <typename T>
                // void TKFRLegacy *<T>::Print(TF tf)
                // {
                //         std::cout << "-- Transfer function (B/A):" << std::endl;
                //         std::cout << Form("-- B = [");
                //         for(int i = 0, I = tf.B.size(); i < I; i++) {
                        
                //                 if(i > 0) std::cout << ", ";
                //                 std::cout << Form("%.4e", tf.B[i]);
                //         }
                //         std::cout << "]" << std::endl;

                //         std::cout << Form("-- A = [");
                //         for(int i = 0, I = tf.A.size(); i < I; i++) {
                        
                //                 if(i > 0) std::cout << ", ";
                //                 std::cout << Form("%.4e", tf.A[i]);
                //         }
                //         std::cout << "]" << std::endl;
                // }
