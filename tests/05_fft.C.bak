#include <TKFRLegacy.h>
#include <ROOT/Signal/Impl/TKFRImpl.h>
#include <ROOT/IOPlus/Helpers.h>
#include <gtest/gtest.h>

//#include "ROOT/Signal/gtest/vector.h"

using namespace ROOT::Signal;

#ifndef NDEBUG
void Mirroring(size_t N)
{
    std::vector<std::complex<double>> x0 = gKFRLegacy->Complex(gKFRLegacy->Range(N), gKFRLegacy->Range(N));
    std::vector<std::complex<double>> x1;
    std::vector<std::complex<double>> x2;
    std::vector<double> x3;

    x1 = gKFRLegacy->Mirroring(x0);
    x2 = gKFRLegacy->MirroringDFT(x0);
    x3 = gKFRLegacy->MirroringPSD(gKFRLegacy->Real(x0));

    std::cout << std::SetLimitManip(2*N+1) << x0 << std::endl;
    std::cout << "-- " << x1 << std::endl;
    std::cout << "-- " << x2 << std::endl;
    std::cout << "-- " << x3 << std::endl;
}
#endif 

void DFT(size_t N)
{
    // std::vector<double> x = std::vector<double>({1,0,1,1,1});//ROOT::IOPlus::Helpers::GetRandomVector(N);
    std::vector<double> x = std::vector<double>(N, 1);//ROOT::IOPlus::Helpers::GetRandomVector(N);

    // Real to complex
    std::cout << "----- TS: "  << x  << std::endl;
    std::vector<std::complex<double>> X = gKFRLegacy->DFT(x);
    std::cout << "----- FFT: " << X  << std::endl;
    std::vector<double> iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
    std::cout << "----- iFFT:" << iX << std::endl;
    _EXPECT_NEAR(x, iX/*, 10*std::numeric_limits<double>::epsilon()*/);

    // Complex to complex
    // std::vector<std::complex<double>> y  = gKFRLegacy->Complex(x,x);
    // std::vector<std::complex<double>> Y  = gKFRLegacy->DFT(y);
    // std::vector<std::complex<double>> iY = gKFRLegacy->iDFT(Y);    
    // _EXPECT_NEAR(y, iY/*, 10*std::numeric_limits<double>::epsilon()*/);

    // Complex to real
    // std::vector<std::complex<double>> y  = ROOT::IOPlus::Helpers::GetRandomComplexVector(N);
    // std::vector<std::complex<double>> Y  = gKFRLegacy->DFT(y);
    // std::vector<std::complex<double>> iY = gKFRLegacy->iDFT(Y);    
    // _EXPECT_NEAR(y, iY, 10*std::numeric_limits<double>::epsilon());

    // Real to real
    // std::vector<std::complex<double>> y  = ROOT::IOPlus::Helpers::GetRandomComplexVector(N);
    // std::vector<std::complex<double>> Y  = gKFRLegacy->DFT(y);
    // std::vector<std::complex<double>> iY = gKFRLegacy->iDFT(Y);    
    // _EXPECT_NEAR(y, iY, 10*std::numeric_limits<double>::epsilon());
}

void SFT(size_t N)
{
    std::vector<double> x = ROOT::IOPlus::Helpers::GetRandomVector(N);
    std::vector<double> iX;
    std::vector<std::vector<std::complex<double>>> X;

    X = gKFRLegacy->SFT(x, N);
    iX = gKFRLegacy->iSFT(ComplexPart::Real, X, N);
    _EXPECT_NEAR(x, iX, 10*std::numeric_limits<double>::epsilon());
    
    std::vector<std::complex<double>> y = ROOT::IOPlus::Helpers::GetRandomComplexVector(N);
    std::vector<std::complex<double>> iY;
    std::vector<std::vector<std::complex<double>>> Y;

    Y = gKFRLegacy->SFT(y, N);
    iY = gKFRLegacy->iSFT(Y, N);
    _EXPECT_NEAR(y, iY, 10*std::numeric_limits<double>::epsilon());
}

void Resample(size_t N)
{
    std::vector<double> x = ROOT::IOPlus::Helpers::GetRandomVector(N);
    std::vector<double> iX;
    std::vector<double> X;

    X = gKFRLegacy->Resample(x, N, N/2);
    iX = gKFRLegacy->Resample(X, N/2, N);
    _EXPECT_NEAR(x, iX, 10*std::numeric_limits<double>::epsilon());
    
    std::vector<std::complex<double>> y = ROOT::IOPlus::Helpers::GetRandomComplexVector(N);
    std::vector<std::complex<double>> iY;
    std::vector<std::complex<double>> Y;

    Y  = gKFRLegacy->Resample(y, N, N/2, Domain::ComplexFrequency);
    iY = gKFRLegacy->Resample(Y, N/2, N, Domain::ComplexFrequency);
    _EXPECT_NEAR(y, iY, 10*std::numeric_limits<double>::epsilon());
}

// TEST(Mirroring, Size0)   { Mirroring(0);   }
// TEST(Mirroring, Size1)   { Mirroring(1);   }
// TEST(Mirroring, Size2)   { Mirroring(2);   }
// TEST(Mirroring, Size3)   { Mirroring(3);   }
// TEST(Mirroring, Size4)   { Mirroring(4);   }
// TEST(Mirroring, Size8)   { Mirroring(8);   }
// TEST(Mirroring, Size10)  { Mirroring(10);  }
// TEST(Mirroring, Size16)  { Mirroring(16);  }
// TEST(Mirroring, Size32)  { Mirroring(32);  }
// TEST(Mirroring, Size64)  { Mirroring(64);  }
// TEST(Mirroring, Size128) { Mirroring(128); }
// TEST(Mirroring, Size256) { Mirroring(256); }

// TEST(DFT, Size0)   { DFT(0);   }
// TEST(DFT, Size1)   { DFT(1);   }
// TEST(DFT, Size2)   { DFT(2);   }
TEST(DFT, Size3)   { DFT(3);   }
// TEST(DFT, Size4)   { DFT(4);   }
// TEST(DFT, Size8)   { DFT(8);   }
// TEST(DFT, Size10)  { DFT(10);  }
// TEST(DFT, Size16)  { DFT(16);  }
// TEST(DFT, Size32)  { DFT(32);  }
// TEST(DFT, Size64)  { DFT(64);  }
// TEST(DFT, Size128) { DFT(128); }
// TEST(DFT, Size256) { DFT(256); }

// TEST(SFT, Size0)   { SFT(0);   }
// TEST(SFT, Size1)   { SFT(1);   }
// TEST(SFT, Size2)   { SFT(2);   }
// TEST(SFT, Size3)   { SFT(3);   }
// TEST(SFT, Size4)   { SFT(4);   }
// TEST(SFT, Size8)   { SFT(8);   }
// TEST(SFT, Size10)  { SFT(10);  }
// TEST(SFT, Size16)  { SFT(16);  }
// TEST(SFT, Size32)  { SFT(32);  }
// TEST(SFT, Size64)  { SFT(64);  }
// TEST(SFT, Size128) { SFT(128); }
// TEST(SFT, Size256) { SFT(256); }

// TEST(Resample, Size0)   { Resample(0);   }
// TEST(Resample, Size1)   { Resample(1);   }
// TEST(Resample, Size2)   { Resample(2);   }
// TEST(Resample, Size3)   { Resample(3);   }
// TEST(Resample, Size4)   { Resample(4);   }
// TEST(Resample, Size8)   { Resample(8);   }
// TEST(Resample, Size10)  { Resample(10);  }
// TEST(Resample, Size16)  { Resample(16);  }
// TEST(Resample, Size32)  { Resample(32);  }
// TEST(Resample, Size64)  { Resample(64);  }
// TEST(Resample, Size128) { Resample(128); }
// TEST(Resample, Size256) { Resample(256); }

int main(int argc,char **argv) {

    ROOT::IOPlus::Helpers::SetSeed(1);

    // Initialize Google Test framework
    ::testing::InitGoogleTest(&argc,argv);

    // Run all tests
    return RUN_ALL_TESTS();
}

// int main()
// {
//       std::vector<double> x, iX;
//       std::vector<std::complex<double>> X;

//       x = {};
//       gKFRLegacy->Print(x);
//       X = gKFRLegacy->DFT(x);
//       gKFRLegacy->Print(X);
//       iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       gKFRLegacy->Print(iX);
//       std::cout << std::endl;

//       x = {};
//       gKFRLegacy->Print(x);
//       X = gKFRLegacy->DFT(gKFRLegacy->Complex(x));
//       gKFRLegacy->Print(X);
//       iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       gKFRLegacy->Print(iX);
//       std::cout << std::endl << "----- ";

//       x = {1,2,3,4};
//       gKFRLegacy->Print(x);
//       X = gKFRLegacy->DFT(x);
//       gKFRLegacy->Print(X);
//       X = gKFRLegacy->DFT(gKFRLegacy->Complex(x));
//       gKFRLegacy->Print(X);
//       iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       gKFRLegacy->Print(iX);

//       gKFRLegacy->Print(gKFRLegacy->SumXY(x,x));
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X, Domain::Frequency);
//       // gKFRLegacy->Print(iX);

//       // x = {0,1};
//       // std::cout << std::endl << "----- ";
//       // gKFRLegacy->Print(x);
//       // std::cout << std::endl << "----- ";
//       // X = gKFRLegacy->DFT(x);
//       // gKFRLegacy->Print(X);
//       // X = gKFRLegacy->DFT(gKFRLegacy->Complex(x));
//       // gKFRLegacy->Print(X);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       // gKFRLegacy->Print(iX);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X, Domain::Frequency);
//       // gKFRLegacy->Print(iX);
//       // std::cout << std::endl;

//       // x = {0,1,1,2};
//       // std::cout << std::endl << "----- ";
//       // gKFRLegacy->Print(x);
//       // X = gKFRLegacy->DFT(x);
//       // gKFRLegacy->Print(X);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X, Domain::Frequency);
//       // gKFRLegacy->Print(iX);
//       // X = gKFRLegacy->DFT(gKFRLegacy->Complex(x));
//       // gKFRLegacy->Print(X);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       // gKFRLegacy->Print(iX);
      
//       // std::cout << std::endl;

//       // x = {0,1,1,0};
//       // std::cout << std::endl << "----- ";
//       // gKFRLegacy->Print(x);
//       // X = gKFRLegacy->DFT(x);
//       // gKFRLegacy->Print(X);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X, Domain::Frequency);
//       // gKFRLegacy->Print(iX);
//       // X = gKFRLegacy->DFT(gKFRLegacy->Complex(x));
//       // gKFRLegacy->Print(X);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       // gKFRLegacy->Print(iX);
//       // std::cout << std::endl;

//       // x = {0,1,1,1,0};
//       // std::cout << std::endl << "----- ";
//       // gKFRLegacy->Print(x);
//       // X = gKFRLegacy->DFT(x);
//       // gKFRLegacy->Print(X);
//       // X = gKFRLegacy->DFT(gKFRLegacy->Complex(x));
//       // gKFRLegacy->Print(X);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       // gKFRLegacy->Print(iX);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X, Domain::Frequency);
//       // gKFRLegacy->Print(iX);
//       // std::cout << std::endl;

//       // x = {0,1,1,0,1,1};
//       // std::cout << std::endl << "----- ";
//       // gKFRLegacy->Print(x);
//       // X = gKFRLegacy->DFT(x);
//       // gKFRLegacy->Print(X);
//       // X = gKFRLegacy->DFT(gKFRLegacy->Complex(x));
//       // gKFRLegacy->Print(X);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X);
//       // gKFRLegacy->Print(iX);
//       // iX = gKFRLegacy->iDFT(ComplexPart::Real, X, Domain::Frequency);
//       // gKFRLegacy->Print(iX);
//       // std::cout << std::endl;

//       return 0;
// }
