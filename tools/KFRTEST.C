#include <kfr/all.hpp>
#include <iostream>
#include <vector>
#include <complex>
#include <cmath>
#include <iomanip>
using namespace kfr;

template <typename T>
std::vector<std::complex<T>> compute_dft(const std::vector<T>& input) {

    kfr::dft_plan_real_ptr<T> dft = kfr::dft_cache::instance().getreal(kfr::ctype_t<T>(), input.size());
    std::vector<std::complex<T>> output(input.size(), std::numeric_limits<T>::quiet_NaN());
    std::vector<kfr::u8> temp(dft->temp_size);

    dft->execute(reinterpret_cast<std::complex<T>*>(&output[0]), reinterpret_cast<const T*>(&input[0]), &temp[0]);

    output.resize(input.size() / 2 + 1);
    return output;
}

template <typename T>
std::vector<T> compute_inverse_dft(const std::vector<std::complex<T>>& input) {

    kfr::dft_plan_real_ptr<T> dft = kfr::dft_cache::instance().getreal(kfr::ctype_t<T>(), (input.size() - 1) * 2);
    std::vector<T> output((input.size() - 1) * 2, std::numeric_limits<T>::quiet_NaN());
    std::vector<kfr::u8> temp(dft->temp_size);

    dft->execute(&output[0], reinterpret_cast<const std::complex<T>*>(&input[0]), &temp[0]);
    for (auto &v : output) v /= 2; 
    return output;
}

int main() {
    // Create a small vector of size 2
    std::vector<double> vec1 = { 0.185917720285205980000000000, 0.719124833456764370000000000 };

    // Compute DFT
    auto dft_vec0 = compute_dft(vec1);
    auto dft_vec1 = compute_inverse_dft(dft_vec0);

    // std::cout << std::cout.precision() << std::endl;

    // Print DFT results
    for (auto v : vec1) std::cout << std::setprecision(16) << v << " ";
    std::cout << std::endl;

    for (auto v : dft_vec1) std::cout << std::setprecision(16) << v << " ";
    std::cout << std::endl;

    std::cout << std::numeric_limits<double>::epsilon() << std::endl;
    // std::cout << std::cout.precision() << std::endl;
    return 0;
}
